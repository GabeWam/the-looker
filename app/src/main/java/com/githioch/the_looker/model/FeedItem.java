package com.githioch.the_looker.model;

import com.parse.ParseFile;

import org.json.JSONObject;

import java.util.Date;
import java.util.List;

/**
 * Created by Githioch on 6/2/2015.
 */
public class FeedItem {


    private String tag;
    private String parseUserWhoPostedId;
    private int numberOfComments;
    private Date date;
    private List<String> parseUsersWhoGotead;
    private List<String> fbUsersWhoGotead;
    private Number goteas;
    private String facebookName;
    private  String facebookId;
    private ParseFile icon;
    private JSONObject jsonObject;
    private String id;
    private String name;
    private String status;
    private ParseFile image;
    private ParseFile profilePic;
    private String timeStamp;
    private String newsTitle;
    private String newsStatus;
    private String url;
    private String newsImageUrl;
    private boolean isFeed;
    private String userId;

    public FeedItem() {
    }

    public FeedItem(String tag, String userId, String id, String parseUserWhoPostedId, String name,String status,
                    ParseFile image, ParseFile icon,
                    String facebookId, String facebookName,Number goteas,
                    List<String> parseUsersWhoGotead, List<String> fbUsersWhoGotead, Date date,
                    int numberOfComments) {
        super();
        this.id = id;
        this.parseUserWhoPostedId = parseUserWhoPostedId;
        this.name = name;
        this.image = image;
        this.status = status;
        this.icon = icon;
        this.facebookId = facebookId;
        this.facebookName = facebookName;
        this.goteas = goteas;
        this.parseUsersWhoGotead = parseUsersWhoGotead;
        this.fbUsersWhoGotead = fbUsersWhoGotead;
        this.date  = date;
        this.numberOfComments = numberOfComments;
        this.isFeed = true;
        this.userId = userId;
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isFeed() {
        return isFeed;
    }

    public void setIsFeed(boolean isFeed) {
        this.isFeed = isFeed;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsStatus() {
        return newsStatus;
    }

    public void setNewsStatus(String newsStatus) {
        this.newsStatus = newsStatus;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNewsImageUrl() {
        return newsImageUrl;
    }

    public void setNewsImageUrl(String newsImageUrl) {
        this.newsImageUrl = newsImageUrl;
    }


    public String getParseUserWhoPostedId() {
        return parseUserWhoPostedId;
    }

    public void setParseUserWhoPostedId(String parseUserWhoPostedId) {
        this.parseUserWhoPostedId = parseUserWhoPostedId;
    }

    public Date getDate(){
        if(this.date != null){
            return this.date;
        }
        return null;
    }

    public int getNumberOfComments() {
        return numberOfComments;
    }

    public void setNumberOfComments(int numberOfComments) {
        this.numberOfComments = numberOfComments;
    }


    public void setParseUsersWhoGotead(List<String> list){
        this.parseUsersWhoGotead = list;
    }

    public List<String> getFbUsersWhoGotead(){
        if(this.fbUsersWhoGotead != null){
            return this.fbUsersWhoGotead;
        }
        return null;
    }

    public List<String> getParseUsersWhoGotead(){
        if(this.parseUsersWhoGotead != null){
            return this.parseUsersWhoGotead;
        }
        return null;
    }

    public int getGoteas(){
        if(this.goteas != null){
            return goteas.intValue();
        }
        return 0;
    }

    public String getFacebookName(){
        return this.facebookName;
    }

    public ParseFile getIcon(){
        return this.icon;
    }
    public String getFacebookId(){
        return this.facebookId;
    }
    public JSONObject getJsonObject(){
        return jsonObject;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ParseFile getImge() {
        return image;
    }

    public void setImge(ParseFile image) {
        this.image = image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ParseFile getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(ParseFile profilePic) {
        this.profilePic = profilePic;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getImageUri(){
        if(this.image != null){
            return this.image.getUrl();
        }

        return null;

    }

    public String getIconUri(){
        if(this.icon != null){
            return this.icon.getUrl();
        }

        return null;

    }


    public void setGoteas(int goteas) {
        this.goteas = goteas;
    }

    public void setFbUsersWhoGotead(List<String> fbUsersWhoGotead) {
        this.fbUsersWhoGotead = fbUsersWhoGotead;
    }
}
