package com.githioch.the_looker.model;

import com.parse.ParseFile;

/**
 * Created by Githioch on 6/25/2015.
 */
public class NewsItem {

    private String newsTitle;
    private String newsStatus;
    private String url;
    private ParseFile image;

    public NewsItem(){

    }

    public ParseFile getImage() {
        return image;
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }

    public String getNewsStatus() {
        return newsStatus;
    }

    public void setNewsStatus(String newsStatus) {
        this.newsStatus = newsStatus;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setImage(ParseFile image) {
        this.image = image;
    }
}
