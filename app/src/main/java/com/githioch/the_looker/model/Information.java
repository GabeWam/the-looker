package com.githioch.the_looker.model;

/**
 * Created by Githioch on 6/4/2015.
 */
public class Information {
    int itemId;
    String title;
    private int notificationNumber;

    public int getItemId() {
        return itemId;
    }

    public String getTitle() {
        return title;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setNotificationNumber(int notificationNumber) {
        this.notificationNumber = notificationNumber;
    }

    public int getNotificationNumber() {
        return notificationNumber;
    }
}
