package com.githioch.the_looker.model;

import com.parse.ParseFile;

import java.util.Date;

/**
 * Created by Githioch on 7/14/2015.
 */
public class Comment {

    String facebookId;
    Date time;
    String name;
    String comment;
    boolean isNewComment;
    private ParseFile parseImage;

    public Comment(){

    }

    public boolean isNewComment() {
        return isNewComment;
    }

    public void setIsNewComment(boolean isNewComment) {
        this.isNewComment = isNewComment;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setParseImage(ParseFile parseImage) {
        this.parseImage = parseImage;
    }

    public ParseFile getParseImage() {
        return parseImage;
    }
}
