package com.githioch.the_looker.model;

import com.parse.ParseFile;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Githioch on 7/13/2015.
 */
public class Notification implements Serializable{

    private boolean isComment = false;
    private ParseFile parseFile;
    private int numberOfComments = 0;
    private int numberOfGoteas = 0;
    private Date dateModified;
    private String postId;
    private String imageUrl;
    private String post;
    boolean isClicked;

    public Notification(){

    }

    public boolean isClicked() {
        return isClicked;
    }

    public void setIsClicked(boolean isClicked) {
        this.isClicked = isClicked;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public Date getDateModified() {
        return dateModified;
    }

    public void setDateModified(Date dateModified) {
        this.dateModified = dateModified;
    }

    public boolean isComment() {
        return isComment;
    }

    public void setIsComment(boolean isComment) {
        this.isComment = isComment;
    }

    public ParseFile getParseFile() {
        return parseFile;
    }

    public void setParseFile(ParseFile parseFile) {
        this.parseFile = parseFile;
    }

    public int getNumberOfComments() {
        return numberOfComments;
    }

    public void setNumberOfComments(int numberOfComments) {
        this.numberOfComments = numberOfComments;
    }

    public int getNumberOfGoteas() {
        return numberOfGoteas;
    }

    public void setNumberOfGoteas(int numberOfGoteas) {
        this.numberOfGoteas = numberOfGoteas;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getPost() {
        return post;
    }
}
