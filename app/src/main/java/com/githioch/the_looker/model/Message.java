package com.githioch.the_looker.model;

/**
 * Created by Githioch on 7/15/2015.
 */
public class Message{

    CharSequence date;
    String response;
    boolean isResponse;
    boolean isMessage;
    String message;
    private String newMessage;
    private boolean isNewMessage;

    public Message(){

    }

    public String getNewMessage() {
        return newMessage;
    }

    public void setNewMessage(String newMessage) {
        this.newMessage = newMessage;
    }

    public boolean isNewMessage() {
        return isNewMessage;
    }

    public void setIsNewMessage(boolean isNewMessage) {
        this.isNewMessage = isNewMessage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isMessage() {
        return isMessage;
    }

    public void setIsMessage(boolean isMessage) {
        this.isMessage = isMessage;
    }

    public boolean isResponse() {
        return isResponse;
    }

    public void setIsResponse(boolean isResponse) {
        this.isResponse = isResponse;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }


    public CharSequence getDate() {
        return date;
    }

    public void setDate(CharSequence date) {
        this.date = date;
    }
}
