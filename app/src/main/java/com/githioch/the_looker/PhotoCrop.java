package com.githioch.the_looker;

import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.githioch.the_looker.adapter.ImageAdapter;
import com.githioch.the_looker.util.CameraPreview;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Githioch on 7/4/2015.
 */
public class PhotoCrop extends Fragment {

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;
    private static final String TAG = PhotoCrop.class.getSimpleName();
    private TextView imageGrid;
    private int count;
    private ArrayList<Bitmap> thumbnails;
    private String[] arrPath;
    private ImageAdapter imageAdapter;
    private ImageAdapter.GridImageClickListener gridImageClickListener;
    private Camera mCamera;
    private CameraPreview mPreview;
    private FrameLayout preview;
    private Camera.PictureCallback mPicture;
    private android.support.v7.app.ActionBar menu;
    private int screenHeight;
    private int screenWidth;
    private ImageView icTakePhoto;

    private static final String IMAGE_CACHE_DIR = "thumbs";

    private int mImageThumbSize;
    private int mImageThumbSpacing;
    private DisplayImageOptions options;
    private ImageLoader imageLoader;

    public PhotoCrop(){

    }

    public static PhotoCrop newInstance()
    {
        return new PhotoCrop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        mCamera.release();
    }

    public float convertPixelsToDp(float px){
        Resources resources = getActivity().getApplicationContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_photo_crop, container, false);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Create our Preview view and set it as the content of our activity.
        preview = (FrameLayout) rootView.findViewById(R.id.camera_preview);
//        mPreview = (CameraPreview)rootView.findViewById(R.id.fragment_photo_crop_camera_preview);

        icTakePhoto = (ImageView)rootView.findViewById(R.id.img_take_pic);
        imageGrid = (TextView) rootView.findViewById(R.id.PhoneImageGrid);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        screenWidth = displaymetrics.widthPixels;
        screenHeight = displaymetrics.heightPixels;

        FrameLayout.LayoutParams layoutParamsIc = (FrameLayout.LayoutParams)icTakePhoto.getLayoutParams();
        layoutParamsIc.bottomMargin = (screenHeight - screenWidth) + 10;
        icTakePhoto.setLayoutParams(layoutParamsIc);

        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams)imageGrid.getLayoutParams();
        layoutParams.height = screenHeight - screenWidth;
        imageGrid.setLayoutParams(layoutParams);

        icTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCamera.takePicture(null, null, mPicture);
            }
        });
        Button okay = (Button)rootView.findViewById(R.id.buttonSend);
        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get an image from the camera
//                mCamera.takePicture(null, null, mPicture);
            }
        });

        mPicture = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                Bitmap scaledBitmap = null;

                File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
                if (pictureFile == null){
                    Log.d("camera", "Error creating media file, check storage permissions: ");
                    return;
                }

                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);

                int actualHeight = options.outHeight;
                int actualWidth = options.outWidth;

                Log.d("actual height", String.valueOf(actualHeight));
                Log.d("actual width", String.valueOf(actualWidth));

//      max Height and width values of the compressed image is taken as 816x612

                float maxHeight = 816.0f;
                float maxWidth = 612.0f;
                float imgRatio = actualWidth / actualHeight;
                float maxRatio = maxWidth / maxHeight;
//      width and height values are set maintaining the aspect ratio of the image

                if (actualHeight > maxHeight || actualWidth > maxWidth) {
                    if (imgRatio < maxRatio) {
                        imgRatio = maxHeight / actualHeight;
                        actualWidth = (int) (imgRatio * actualWidth);
                        actualHeight = (int) maxHeight;
                    } else if (imgRatio > maxRatio) {
                        imgRatio = maxWidth / actualWidth;
                        actualHeight = (int) (imgRatio * actualHeight);
                        actualWidth = (int) maxWidth;
                    } else {
                        actualHeight = (int) maxHeight;
                        actualWidth = (int) maxWidth;

                    }
                }
                // setting inSampleSize value allows to load a scaled down version of the original image

                options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
                options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
                options.inPurgeable = true;
                options.inInputShareable = true;
                options.inTempStorage = new byte[16 * 1024];

                try {
//          load the bitmap from its path
                    bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
                } catch (OutOfMemoryError exception) {
                    exception.printStackTrace();

                }
                try {
                    scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight,
                            Bitmap.Config.ARGB_8888);
                } catch (OutOfMemoryError exception) {
                    exception.printStackTrace();
                }

                float ratioX = actualWidth / (float) options.outWidth;
                float ratioY = actualHeight / (float) options.outHeight;
                float middleX = actualWidth / 2.0f;
                float middleY = actualHeight / 2.0f;

                Matrix scaleMatrix = new Matrix();
                scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

                Canvas canvas = new Canvas(scaledBitmap);
                canvas.setMatrix(scaleMatrix);
                canvas.drawBitmap(bitmap, middleX - bitmap.getWidth() / 2, middleY
                                - bitmap.getHeight() / 2,
                        new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
                ExifInterface exif;
                try {
                    exif = new ExifInterface(pictureFile.getAbsolutePath());

                    int orientation = exif.getAttributeInt(
                            ExifInterface.TAG_ORIENTATION, 0);
                    Log.d("EXIF", "Exif: " + orientation);
                    Matrix matrix = new Matrix();
                    if(orientation == 0){
                        matrix.postRotate(90);
                        Log.d("EXIF", "Exif: " + orientation);
                    }
                    if (orientation == 6) {
                        matrix.postRotate(90);
                        Log.d("EXIF", "Exif: " + orientation);
                    } else if (orientation == 3) {
                        matrix.postRotate(180);
                        Log.d("EXIF", "Exif: " + orientation);
                    } else if (orientation == 8) {
                        matrix.postRotate(270);
                        Log.d("EXIF", "Exif: " + orientation);
                    }
                    scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                            scaledBitmap.getHeight() , scaledBitmap.getHeight(), matrix,
                            true);
                    Log.d("preview width", String.valueOf(convertPixelsToDp(screenWidth)));
                    Log.d("image width", String.valueOf(bitmap.getHeight()));

                } catch (IOException e) {
                    e.printStackTrace();
                }

                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
                    Log.d("image", "saved");
                } catch (FileNotFoundException e) {
                    Log.d(TAG, "File not found: " + e.getMessage());
                }
            }
        };

        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        imageLoader = ImageLoader.getInstance();

         options = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .considerExifParams(true)
                .build();



        // Create an instance of Camera

//        mCamera = getCameraInstance();
//        mPreview = new CameraPreview(getActivity(), mCamera);




    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /** Create a File for saving an image or video */
    private static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "MyCameraApp");
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
            Log.d("camera", "unavailable " + e.getMessage());
        }
        return c; // returns null if camera is unavailable
    }

    public void startCamera() {
        mCamera = getCameraInstance();

    }
}
