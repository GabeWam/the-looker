package com.githioch.the_looker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.githioch.the_looker.adapter.FeedListAdapter;
import com.githioch.the_looker.adapter.NewsAdapter;
import com.githioch.the_looker.model.NewsItem;
import com.githioch.the_looker.util.ConnectionDetector;
import com.githioch.the_looker.util.MaterialViewPagerHelper;
import com.githioch.the_looker.util.RecyclerViewMaterialAdapter;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Githioch on 6/22/2015.
 */
public class RecyclerViewNewsFragment extends Fragment{

    private static final String TAG = RecyclerViewNewsFragment.class.getSimpleName();
    private static final int RESULT_LOAD_IMG = 100;
    private static final int PIC_CROP = 2;
    private static final int TAKE_PHOTO = 99;

    private RecyclerView.Adapter mAdapter;
    private List mContentItems;
    private RecyclerView mRecyclerView;
    private ProgressWheel progressWheel;
    private FeedListAdapter feedListAdapter;
    private NewsAdapter newsAdapter;
    private ParseQuery<ParseObject> parseQuery;

    public RecyclerViewNewsFragment()
    {
        mContentItems = new ArrayList();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(parseQuery != null){
            parseQuery.cancel();
        }

    }

    public static RecyclerViewNewsFragment newInstance()
    {
        return new RecyclerViewNewsFragment();
    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle){
        View rootView = layoutinflater.inflate(R.layout.layout_recyclerview_news, viewgroup, false);
        mRecyclerView = (RecyclerView)rootView.findViewById(R.id.news_feed_recyclerview_test);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        final Button retry = (Button)rootView.findViewById(R.id.retry);

        if(new ConnectionDetector(getActivity()).isConnectingToInternet()){
            getData();
        }else{
            retry.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(new ConnectionDetector(getActivity()).isConnectingToInternet()){
                    retry.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    getData();
                }
            }
        });

        return rootView;
    }

    private void getData() {
        final ArrayList<NewsItem> newsItems = new ArrayList<>();

        parseQuery = new ParseQuery<ParseObject>("News");
        parseQuery.orderByDescending("createdAt");
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
               if(e == null){
                   if(list != null && list.size() > 0){
                       for (int i = 0; i < list.size(); i++) {
                           NewsItem newsItem = new NewsItem();
                           ParseObject parseObject = list.get(i);
                           newsItem.setNewsTitle(parseObject.getString("newsTitle"));
                           newsItem.setNewsStatus(parseObject.getString("newsStatus"));
                           newsItem.setUrl(parseObject.getString("url"));
                           newsItem.setImage(parseObject.getParseFile("image"));

                           newsItems.add(newsItem);
                       }
                       newsAdapter = new NewsAdapter(getActivity());
                       newsAdapter.setNewsList(newsItems);

                       mAdapter = new RecyclerViewMaterialAdapter(newsAdapter);
                       mRecyclerView.setAdapter(mAdapter);
                       MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView, null);
                   }else{
                       Log.d(TAG, "list null or empty");
                   }
               }else{
                   Log.d(TAG, e.toString());
               }

            }
        });
    }

}

