package com.githioch.the_looker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.widget.ProfilePictureView;
import com.githioch.the_looker.util.ConnectionDetector;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Githioch on 6/7/2015.
 */
//TODO skipping frames when loading comments
//TODO download all comments with newsfeed maybe???
public class FeedDetailsActivity extends Activity{

    private ImageView imageView;
    private TextView status;
    private Activity activity;
    private EditText editMessage;
    private Button btnComment;
    private RelativeLayout relativeLayout;
    private Bitmap icon;
    private ImageLoader imageLoader;
    private ScrollView scrollView;
    private ProgressWheel progressWheel;
    private Button retry;
    private RelativeLayout noInternet;
    private LinearLayout commentingFields;
    private TextView textView;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        returnIntent.putExtra("feed","feed");
        finish();

//        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_detail);

        imageLoader = ImageLoader.getInstance();

        activity = this;

        Bundle extras = getIntent().getExtras();
        final String objectId = extras.getString("id");

        status = (TextView) findViewById(R.id.feed_details_status);
        editMessage  = (EditText)findViewById(R.id.editMessage);
        btnComment = (Button) findViewById(R.id.buttonSend);
        scrollView = (ScrollView)findViewById(R.id.scrollView);
        progressWheel = (ProgressWheel)findViewById(R.id.progress_wheel);
        progressWheel.setBarColor(getResources().getColor(R.color.primary_dark));
        noInternet = (RelativeLayout)findViewById(R.id.no_internet);
        retry = (Button)findViewById(R.id.retry);
        commentingFields = (LinearLayout)findViewById(R.id.edittext_and_button_for_commenting);

        fectchAndDisplayComments(objectId);

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("retry", "clicked");
                ConnectionDetector connectionDetector = new ConnectionDetector(FeedDetailsActivity.this);
                if (connectionDetector.isConnectingToInternet()) {
                    scrollView.setVisibility(View.VISIBLE);
                    noInternet.setVisibility(View.GONE);
                    commentingFields.setVisibility(View.VISIBLE);
                    fectchAndDisplayComments(objectId);
                }
            }
        });



        btnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String facebookId = checkAndReturnFacebookId();
                final String comment = editMessage.getText().toString();

                if(!comment.equals("")){
                    ParseObject parseObject = new ParseObject("comments");
                    parseObject.put("userId", ParseUser.getCurrentUser().getObjectId());
                    parseObject.put("objectIdRef", objectId);
                    parseObject.put("comment", comment);
                    if (facebookId != null) {
                        parseObject.put("facebookId", facebookId);
                        parseObject.put("facebookName", ParseUser.getCurrentUser()
                                .getJSONObject("profile").optString("name"));
                    } else {
                        Bitmap icon = createIcon();
                        setIcon(icon);

                        if (icon != null) {
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            icon.compress(Bitmap.CompressFormat.PNG, 100, stream);
                            byte[] byteArray = stream.toByteArray();
                            ParseFile newParse = new ParseFile("send", byteArray);
                            parseObject.put("parseIcon", newParse);
                        }

                        parseObject.put("parseName", ParseUser.getCurrentUser().getString("name"));
                    }

                    parseObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                setData(comment, facebookId, icon);
                            } else {
                                Toast.makeText(getApplicationContext(), "Error saving. Try" +
                                        " again later.", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });

                    // get parseonject and increase number of comments
                    ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("News_Feed_Test");
                    parseQuery.getInBackground(objectId, new GetCallback<ParseObject>() {
                        @Override
                        public void done(ParseObject parseObject, ParseException e) {
                            int number = parseObject.getInt("numberOfComments");
                            number++;
                            parseObject.put("numberOfComments", number);
                            parseObject.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    Log.d("comment number", "changed");
                                }
                            });
                            updateNotifications(parseObject, objectId);

                        }
                    });

                    editMessage.setText("");
                    InputMethodManager imm = (InputMethodManager) getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(editMessage.getWindowToken(), 0);
                }else{
                    Toast.makeText(getApplicationContext(), "Please enter comment", Toast.LENGTH_SHORT)
                            .show();
                }

            }

            private Bitmap createIcon() {

                if (ParseUser.getCurrentUser().has("image")) {

                    ParseFile parseFile = ParseUser.getCurrentUser().getParseFile("image");

                    byte[] bitmapdata = new byte[0];
                    try {
                        bitmapdata = parseFile.getData();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
                    bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true);

                    return bitmap;
                }
                return null;
            }

            private String checkAndReturnFacebookId() {

                if (ParseUser.getCurrentUser() != null && ParseUser.getCurrentUser().getJSONObject("profile") != null) {
                    return ParseUser.getCurrentUser()
                            .getJSONObject("profile").optString("facebookId");
                }

                return null;
            }

            private void setData(String comment, String facebookId, Bitmap icon) {

                ProfilePictureView profilePictureView = null;
                ImageView imageView = null;

                RelativeLayout layout = new RelativeLayout(FeedDetailsActivity.this);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams
                        (RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                layout.setLayoutParams(layoutParams);

                RelativeLayout.LayoutParams params1 =
                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT);
                RelativeLayout.LayoutParams params2 =
                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT);
                RelativeLayout.LayoutParams params3 =
                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT);
                RelativeLayout.LayoutParams params4 =
                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                RelativeLayout.LayoutParams.WRAP_CONTENT);

                TextView msg = new TextView(FeedDetailsActivity.this);
                TextView name = new TextView(FeedDetailsActivity.this);
                TextView date= new TextView(FeedDetailsActivity.this);

                name.setId(2);
                msg.setId(3);
                date.setId(4);


                if (facebookId != null) {

                    profilePictureView = new ProfilePictureView(FeedDetailsActivity.this);
                    profilePictureView.setProfileId(facebookId);
                    profilePictureView.setId(1);
                    profilePictureView.setPadding(20, 10, 20, 10);
                    profilePictureView.setPresetSize(ProfilePictureView.SMALL);
                    params2.addRule(RelativeLayout.RIGHT_OF, profilePictureView.getId());
                    params2.addRule(RelativeLayout.BELOW, name.getId());
                    params3.addRule(RelativeLayout.BELOW, msg.getId());
                    params4.addRule(RelativeLayout.RIGHT_OF, profilePictureView.getId());

                } else {
                    imageView = new ImageView(FeedDetailsActivity.this);

                    if (icon != null) {
                        imageView.setImageBitmap(icon);
                    } else {
                        imageView.setImageResource(R.drawable.no_profile);

                    }
                    imageView.setPadding(20, 10, 10, 10);
                    params2.addRule(RelativeLayout.RIGHT_OF, imageView.getId());
                    params2.addRule(RelativeLayout.BELOW, name.getId());
                    params3.addRule(RelativeLayout.RIGHT_OF, imageView.getId());
                    params3.addRule(RelativeLayout.BELOW, msg.getId());
                    params4.addRule(RelativeLayout.RIGHT_OF, imageView.getId());

                }

                LinearLayout chat = (LinearLayout) findViewById(R.id.comments_relative_layout);

                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
                formatter.setTimeZone(TimeZone.getTimeZone("Africa/Nairobi"));
                Date d = Calendar.getInstance().getTime();

                try {
                    d = formatter.parse(formatter.format(d));
                } catch (java.text.ParseException e1) {
                    e1.printStackTrace();
                }

                CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                        d.getTime(),
                        System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
                date.setText(timeAgo);


                msg.setText(comment);
                if (profilePictureView != null) {
                    name.setText(ParseUser.getCurrentUser().getJSONObject("profile").optString("name"));
                    name.setPadding(20, 0, 20, 0);
                    msg.setPadding(20, 0, 20, 0);
                    date.setPadding(180, 0, 10, 0);
                } else {
                    name.setText(ParseUser.getCurrentUser().getString("name"));
                    name.setPadding(180, 0, 10, 0);
                    msg.setPadding(180, 0, 10, 0);
                    date.setPadding(180, 0, 10, 0);
                }
//                msg.setPadding(10, 10, 10, 10);
                msg.setTextColor(getResources().getColor(R.color.primary_dark_material_dark));
                name.setTextColor(getResources().getColor(R.color.primary_dark_material_dark));
                date.setTextColor(getResources().getColor(R.color.secondary_text));

                date.setTextSize(10);

                if (profilePictureView != null) {
                    layout.addView(profilePictureView, params1);
                } else {
                    layout.addView(imageView, params1);
                }

                layout.addView(name, params4);
                layout.addView(msg, params2);
                layout.addView(date, params3);

                chat.addView(layout);

                if(textView != null){
                    textView.setVisibility(View.GONE);
                }

                scrollView.fullScroll(View.FOCUS_DOWN);

            }
        });

    }

    private void updateNotifications(ParseObject parseObjectPassed, final String objectId) {
        // get id of user who posted so that the new notification can be saved
        // in a notifications array for the user
        // get json array
        // loop through array to get jsonobjects
        // get jsonobject that has the post id matching the key
        // update its new field
        final String userWhoPostedId = parseObjectPassed.getString("userId");
        if(userWhoPostedId != ParseUser.getCurrentUser().getObjectId()){
            ParseQuery<ParseObject> parseObjectParseQuery = new ParseQuery<ParseObject>("notifications");
            parseObjectParseQuery.whereEqualTo("userId", userWhoPostedId);
            parseObjectParseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if(e == null){
                        Log.d("id", parseObject.getObjectId());
                        JSONArray jsonArray = parseObject.getJSONArray("notif");
                        for(int i = 0; i < jsonArray.length(); i++){
                            try {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Iterator<String> iterator = jsonObject.keys();
                                while(iterator.hasNext()){
                                    String key  = iterator.next();
                                    if(key.equals("postId")){
                                        Log.d("passed", "key check");
                                        String postId = jsonObject.getString(key);
                                        if(postId.equals(objectId)){
                                            Log.d("passed", "post id check");
                                            int newNumber = jsonObject.getInt("new");
                                            newNumber++;
                                            jsonObject.put("new", newNumber);
                                            parseObject.save();
                                        }
                                    }

                                }
                            } catch (JSONException | ParseException e1) {
                                e1.printStackTrace();
                            }
                        }

                    }

                }
            });
        }
    }

    private void fectchAndDisplayComments(final String objectId) {

        ConnectionDetector connectionDetector = new ConnectionDetector(FeedDetailsActivity.this);
        if(connectionDetector.isConnectingToInternet()){

            progressWheel.setVisibility(View.VISIBLE);
            progressWheel.spin();
            final LinearLayout chat = (LinearLayout) findViewById(R.id.comments_relative_layout);

            ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("comments");
            parseQuery.whereEqualTo("objectIdRef", objectId);
            parseQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(final List<ParseObject> list, ParseException e) {
                    if (e == null) {
                        if(list.size() > 0){
                            for (int i = 0; i < list.size(); i++) {

                                ProfilePictureView profilePictureView = null;
                                ImageView imageView = null;

                                RelativeLayout layout = new RelativeLayout(FeedDetailsActivity.this);
                                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams
                                        (RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                                layout.setLayoutParams(layoutParams);

                                RelativeLayout.LayoutParams params1 =
                                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                                RelativeLayout.LayoutParams.WRAP_CONTENT);
                                RelativeLayout.LayoutParams params2 =
                                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                                RelativeLayout.LayoutParams.WRAP_CONTENT);

                                RelativeLayout.LayoutParams params3 =
                                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                                RelativeLayout.LayoutParams.WRAP_CONTENT);

                                RelativeLayout.LayoutParams params4 =
                                        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                                RelativeLayout.LayoutParams.WRAP_CONTENT);

                                TextView name = new TextView(FeedDetailsActivity.this);
                                TextView msg = new TextView(FeedDetailsActivity.this);
                                TextView date= new TextView(FeedDetailsActivity.this);

                                name.setId(2);
                                msg.setId(3);
                                date.setId(4);

                                if (list.get(i).getString("facebookId") != null) {
                                    profilePictureView = new ProfilePictureView(FeedDetailsActivity.this);
                                    profilePictureView.setProfileId(list.get(i).getString("facebookId"));
                                    profilePictureView.setId(1);
                                    profilePictureView.setPadding(20, 10, 20, 10);
                                    profilePictureView.setPresetSize(ProfilePictureView.SMALL);
                                    params2.addRule(RelativeLayout.RIGHT_OF, profilePictureView.getId());
                                    params2.addRule(RelativeLayout.BELOW, name.getId());
                                    params3.addRule(RelativeLayout.BELOW, msg.getId());
                                    params4.addRule(RelativeLayout.RIGHT_OF, profilePictureView.getId());
                                }
                                else if(list.get(i).getParseFile("parseIcon") != null) {
                                    imageView = new ImageView(FeedDetailsActivity.this);
                                    ParseFile parseFile = list.get(i).getParseFile("parseIcon");

                                    byte[] bitmapdata = new byte[0];
                                    try {
                                        bitmapdata = parseFile.getData();
                                    } catch (ParseException e1) {
                                        e1.printStackTrace();
                                    }
                                    Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
                                    BitmapDrawable bitmapDrawable = new BitmapDrawable(activity.getResources(), bitmap);

                                    imageView.setImageDrawable(bitmapDrawable);
                                    imageView.setPadding(20, 10, 10, 10);
                                    params2.addRule(RelativeLayout.RIGHT_OF, imageView.getId());
                                    params2.addRule(RelativeLayout.BELOW, name.getId());
                                    params3.addRule(RelativeLayout.RIGHT_OF, imageView.getId());
                                    params3.addRule(RelativeLayout.BELOW, msg.getId());
                                    params4.addRule(RelativeLayout.RIGHT_OF, imageView.getId());

                                }
                                else{
                                    imageView = new ImageView(FeedDetailsActivity.this);
                                    imageView.setImageResource(R.drawable.no_profile);
                                    imageView.setPadding(10, 10, 10, 10);
                                    params2.addRule(RelativeLayout.RIGHT_OF, imageView.getId());
                                    params2.addRule(RelativeLayout.BELOW, name.getId());
                                    params3.addRule(RelativeLayout.RIGHT_OF, imageView.getId());
                                    params3.addRule(RelativeLayout.BELOW, msg.getId());
                                    params4.addRule(RelativeLayout.RIGHT_OF, imageView.getId());

                                }



                                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
                                formatter.setTimeZone(TimeZone.getTimeZone("Africa/Nairobi"));
                                Date d = list.get(i).getCreatedAt();

                                try {
                                    d = formatter.parse(formatter.format(d));
                                } catch (java.text.ParseException e1) {
                                    e1.printStackTrace();
                                }

                                CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                                        d.getTime(),
                                        System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
                                date.setText(timeAgo);

                                msg.setText(list.get(i).getString("comment"));
                                if(profilePictureView != null){

                                    if(list.get(i).getString("facebookName") != null){
                                        name.setText(list.get(i).getString("facebookName"));
                                        Log.d("name",list.get(i).getString("facebookName"));
                                        name.setPadding(20, 0, 20, 0);
                                    }

                                    msg.setPadding(20, 0, 20, 0);
                                    date.setPadding(180, 0, 10, 0);
                                }
                                else{
                                    if(list.get(i).getString("parseName") != null){
                                        name.setText(list.get(i).getString("parseName"));
                                        Log.d("name",list.get(i).getString("parseName"));
                                        name.setPadding(180, 0, 10, 0);
                                    }

                                    msg.setPadding(180, 0, 10, 0);
                                    date.setPadding(180, 0, 10, 0);
                                }

                                msg.setTextColor(getResources().getColor(R.color.primary_dark_material_dark));
                                name.setTextColor(getResources().getColor(R.color.primary_dark_material_dark));
                                date.setTextColor(getResources().getColor(R.color.secondary_text));

                                date.setTextSize(10);

                                if(profilePictureView != null){
                                    layout.addView(profilePictureView, params1);
                                }
                                else{
                                    layout.addView(imageView, params1);
                                }

                                layout.addView(name, params4);
                                layout.addView(msg, params2);
                                layout.addView(date, params3);

                                chat.addView(layout);
                            }
                        }else{
                            textView = new TextView(FeedDetailsActivity.this);
                            textView.setText("Be the first to comment");
                            textView.setPadding(0,0,0,20);
                            textView.setTextSize(20);
                            RelativeLayout.LayoutParams params5 =
                                    new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
                                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                            params5.addRule(RelativeLayout.CENTER_HORIZONTAL);
                            RelativeLayout layout = new RelativeLayout(FeedDetailsActivity.this);
                            layout.addView(textView, params5);
                            chat.addView(layout);

                        }


                        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("News_Feed_Test");
                        parseQuery.getInBackground(objectId, new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject parseObject, ParseException e) {

                                if (e == null) {

                                    status.setText(parseObject.getString("status"));
                                    progressWheel.setVisibility(View.GONE);
                                    scrollView.setVisibility(View.VISIBLE);
                                } else {
                                    Toast.makeText(getApplicationContext(), "Error fetching data. Try again later." +
                                            "", Toast.LENGTH_SHORT).show();
                                }

                            }
                        });

                    } else {
                        Toast.makeText(getApplicationContext(), "Error fetching comments. Please" +
                                " try again later.", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        } else {
            commentingFields.setVisibility(View.GONE);
            noInternet.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
        }


    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }
}
