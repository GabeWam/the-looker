package com.githioch.the_looker.util;

/**
 * Created by Githioch on 6/29/2015.
 */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewTreeObserver;

import com.nineoldandroids.view.ViewHelper;


public class MaterialViewPagerHeader {
    protected Context context;
    protected View toolbarLayout;
    protected Toolbar toolbar;
    protected View mPagerSlidingTabStrip;
    protected View toolbarLayoutBackground;
    protected View headerBackground;
    protected View statusBackground;
    protected View mLogo;
    public float finalTabsY;
    public float finalTitleY;
    public float finalTitleHeight;
    public float finalTitleX;
    public float originalTitleY;
    public float originalTitleHeight;
    public float originalTitleX;
    public float finalScale;

    private MaterialViewPagerHeader(Toolbar toolbar) {
        this.toolbar = toolbar;
        this.context = toolbar.getContext();
        this.toolbarLayout = (View)toolbar.getParent();
    }

    public static MaterialViewPagerHeader withToolbar(Toolbar toolbar) {
        return new MaterialViewPagerHeader(toolbar);
    }

    public Context getContext() {
        return this.context;
    }

    public MaterialViewPagerHeader withPagerSlidingTabStrip(View pagerSlidingTabStrip) {
        this.mPagerSlidingTabStrip = pagerSlidingTabStrip;
        this.mPagerSlidingTabStrip.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                MaterialViewPagerHeader.this.finalTabsY = Utils.dpToPx(-2.0F, MaterialViewPagerHeader.this.context);
                MaterialViewPagerHeader.this.mPagerSlidingTabStrip.getViewTreeObserver().removeOnPreDrawListener(this);
                return false;
            }
        });
        return this;
    }

    public MaterialViewPagerHeader withHeaderBackground(View headerBackground) {
        this.headerBackground = headerBackground;
        return this;
    }

    public MaterialViewPagerHeader withStatusBackground(View statusBackground) {
        this.statusBackground = statusBackground;
        return this;
    }

    public MaterialViewPagerHeader withToolbarLayoutBackground(View toolbarLayoutBackground) {
        this.toolbarLayoutBackground = toolbarLayoutBackground;
        return this;
    }

    public int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if(resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }

        return result;
    }

    public MaterialViewPagerHeader withLogo(View logo) {
        this.mLogo = logo;
        this.toolbar.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                MaterialViewPagerHeader.this.originalTitleY = ViewHelper.getY(MaterialViewPagerHeader.this.mLogo);
                MaterialViewPagerHeader.this.originalTitleX = ViewHelper.getX(MaterialViewPagerHeader.this.mLogo);
                MaterialViewPagerHeader.this.originalTitleHeight = (float)MaterialViewPagerHeader.this.mLogo.getHeight();
                MaterialViewPagerHeader.this.finalTitleHeight = Utils.dpToPx(21.0F, MaterialViewPagerHeader.this.context);
                MaterialViewPagerHeader.this.finalScale = MaterialViewPagerHeader.this.finalTitleHeight / MaterialViewPagerHeader.this.originalTitleHeight;
                MaterialViewPagerHeader.this.finalTitleY = (float)((MaterialViewPagerHeader.this.toolbar.getPaddingTop() + MaterialViewPagerHeader.this.toolbar.getHeight()) / 2) - MaterialViewPagerHeader.this.finalTitleHeight / 2.0F - (1.0F - MaterialViewPagerHeader.this.finalScale) * MaterialViewPagerHeader.this.finalTitleHeight;
                MaterialViewPagerHeader.this.finalTitleX = Utils.dpToPx(52.0F, MaterialViewPagerHeader.this.context) - (float)(MaterialViewPagerHeader.this.mLogo.getWidth() / 2) * (1.0F - MaterialViewPagerHeader.this.finalScale);
                MaterialViewPagerHeader.this.toolbarLayout.getViewTreeObserver().removeOnPreDrawListener(this);
                return false;
            }
        });
        return this;
    }

    public Toolbar getToolbar() {
        return this.toolbar;
    }

    public View getHeaderBackground() {
        return this.headerBackground;
    }

    public View getStatusBackground() {
        return this.statusBackground;
    }

    public View getLogo() {
        return this.mLogo;
    }
}

