package com.githioch.the_looker.util;

/**
 * Created by Githioch on 6/29/2015.
 */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ListView;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ObservableWebView;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.view.ViewHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class MaterialViewPagerAnimator {
    private static final String TAG = MaterialViewPagerAnimator.class.getSimpleName();
    private static final Boolean ENABLE_LOG = Boolean.valueOf(false);
    private Context context;
    private MaterialViewPagerHeader mHeader;
    private static final int ENTER_TOOLBAR_ANIMATION_DURATION = 600;
    protected MaterialViewPager materialViewPager;
    public final float elevation;
    public final float scrollMax;
    public final float scrollMaxDp;
    protected float lastYOffset = -1.0F;
    protected float lastPercent = 0.0F;
    protected MaterialViewPagerSettings settings;
    protected List<Object> scrollViewList = new ArrayList();
    protected HashMap<Object, Integer> yOffsets = new HashMap();
    private float headerYOffset = 3.4028235E38F;
    private Object headerAnimator;
    boolean followScrollToolbarIsVisible = false;
    float firstScrollValue = 1.4E-45F;

    public MaterialViewPagerAnimator(MaterialViewPager materialViewPager) {
        this.settings = materialViewPager.settings;
        this.materialViewPager = materialViewPager;
        this.mHeader = materialViewPager.materialViewPagerHeader;
        this.context = this.mHeader.getContext();
        this.scrollMax = (float)this.settings.headerHeight;
        this.scrollMaxDp = Utils.dpToPx(this.scrollMax, this.context);
        this.elevation = Utils.dpToPx(4.0F, this.context);
    }

    private void dispatchScrollOffset(Object source, float yOffset) {
        if(this.scrollViewList != null) {
            Iterator i$ = this.scrollViewList.iterator();

            while(i$.hasNext()) {
                Object scroll = i$.next();
                if(scroll != null && scroll != source) {
                    this.setScrollOffset(scroll, yOffset);
                }
            }
        }

    }

    private void setScrollOffset(Object scroll, float yOffset) {
        if(scroll != null && yOffset >= 0.0F) {
            if(scroll instanceof RecyclerView) {
                RecyclerView.LayoutManager layoutManager = ((RecyclerView)scroll).getLayoutManager();
                if(layoutManager instanceof LinearLayoutManager) {
                    LinearLayoutManager staggeredGridLayoutManager = (LinearLayoutManager)layoutManager;
                    staggeredGridLayoutManager.scrollToPositionWithOffset(0, (int)(-yOffset));
                } else if(layoutManager instanceof StaggeredGridLayoutManager) {
                    StaggeredGridLayoutManager staggeredGridLayoutManager1 = (StaggeredGridLayoutManager)layoutManager;
                    staggeredGridLayoutManager1.scrollToPositionWithOffset(0, (int)(-yOffset));
                }
            } else if(scroll instanceof android.widget.ScrollView) {
                ((android.widget.ScrollView)scroll).scrollTo(0, (int)yOffset);
            } else if(scroll instanceof ListView) {
                ((ListView)scroll).scrollTo(0, (int)yOffset);
            } else if(scroll instanceof WebView) {
                ((WebView)scroll).scrollTo(0, (int)yOffset);
            }

            this.yOffsets.put(scroll, Integer.valueOf((int)yOffset));
        }

    }

    public void onMaterialScrolled(Object source, float yOffset) {
        if(yOffset != this.lastYOffset) {
            float scrollTop = -yOffset;
            if(this.mHeader.headerBackground != null) {
                if(this.settings.parallaxHeaderFactor != 0.0F) {
                    ViewHelper.setTranslationY(this.mHeader.headerBackground, scrollTop / this.settings.parallaxHeaderFactor);
                }

                if(ViewHelper.getY(this.mHeader.headerBackground) >= 0.0F) {
                    ViewHelper.setY(this.mHeader.headerBackground, 0.0F);
                }
            }

            if(ENABLE_LOG.booleanValue()) {
                Log.d("yOffset", "" + yOffset);
            }

            this.dispatchScrollOffset(source, Utils.minMax(0.0F, yOffset, this.scrollMaxDp));
            float percent = yOffset / this.scrollMax;
            percent = Utils.minMax(0.0F, percent, 1.0F);
            this.setColorPercent(percent);
            this.lastPercent = percent;
            float scrollUp;
            if(this.mHeader.mPagerSlidingTabStrip != null) {
                if(ENABLE_LOG.booleanValue()) {
                    Log.d(TAG, "" + scrollTop);
                }

                if(scrollTop <= 0.0F) {
                    ViewHelper.setTranslationY(this.mHeader.mPagerSlidingTabStrip, scrollTop);
                    ViewHelper.setTranslationY(this.mHeader.toolbarLayoutBackground, scrollTop);
                    if(ViewHelper.getY(this.mHeader.mPagerSlidingTabStrip) < (float)this.mHeader.getToolbar().getBottom()) {
                        scrollUp = (float)(this.mHeader.getToolbar().getBottom() - this.mHeader.mPagerSlidingTabStrip.getTop());
                        ViewHelper.setTranslationY(this.mHeader.mPagerSlidingTabStrip, scrollUp);
                        ViewHelper.setTranslationY(this.mHeader.toolbarLayoutBackground, scrollUp);
                    }
                }
            }

            if(this.mHeader.mLogo != null) {
                if(this.settings.hideLogoWithFade) {
                    ViewHelper.setAlpha(this.mHeader.mLogo, 1.0F - percent);
                    ViewHelper.setTranslationY(this.mHeader.mLogo, (this.mHeader.finalTitleY - this.mHeader.originalTitleY) * percent);
                } else {
                    ViewHelper.setTranslationY(this.mHeader.mLogo, (this.mHeader.finalTitleY - this.mHeader.originalTitleY) * percent);
                    ViewHelper.setTranslationX(this.mHeader.mLogo, (this.mHeader.finalTitleX - this.mHeader.originalTitleX) * percent);
                    scrollUp = (1.0F - percent) * (1.0F - this.mHeader.finalScale) + this.mHeader.finalScale;
                    Utils.setScale(scrollUp, new View[]{this.mHeader.mLogo});
                }
            }

            if(this.settings.hideToolbarAndTitle && this.mHeader.toolbarLayout != null) {
                boolean scrollUp1 = this.lastYOffset < yOffset;
                if(scrollUp1) {
                    this.scrollUp(yOffset);
                } else {
                    this.scrollDown(yOffset);
                }
            }

            if(this.headerAnimator != null && percent < 1.0F) {
                if(this.headerAnimator instanceof ObjectAnimator) {
                    ((ObjectAnimator)this.headerAnimator).cancel();
                } else if(this.headerAnimator instanceof android.animation.ObjectAnimator) {
                    ((android.animation.ObjectAnimator)this.headerAnimator).cancel();
                }

                this.headerAnimator = null;
            }

            this.lastYOffset = yOffset;
        }
    }

    private void scrollUp(float yOffset) {
        if(ENABLE_LOG.booleanValue()) {
            Log.d(TAG, "scrollUp");
        }

        this.followScrollToolbarLayout(yOffset);
    }

    private void scrollDown(float yOffset) {
        if(ENABLE_LOG.booleanValue()) {
            Log.d(TAG, "scrollDown");
        }

        if(yOffset > (float)this.mHeader.toolbarLayout.getHeight()) {
            this.animateEnterToolbarLayout(yOffset);
        } else if(this.headerAnimator != null) {
            ViewHelper.setTranslationY(this.mHeader.toolbarLayout, 0.0F);
            this.followScrollToolbarIsVisible = true;
        } else {
            this.headerYOffset = 3.4028235E38F;
            this.followScrollToolbarLayout(yOffset);
        }

    }

    public void setColor(int color, int duration) {
        ObjectAnimator colorAnim = ObjectAnimator.ofInt(this.mHeader.headerBackground, "backgroundColor", new int[]{this.settings.color, color});
        colorAnim.setEvaluator(new ArgbEvaluator());
        colorAnim.setDuration((long)duration);
        colorAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                int colorAlpha = Utils.colorWithAlpha(((Integer)animation.getAnimatedValue()).intValue(), MaterialViewPagerAnimator.this.lastPercent);
                MaterialViewPagerAnimator.this.mHeader.headerBackground.setBackgroundColor(colorAlpha);
                MaterialViewPagerAnimator.this.mHeader.statusBackground.setBackgroundColor(colorAlpha);
                MaterialViewPagerAnimator.this.mHeader.toolbar.setBackgroundColor(colorAlpha);
                MaterialViewPagerAnimator.this.mHeader.toolbarLayoutBackground.setBackgroundColor(colorAlpha);
                MaterialViewPagerAnimator.this.mHeader.mPagerSlidingTabStrip.setBackgroundColor(colorAlpha);
            }
        });
        colorAnim.start();
        this.settings.color = color;
    }

    public void setColorPercent(float percent) {
        Utils.setBackgroundColor(Utils.colorWithAlpha(this.settings.color, percent), new View[]{this.mHeader.statusBackground});
        if(percent >= 1.0F) {
            Utils.setBackgroundColor(Utils.colorWithAlpha(this.settings.color, percent), new View[]{this.mHeader.toolbar, this.mHeader.toolbarLayoutBackground, this.mHeader.mPagerSlidingTabStrip});
        } else {
            Utils.setBackgroundColor(Utils.colorWithAlpha(this.settings.color, 0.0F), new View[]{this.mHeader.toolbar, this.mHeader.toolbarLayoutBackground, this.mHeader.mPagerSlidingTabStrip});
        }

        if(this.settings.enableToolbarElevation && this.toolbarJoinsTabs()) {
            Utils.setElevation(percent == 1.0F?this.elevation:0.0F, new View[]{this.mHeader.toolbar, this.mHeader.toolbarLayoutBackground, this.mHeader.mPagerSlidingTabStrip, this.mHeader.mLogo});
        }

    }

    private boolean toolbarJoinsTabs() {
        return (float)this.mHeader.toolbar.getBottom() ==
                (float)this.mHeader.mPagerSlidingTabStrip.getTop() +
                        ViewHelper.getTranslationY(this.mHeader.mPagerSlidingTabStrip);

    }

    private void followScrollToolbarLayout(float yOffset) {
        if(this.mHeader.toolbar.getBottom() != 0) {
            if(this.toolbarJoinsTabs()) {
                if(this.firstScrollValue == 1.4E-45F) {
                    this.firstScrollValue = yOffset;
                }

                ViewHelper.setTranslationY(this.mHeader.toolbarLayout, this.firstScrollValue - yOffset);
            } else {
                ViewHelper.setTranslationY(this.mHeader.toolbarLayout, 0.0F);
            }

            this.followScrollToolbarIsVisible = ViewHelper.getY(this.mHeader.toolbarLayout) >= 0.0F;
        }
    }

    private void animateEnterToolbarLayout(float yOffset) {
        if(!this.followScrollToolbarIsVisible && this.headerAnimator != null) {
            if(this.headerAnimator instanceof ObjectAnimator) {
                ((ObjectAnimator)this.headerAnimator).cancel();
            } else if(this.headerAnimator instanceof android.animation.ObjectAnimator) {
                ((android.animation.ObjectAnimator)this.headerAnimator).cancel();
            }

            this.headerAnimator = null;
        }

        if(this.headerAnimator == null) {
            if(Build.VERSION.SDK_INT > 10) {
                this.headerAnimator = android.animation.ObjectAnimator.ofFloat(this.mHeader.toolbarLayout, "translationY", new float[]{0.0F}).setDuration(600L);
                ((android.animation.ObjectAnimator)this.headerAnimator).addListener(new AnimatorListenerAdapter() {
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        MaterialViewPagerAnimator.this.followScrollToolbarIsVisible = true;
                    }
                });
                ((android.animation.ObjectAnimator)this.headerAnimator).start();
            } else {
                this.headerAnimator = ObjectAnimator.ofFloat(this.mHeader.toolbarLayout, "translationY", new float[]{0.0F}).setDuration(600L);
                ((ObjectAnimator)this.headerAnimator).addListener(new com.nineoldandroids.animation.AnimatorListenerAdapter() {
                    public void onAnimationEnd(com.nineoldandroids.animation.Animator animation) {
                        super.onAnimationEnd(animation);
                        MaterialViewPagerAnimator.this.followScrollToolbarIsVisible = true;
                    }
                });
                ((ObjectAnimator)this.headerAnimator).start();
            }

            this.headerYOffset = yOffset;
        }

    }

    public int getHeaderHeight() {
        return this.settings.headerHeight;
    }

    protected boolean isNewYOffset(int yOffset) {
        return this.lastYOffset == -1.0F?true:(float)yOffset != this.lastYOffset;
    }

    public void registerRecyclerView(final RecyclerView recyclerView, final RecyclerView.OnScrollListener onScrollListener) {
        if(recyclerView != null) {
            this.scrollViewList.add(recyclerView);
            this.yOffsets.put(recyclerView, Integer.valueOf(recyclerView.getScrollY()));
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                boolean firstZeroPassed;

                public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                    super.onScrollStateChanged(recyclerView, newState);
                    if(onScrollListener != null) {
                        onScrollListener.onScrollStateChanged(recyclerView, newState);
                    }

                }

                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    if(onScrollListener != null) {
                        onScrollListener.onScrolled(recyclerView, dx, dy);
                    }

                    int yOffset = ((Integer)MaterialViewPagerAnimator.this.yOffsets.get(recyclerView)).intValue();
                    yOffset += dy;
                    MaterialViewPagerAnimator.this.yOffsets.put(recyclerView, Integer.valueOf(yOffset));
                    if(yOffset == 0 && !this.firstZeroPassed) {
                        this.firstZeroPassed = true;
                    } else {
                        if(MaterialViewPagerAnimator.this.isNewYOffset(yOffset)) {
                            MaterialViewPagerAnimator.this.onMaterialScrolled(recyclerView, (float)yOffset);
                        }

                    }
                }
            });
            recyclerView.post(new Runnable() {
                public void run() {
                    MaterialViewPagerAnimator.this.setScrollOffset(recyclerView, MaterialViewPagerAnimator.this.lastYOffset);
                }
            });
        }

    }

    public void registerScrollView(final ObservableScrollView scrollView, final ObservableScrollViewCallbacks observableScrollViewCallbacks) {
        if(scrollView != null) {
            this.scrollViewList.add(scrollView);
            if(scrollView.getParent() != null && scrollView.getParent().getParent() != null && scrollView.getParent().getParent() instanceof ViewGroup) {
                scrollView.setTouchInterceptionViewGroup((ViewGroup)scrollView.getParent().getParent());
            }

            scrollView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
                boolean firstZeroPassed;

                public void onScrollChanged(int yOffset, boolean b, boolean b2) {
                    if(observableScrollViewCallbacks != null) {
                        observableScrollViewCallbacks.onScrollChanged(yOffset, b, b2);
                    }

                    if(yOffset == 0 && !this.firstZeroPassed) {
                        this.firstZeroPassed = true;
                    } else {
                        if(MaterialViewPagerAnimator.this.isNewYOffset(yOffset)) {
                            MaterialViewPagerAnimator.this.onMaterialScrolled(scrollView, (float)yOffset);
                        }

                    }
                }

                public void onDownMotionEvent() {
                    if(observableScrollViewCallbacks != null) {
                        observableScrollViewCallbacks.onDownMotionEvent();
                    }

                }

                public void onUpOrCancelMotionEvent(ScrollState scrollState) {
                    if(observableScrollViewCallbacks != null) {
                        observableScrollViewCallbacks.onUpOrCancelMotionEvent(scrollState);
                    }

                }
            });
            scrollView.post(new Runnable() {
                public void run() {
                    MaterialViewPagerAnimator.this.setScrollOffset(scrollView, MaterialViewPagerAnimator.this.lastYOffset);
                }
            });
        }

    }

    public void registerWebView(final ObservableWebView webView, final ObservableScrollViewCallbacks observableScrollViewCallbacks) {
        if(webView != null) {
            if(this.scrollViewList.isEmpty()) {
                this.onMaterialScrolled(webView, (float)webView.getCurrentScrollY());
            }

            this.scrollViewList.add(webView);
            webView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
                public void onScrollChanged(int yOffset, boolean b, boolean b2) {
                    if(observableScrollViewCallbacks != null) {
                        observableScrollViewCallbacks.onScrollChanged(yOffset, b, b2);
                    }

                    if(MaterialViewPagerAnimator.this.isNewYOffset(yOffset)) {
                        MaterialViewPagerAnimator.this.onMaterialScrolled(webView, (float)yOffset);
                    }

                }

                public void onDownMotionEvent() {
                    if(observableScrollViewCallbacks != null) {
                        observableScrollViewCallbacks.onDownMotionEvent();
                    }

                }

                public void onUpOrCancelMotionEvent(ScrollState scrollState) {
                    if(observableScrollViewCallbacks != null) {
                        observableScrollViewCallbacks.onUpOrCancelMotionEvent(scrollState);
                    }

                }
            });
            this.setScrollOffset(webView, -this.lastYOffset);
        }

    }

    public void restoreScroll(float scroll, MaterialViewPagerSettings settings) {
        this.settings = settings;
        this.onMaterialScrolled((Object)null, scroll);
    }

    public void onViewPagerPageChanged() {
        this.scrollDown(this.lastYOffset);
    }
}

