package com.githioch.the_looker.util;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.githioch.the_looker.R;
import com.githioch.the_looker.model.Image;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;


public class ImagePickerActivity extends ActionBarActivity {

    /**
     * Key to persist the list when saving the state of the activity.
     */
    private static final String KEY_LIST = "nl.changer.polypicker.savedinstance.key.list";

    /**
     * Returns the parcelled image uris in the intent with this extra.
     */
    public static final String EXTRA_IMAGE_URIS = "nl.changer.changer.nl.polypicker.extra.selected_image_uris";

    private Set<Image> mSelectedImages;
//    private LinearLayout mSelectedImagesContainer;
//    protected TextView mSelectedImageEmptyMessage;

    private ViewPager mViewPager;
    public ImageInternalFetcher mImageFetcher;
    private android.support.v7.app.ActionBar menu;


    private SlidingTabText mSlidingTabText;

    // initialize with default config.
    private static Config mConfig = new Config.Builder().build();

    public static void setConfig(Config config) {

        if (config == null) {
            throw new NullPointerException("Config cannot be passed null. Not setting config will use default values.");
        }

        mConfig = config;
    }

    public static Config getConfig() {
        return mConfig;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_pp);

        /*Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();

        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
            menu.setTitle("");
        }*/

       // Dont enable the toolbar.
       // Consumes a lot of space in the UI unnecessarily.
       Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            menu = getSupportActionBar();
            if (menu != null) {
                menu.setDisplayHomeAsUpEnabled(true);
                menu.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
                menu.setTitle("");
            }
        }

//        mSelectedImagesContainer = (LinearLayout) findViewById(R.id.selected_photos_container);
//        mSelectedImageEmptyMessage = (TextView) findViewById(R.id.selected_photos_empty);
        mViewPager = (ViewPager) findViewById(R.id.pager);


        mSelectedImages = new HashSet<Image>();
        mImageFetcher = new ImageInternalFetcher(this, 500);


        setupActionBar();
        if (savedInstanceState != null) {
            populateUi(savedInstanceState);
        }
    }

    private void populateUi(Bundle savedInstanceState) {
        ArrayList<Image> list = savedInstanceState.getParcelableArrayList(KEY_LIST);

        if (list != null) {
            for (Image image : list) {
                addImage(image);
            }
        }
    }

    /**
     * Sets up the action bar, adding view page indicator.
     */
    private void setupActionBar() {
       /*final ActionBar actionBar = getActionBar();

        if (actionBar == null) {
            return;
        }

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);

        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            actionBar.addTab(actionBar.newTab().setText(mSectionsPagerAdapter.getPageTitle(i)).setTabListener(this));
        }*/

        mSlidingTabText = (SlidingTabText) findViewById(R.id.sliding_tabs);
        mSlidingTabText.setSelectedIndicatorColors(getResources().getColor(mConfig.getTabSelectionIndicatorColor()));
        mSlidingTabText.setCustomTabView(R.layout.tab_view_text, R.id.tab_icon);
        mSlidingTabText.setTabStripColor(mConfig.getTabBackgroundColor());
        mViewPager.setAdapter(new PagerAdapter2Fragments(getFragmentManager()));
        mSlidingTabText.setTabTitles(getResources().getStringArray(R.array.tab_titles));
        mSlidingTabText.setViewPager(mViewPager);
    }

    public boolean addImage(Image image) {

        if (mSelectedImages == null) {
            // this condition may arise when the activity is being
            // restored when sufficient memory is available. onRestoreState()
            // will be called.
            mSelectedImages = new HashSet<Image>();
        }
        mSelectedImages.clear();

        if (mSelectedImages.add(image)) {
            return true;
        }
        return false;
    }

    public boolean removeImage(Image image) {
        if (mSelectedImages.remove(image)) {
            /*for (int i = 0; i < mSelectedImagesContainer.getChildCount(); i++) {
                View childView = mSelectedImagesContainer.getChildAt(i);
                if (childView.getTag().equals(image.mUri)) {
                    mSelectedImagesContainer.removeViewAt(i);
                    break;
                }
            }*/

            if (mSelectedImages.size() == 0) {
//                mSelectedImagesContainer.setVisibility(View.GONE);
//                mSelectedImageEmptyMessage.setVisibility(View.VISIBLE);
            }
            return true;
        }
        return false;
    }

    public boolean containsImage(Image image) {
        return mSelectedImages.contains(image);
    }

    public boolean oneImageAlreadySelected(Image image){
        if(mSelectedImages.size() == 1){
            return true;
        }
        return false;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        // preserve already taken images on configuration changes like
        // screen rotation or activity run out of memory.
        // HashSet cannot be saved, so convert to list and then save.
        ArrayList<Image> list = new ArrayList<Image>(mSelectedImages);
        outState.putParcelableArrayList(KEY_LIST, list);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        populateUi(savedInstanceState);
    }

    public void doneClicked(View view) {

        if (view.getId() == R.id.action_btn_done) {

            Uri[] uris = new Uri[mSelectedImages.size()];
            int i = 0;
            for (Image img : mSelectedImages) {
                uris[i++] = img.mUri;
            }

            Intent intent = new Intent();
            intent.putExtra(EXTRA_IMAGE_URIS, uris);
            intent.putExtra("gallery", "true");
            setResult(Activity.RESULT_OK, intent);
        } else if (view.getId() == R.id.action_btn_cancel) {
            setResult(Activity.RESULT_CANCELED);
        }
        finish();

    }

    public void doneClicked() {

    Uri[] uris = new Uri[mSelectedImages.size()];
    int i = 0;
    for (Image img : mSelectedImages) {
        uris[i++] = img.mUri;
        Log.d("uris clicked", String.valueOf(img.mUri));
    }

    Intent intent = new Intent();
    intent.putExtra(EXTRA_IMAGE_URIS, uris);
    setResult(Activity.RESULT_OK, intent);

    finish();

    }
}