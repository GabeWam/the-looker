package com.githioch.the_looker.util;

/**
 * Created by Githioch on 6/29/2015.
 */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ObservableWebView;
import com.nineoldandroids.animation.ObjectAnimator;

import java.util.concurrent.ConcurrentHashMap;


public class MaterialViewPagerHelper {
    private static ConcurrentHashMap<Object, MaterialViewPagerAnimator> hashMap = new ConcurrentHashMap();

    public MaterialViewPagerHelper() {
    }

    public static void register(Context context, MaterialViewPagerAnimator animator) {
        hashMap.put(context, animator);
    }

    public static void unregister(Context context) {
        if(context != null) {
            hashMap.remove(context);
        }

    }

    public static void registerRecyclerView(Activity activity, RecyclerView recyclerView, RecyclerView.OnScrollListener onScrollListener) {
        if(activity != null && hashMap.containsKey(activity)) {
           MaterialViewPagerAnimator animator = (MaterialViewPagerAnimator)hashMap.get(activity);
            if(animator != null) {
                animator.registerRecyclerView(recyclerView, onScrollListener);
            }
        }

    }

    public static void registerWebView(Activity activity, ObservableWebView webView, ObservableScrollViewCallbacks observableScrollViewCallbacks) {
        if(activity != null && hashMap.containsKey(activity)) {
            MaterialViewPagerAnimator animator = (MaterialViewPagerAnimator)hashMap.get(activity);
            if(animator != null) {
                animator.registerWebView(webView, observableScrollViewCallbacks);
            }
        }

    }

    public static void registerScrollView(Activity activity, ObservableScrollView mScrollView, ObservableScrollViewCallbacks observableScrollViewCallbacks) {
        if(activity != null && hashMap.containsKey(activity)) {
            MaterialViewPagerAnimator animator = (MaterialViewPagerAnimator)hashMap.get(activity);
            if(animator != null) {
                animator.registerScrollView(mScrollView, observableScrollViewCallbacks);
            }
        }

    }

    public static MaterialViewPagerAnimator getAnimator(Context context) {
        return (MaterialViewPagerAnimator)hashMap.get(context);
    }

    private static void webViewLoadJS(WebView webView, String js) {
        if(Build.VERSION.SDK_INT >= 19) {
            webView.evaluateJavascript(js, (ValueCallback)null);
        } else {
            webView.loadUrl("javascript: " + js);
        }

    }

    public static void injectHeader(final WebView webView, boolean withAnimation) {
        if(webView != null) {
            MaterialViewPagerAnimator animator = getAnimator(webView.getContext());
            if(animator != null) {
                WebSettings webSettings = webView.getSettings();
                webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
                webSettings.setCacheMode(2);
                webSettings.setJavaScriptEnabled(true);
                webSettings.setDomStorageEnabled(true);
                if(Build.VERSION.SDK_INT >= 11) {
                    webView.setLayerType(1, (Paint)null);
                }

                int js = animator.getHeaderHeight() + 10;
                String js1 = String.format("document.body.style.marginTop= \"%dpx\"", new Object[]{Integer.valueOf(js)});
                webViewLoadJS(webView, js1);
                String js2 = "document.body.style.backround-color= white";
                webViewLoadJS(webView, "document.body.style.backround-color= white");
                if(withAnimation) {
                    webView.postDelayed(new Runnable() {
                        public void run() {
                            webView.setVisibility(0);
                            ObjectAnimator.ofFloat(webView, "alpha", new float[]{0.0F, 1.0F}).start();
                        }
                    }, 400L);
                }
            }
        }

    }

    public static void preLoadInjectHeader(WebView mWebView) {
        mWebView.setBackgroundColor(0);
        mWebView.setVisibility(4);
    }
}

