package com.githioch.the_looker.util;

/**
 * Created by Githioch on 6/29/2015.
 */
//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.flaviofaria.kenburnsview.KenBurnsView;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorListenerAdapter;
import com.nineoldandroids.animation.ObjectAnimator;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;


public class MaterialViewPagerImageHeader extends KenBurnsView {
    public MaterialViewPagerImageHeader(Context context) {
        super(context);
    }

    public MaterialViewPagerImageHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MaterialViewPagerImageHeader(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setImageUrl(final String urlImage, final int fadeDuration) {
        final float alpha = this.getAlpha();
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(this, "alpha", new float[]{0.0F}).setDuration((long)fadeDuration);
        fadeOut.setInterpolator(new DecelerateInterpolator());
        fadeOut.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                Picasso.with(MaterialViewPagerImageHeader.this.getContext()).load(urlImage).centerCrop().fit().into(MaterialViewPagerImageHeader.this, new Callback() {
                    public void onSuccess() {
                        ObjectAnimator fadeIn = ObjectAnimator.ofFloat(MaterialViewPagerImageHeader.this, "alpha", new float[]{alpha}).setDuration((long)fadeDuration);
                        fadeIn.setInterpolator(new AccelerateInterpolator());
                        fadeIn.start();
                    }

                    public void onError() {
                    }
                });
            }
        });
        fadeOut.start();
    }

    public void setImageDrawable(final Drawable drawable, final int fadeDuration) {
        final float alpha = this.getAlpha();
        ObjectAnimator fadeOut = ObjectAnimator.ofFloat(this, "alpha", new float[]{0.0F}).setDuration((long)fadeDuration);
        fadeOut.setInterpolator(new DecelerateInterpolator());
        fadeOut.addListener(new AnimatorListenerAdapter() {
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                MaterialViewPagerImageHeader.this.setImageDrawable(drawable);
                ObjectAnimator fadeIn = ObjectAnimator.ofFloat(MaterialViewPagerImageHeader.this, "alpha", new float[]{alpha}).setDuration((long)fadeDuration);
                fadeIn.setInterpolator(new AccelerateInterpolator());
                fadeIn.start();
            }
        });
        fadeOut.start();
    }
}

