package com.githioch.the_looker;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.githioch.the_looker.util.ConnectionDetector;
import com.githioch.the_looker.util.ProfilePictureViewNavigationDrawer;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.ParseUser;
import com.pkmmte.view.CircularImageView;

/**
 * Created by Githioch on 7/10/2015.
 */
public class ProfileActivity extends AppCompatActivity{

    private android.support.v7.app.ActionBar menu;
    private FrameLayout frameLayout;
    private ImageView largeImage;
    private RelativeLayout topLayout;
    private TextView profileName;
    private CircularImageView circularImageView;
    private ImageLoader imageLoader;

    public void displayLargeImage(Bitmap bitmap){
        largeImage.setImageBitmap(bitmap);
        largeImage.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.GONE);
        topLayout.setVisibility(View.GONE);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(largeImage.getVisibility() == View.VISIBLE){
            largeImage.setVisibility(View.GONE);
            frameLayout.setVisibility(View.VISIBLE);
            topLayout.setVisibility(View.VISIBLE);

        }else{
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(item));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        imageLoader = ImageLoader.getInstance();

        if(ParseUser.getCurrentUser().getJSONObject("profile") != null){
            setContentView(R.layout.activity_profile_new_facebook);
            profileName = (TextView)findViewById(R.id.profile_name);
            ProfilePictureViewNavigationDrawer profilePictureView =
                    (ProfilePictureViewNavigationDrawer)findViewById(R.id.profilePic);
            profilePictureView.setStrokeWidth(10f);
            profilePictureView.setProfileId(ParseUser.getCurrentUser().getJSONObject("profile")
                    .optString("facebookId"));
            profileName.setText(ParseUser.getCurrentUser().getJSONObject("profile").optString("name"));
        }else{
            setContentView(R.layout.activity_profile_new_parse);
            profileName = (TextView)findViewById(R.id.profile_name);
            circularImageView = (CircularImageView)findViewById(R.id.profilePic);
            loadParseImage();
            profileName.setText(ParseUser.getCurrentUser().getString("name"));
        }

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();

        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setHomeAsUpIndicator(R.drawable.ic_back_arrow);
            menu.setTitle("");
        }

        frameLayout = (FrameLayout)findViewById(R.id.frame_main);
        topLayout = (RelativeLayout)findViewById(R.id.profile_top_layout);
        largeImage = (ImageView)findViewById(R.id.feed_large_image);

        RecyclerViewFragment recyclerViewFragment = new RecyclerViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("profile", profileName.getText().toString());
        recyclerViewFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().
                add(R.id.frame_main, recyclerViewFragment)
                .commit();


    }

    private void loadParseImage() {
        if(ParseUser.getCurrentUser().getParseFile("image") != null &&
                new ConnectionDetector(this).isConnectingToInternet()){
            String url = ParseUser.getCurrentUser().getParseFile("image").getUrl();
            imageLoader.loadImage(url, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {
                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    circularImageView.setImageBitmap(bitmap);
                    circularImageView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
        }else{
            circularImageView.setImageResource(R.drawable.no_profile);
        }



    }
}
