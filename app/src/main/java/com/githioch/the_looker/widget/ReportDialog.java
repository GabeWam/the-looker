package com.githioch.the_looker.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.githioch.the_looker.util.ConnectionDetector;
import com.githioch.the_looker.util.SimpleLocation;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;

/**
 * Created by Githioch on 7/29/2015.
 */
public class ReportDialog extends Activity{

    // LogCat tag
    private static final String TAG = ReportDialog.class.getSimpleName();
    private SimpleLocation location;

    private void showReportDialog(Context context) {
        AlertDialog alertDialog;

        final String[] items = {"Theft","Break In"};
        // arrayList to keep the selected items
        final ArrayList<String> selectedItems = new ArrayList();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Threat");
        builder.setMultiChoiceItems(items, null,
                new DialogInterface.OnMultiChoiceClickListener() {
                    // indexSelected contains the index of item (of which checkbox checked)
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected,
                                        boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            // write your code when user checked the checkbox
                            selectedItems.add(items[indexSelected]);
                        } else if (selectedItems.contains(items[indexSelected])) {
                            // Else, if the item is already in the array, remove it
                            // write your code when user Unchecked the checkbox
                            selectedItems.remove(items[indexSelected]);
                        }
                    }
                })
                // Set the action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on OK
                        //  You can write the code  to save the selected item here
                        final double latitude = location.getLatitude();
                        final double longitude = location.getLongitude();

                        Log.d(TAG, latitude + ", " + longitude);

                        ParseObject parseObject = new ParseObject("Panic_Button_Reports");
                        if (ParseUser.getCurrentUser() != null) {
                            if (ParseUser.getCurrentUser().getString("name") != null) {
                                parseObject.put("userName", ParseUser.getCurrentUser().getString("name"));
                            } else {
                                if (ParseUser.getCurrentUser().has("profile") &&
                                        ParseUser.getCurrentUser().getJSONObject("profile") != null) {
                                    parseObject.put("userName", ParseUser.getCurrentUser()
                                            .getJSONObject("profile").optString("facebookName"));
                                }
                            }
                        }
                        parseObject.put("userId", ParseUser.getCurrentUser().getObjectId());
                        parseObject.put("latitude", latitude);
                        parseObject.put("longitude", longitude);
                        if (selectedItems.size() > 0) {
                            for (int i = 0; i < selectedItems.size(); i++) {
                                parseObject.put(selectedItems.get(i).replaceAll(" ","_"), true);
                            }
                            if (new ConnectionDetector(getApplicationContext()).isConnectingToInternet()) {
                                parseObject.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        Toast.makeText(getApplicationContext(),
                                                "Report sent successfully", Toast.LENGTH_SHORT)
                                                .show();
                                        ReportDialog.this.finish();
                                    }
                                });
                            } else {
                                Toast.makeText(getApplicationContext(),
                                        "Please connect to the Internet and try again.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            Toast.makeText(getApplicationContext(),
                                    "No item selected", Toast.LENGTH_SHORT)
                                    .show();
                            ReportDialog.this.finish();
                        }

                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel
                        for (int i = 0; i < selectedItems.size(); i++) {
                            Log.d(String.valueOf(i), selectedItems.get(i));
                        }
                        dialog.dismiss();
                        ReportDialog.this.finish();

                    }
                });

        alertDialog = builder.create();//AlertDialog dialog; create like this outside onClick
        alertDialog.show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        showReportDialog(this);

        // construct a new instance of SimpleLocation
        location = new SimpleLocation(this);

        // if we can't access the location yet
        if (!location.hasLocationEnabled()) {
            // ask the user to enable location access
            SimpleLocation.openSettings(this);
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        location.endUpdates();
    }


    @Override
    protected void onResume() {
        super.onResume();
        location.beginUpdates();
    }

}
