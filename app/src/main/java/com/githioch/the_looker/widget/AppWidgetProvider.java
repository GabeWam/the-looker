package com.githioch.the_looker.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.githioch.the_looker.R;

/**
 * Created by Githioch on 7/29/2015.
 */
public class AppWidgetProvider extends android.appwidget.AppWidgetProvider{

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        for (int i = 0; i < appWidgetIds.length; i++) {
            int appWidgetId = appWidgetIds[i];

            Intent intent = new Intent(context, ReportDialog.class);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.panic_button);
            views.setOnClickPendingIntent(R.id.imageButton, pendingIntent);

            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

}
