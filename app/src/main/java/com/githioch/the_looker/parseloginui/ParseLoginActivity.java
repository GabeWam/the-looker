package com.githioch.the_looker.parseloginui;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.githioch.the_looker.MainActivity;
import com.githioch.the_looker.R;
import com.parse.Parse;
import com.parse.ParseFacebookUtils;


/**
 * Created by Githioch on 6/2/2015.
 */
public class ParseLoginActivity extends FragmentActivity implements
        ParseLoginFragment.ParseLoginFragmentListener,
        ParseLoginHelpFragment.ParseOnLoginHelpSuccessListener,
        ParseOnLoginSuccessListener, ParseOnLoadingListener, ParseOnSignUpSuccessListener {

    private static final String PREF_FILE_NAME = "facebookPref";
    public static final String KEY_USER_HAS_SET_COUNTY_AND_AREA = "user_has_set_county_and_area";
    private boolean mUserHasSetCountyAndArea;

    public static final String LOG_TAG = "ParseLoginActivity";

    // All login UI fragment transactions will happen within this parent layout element.
    // Change this if you are modifying this code to be hosted in your own activity.
    private final int fragmentContainer = android.R.id.content;

    private ProgressDialog progressDialog;
    private Bundle configOptions;

    // Although Activity.isDestroyed() is in API 17, we implement it anyways for older versions.
    private boolean destroyed = false;


    public static String readFromPreferences(Context context, String preferenceName, String defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUserHasSetCountyAndArea = Boolean.valueOf(readFromPreferences(this,
                KEY_USER_HAS_SET_COUNTY_AND_AREA, "false"));

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        // Combine options from incoming intent and the activity metadata
        configOptions = getMergedOptions();

        // Show the login form
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().add(fragmentContainer,
                    ParseLoginFragment.newInstance(configOptions)).commit();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        destroyed = true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Required for making Facebook login work
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Called when the user clicked the sign up button on the login form.
     */
    @Override
    public void onSignUpClicked(String username, String password) {
        // Show the signup form, but keep the transaction on the back stack
        // so that if the user clicks the back button, they are brought back
        // to the login form.
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(fragmentContainer,
                ParseSignupFragment.newInstance(configOptions, username, password));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * Called when the user clicked the log in button on the login form.
     */
    @Override
    public void onLoginHelpClicked() {
        // Show the login help form for resetting the user's password.
        // Keep the transaction on the back stack so that if the user clicks
        // the back button, they are brought back to the login form.
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(fragmentContainer, ParseLoginHelpFragment.newInstance(configOptions));
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * Called when the user successfully completes the login help flow.
     */
    @Override
    public void onLoginHelpSuccess() {
        // Display the login form, which is the previous item on the stack
        getSupportFragmentManager().popBackStackImmediate();

    }

    /**
     * Called when the user successfully logs in or signs up.
     */
    @Override
    public void onLoginSuccess() {
        // This default implementation returns to the parent activity with
        // RESULT_OK.
        // You can change this implementation if you want a different behavior.
        /*setResult(RESULT_OK);
        finish();*/

        if (!mUserHasSetCountyAndArea) {
            Intent intent = new Intent(getApplicationContext(),
                    SelectCountyActivity.class);
            startActivity(intent);
            Toast.makeText(getApplicationContext()
                    , "Login successful", Toast.LENGTH_SHORT).show();
        }else{
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            Toast.makeText(this, "Login successful", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Called when we are in progress retrieving some data.
     *
     * @param showSpinner
     *     Whether to show the loading dialog.
     */
    @Override
    public void onLoadingStart(boolean showSpinner) {
        if (showSpinner) {
            progressDialog = ProgressDialog.show(this, null,
                    getString(R.string.com_parse_ui_progress_dialog_text), true, false);
        }
    }

    /**
     * Called when we are finished retrieving some data.
     */
    @Override
    public void onLoadingFinish() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /**
     * @see android.app.Activity#isDestroyed()
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public boolean isDestroyed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return super.isDestroyed();
        }
        return destroyed;
    }

    private Bundle getMergedOptions() {
        // Read activity metadata from AndroidManifest.xml
        ActivityInfo activityInfo = null;
        try {
            activityInfo = getPackageManager().getActivityInfo(
                    this.getComponentName(), PackageManager.GET_META_DATA);
        } catch (PackageManager.NameNotFoundException e) {
            if (Parse.getLogLevel() <= Parse.LOG_LEVEL_ERROR &&
                    Log.isLoggable(LOG_TAG, Log.WARN)) {
                Log.w(LOG_TAG, e.getMessage());
            }
        }

        // The options specified in the Intent (from ParseLoginBuilder) will
        // override any duplicate options specified in the activity metadata
        Bundle mergedOptions = new Bundle();
        if (activityInfo != null && activityInfo.metaData != null) {
            mergedOptions.putAll(activityInfo.metaData);
        }
        if (getIntent().getExtras() != null) {
            mergedOptions.putAll(getIntent().getExtras());
        }

        return mergedOptions;
    }

    @Override
    public void onSignUpSuccess() {
        Intent intent = new Intent(this, SelectCountyActivity.class);
        startActivity(intent);
    }
}


