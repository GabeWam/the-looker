package com.githioch.the_looker.parseloginui;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.githioch.the_looker.R;
import com.parse.ParseUser;


/**
 * Created by Githioch on 6/9/2015.
 * Activity for selecting county. This comes right after user has regsitered and is now being
 * asked to specify their county
 */
public class SelectCountyActivity extends AppCompatActivity {

    private android.support.v7.app.ActionBar menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_county);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        Button ok = (Button)findViewById(R.id.btn_region_ok);
        TextView nairobiCountyNowAvailable = (TextView)findViewById(R.id.text_now_available);

        String text = "<font color=#212121>Nairobi County </font> <font color=#C8E6C9>now available</font>";
        nairobiCountyNowAvailable.setText(Html.fromHtml(text));

        Typeface typeface = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        nairobiCountyNowAvailable.setTypeface(typeface);

        final Spinner spinner = (Spinner)findViewById(R.id.spinner_select_region);

        String[] arraySpinner = new String[] {"Nairobi"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String spinnerSelection = spinner.getSelectedItem().toString();
                if (!spinnerSelection.equals("Nairobi")) {
                    Dfragment dfragment = new Dfragment();
                    dfragment.show(getFragmentManager(), "Alert Dialog");
                }

                if (spinnerSelection.equals("Nairobi")) {
                    Intent intent = new Intent(SelectCountyActivity.this, SelectAreaActivity.class);
                    intent.putExtra("county","Nairobi");
                    startActivity(intent);


                    /*Intent intent = new Intent(SelectCountyActivity.this, SelectAreaActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("restart", "restart");
                    startActivity(intent);
                    Toast.makeText(SelectCountyActivity.this, "Profile Created", Toast.LENGTH_SHORT)
                            .show();*/
                }

                if (spinnerSelection.equals("Select Region")) {
                    Toast.makeText(SelectCountyActivity.this, "Please select region.", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();
        if (menu != null) {
            menu.setTitle("Select County");
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(ParseUser.getCurrentUser() != null && ParseUser.getCurrentUser().has("profile")){
            Toast.makeText(getApplicationContext(), "Already logged in with facebook. Complete" +
                    " sign up process first", Toast.LENGTH_SHORT).show();
        }else{
            super.onBackPressed();
        }
    }


    public static class Dfragment extends DialogFragment{


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            return new AlertDialog.Builder(getActivity())

                            // Set Dialog Title
                    .setTitle("Coming Soon!")
                            // Set Dialog Message
                    .setMessage("We are not in your region yet. Coming soon!")

                            // Positive button
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Do something else
                        }
                    })

                    /*        // Negative Button
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,	int which) {
                            // Do something else
                        }
                    })*/.create();
        }
    }
}
