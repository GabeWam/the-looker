package com.githioch.the_looker.parseloginui;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.githioch.the_looker.MainActivity;
import com.githioch.the_looker.R;
import com.github.paolorotolo.appintro.AppIntro;

/**
 * Created by Githiora Wamunyu on 9/23/2015.
 */
public class TutorialActivity extends AppIntro{

    private Button mButton;

    private void clearBackStackAndOpenMain() {
        Toast.makeText(TutorialActivity.this, "Profile Created", Toast.LENGTH_SHORT)
                .show();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }


    @Override
    public void init(Bundle bundle) {

        addSlide(SampleSlide.newInstance(R.drawable.tutorial_woodley, R.color.green));
        addSlide(SampleSlide.newInstance(R.drawable.tutorial_all_areas, R.color.blue));
        addSlide(SampleSlide.newInstance(R.drawable.tutorial_news, R.color.orange));
        addSlide(SampleSlide.newInstance(R.drawable.tutorial_reply,R.color.green));
        addSlide(SampleSlide.newInstance(R.drawable.tutorial_sharing, R.color.blue));
        addSlide(SampleSlide.newInstance(R.drawable.tutorial_gotea, R.color.orange));
        addSlide(SampleSlide.newInstance(R.drawable.tutorial_post_write, R.color.green));
        addSlide(SampleSlide.newInstance(R.drawable.tutorial_post_image, R.color.blue));
        addSlide(SampleSlide.newInstance(R.drawable.tutorial_visiting, R.color.orange));
        addSlide(SampleSlide.newInstance(R.drawable.tutorial_feedback, R.color.green));

        showSkipButton(false);
        showDoneButton(true);

        setDepthAnimation();

        setVibrate(true);
        setVibrateIntensity(30);

    }

    @Override
    public void onSkipPressed() {

    }

    @Override
    public void onDonePressed() {
        clearBackStackAndOpenMain();
    }

}
