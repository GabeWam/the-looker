package com.githioch.the_looker.parseloginui;

/**
 * Created by Githioch on 6/9/2015.
 */
public interface ParseOnSignUpSuccessListener {
    void onSignUpSuccess();
}
