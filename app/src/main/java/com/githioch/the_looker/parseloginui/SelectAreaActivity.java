package com.githioch.the_looker.parseloginui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.githioch.the_looker.MainActivity;
import com.githioch.the_looker.R;
import com.parse.ParsePush;
import com.parse.ParseUser;

/**
 * Created by Githioch on 6/20/2015.
 * Activity for selecting area. Comes right after selecting county and is the last step in
 * registration
 */
public class SelectAreaActivity extends AppCompatActivity{
    private android.support.v7.app.ActionBar menu;
    private String county;
    private static final String PREF_FILE_NAME = "facebookPref";
    public static final String KEY_USER_HAS_SET_COUNTY_AND_AREA = "user_has_set_county_and_area";
    private boolean mUserHasSetCountyAndArea;
    private Spinner spinnerPrivateSecurity;
    private EditText customerId;
    private EditText phoneNumberEditText;
    private TextView yourLocation;
    private Spinner spinnerGeneralAreas;
    private LinearLayout otherLocation;

    public static void saveToSharedPreferences(Context context, String preferenceName, String preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(item));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        mUserHasSetCountyAndArea = Boolean.valueOf(readFromPreferences(this,
                KEY_USER_HAS_SET_COUNTY_AND_AREA, "false"));

        if(getIntent().getExtras() != null){
            county = getIntent().getStringExtra("county");
        }
        setContentView(R.layout.activity_select_area);

        yourLocation = (TextView)findViewById(R.id.area_text_your_location);
        otherLocation  = (LinearLayout)findViewById(R.id.area_select_other_area);

        final LinearLayout linearLayoutCustomerId = (LinearLayout)findViewById(R.id.area_linear_customer_id);
        final LinearLayout linearLayoutPhoneNumber = (LinearLayout)findViewById(R.id.area_linear_phone_number);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        Button ok = (Button)findViewById(R.id.btn_region_ok);
        TextView almostDone = (TextView)findViewById(R.id.text_almost_done);

        final Spinner spinner = (Spinner)findViewById(R.id.spinner_select_area);
        spinnerPrivateSecurity = (Spinner)findViewById(R.id.area_private_security_company);
        spinnerGeneralAreas = (Spinner)findViewById(R.id.spinner_select_other_area);

        customerId = (EditText)findViewById(R.id.area_customer_id);
        phoneNumberEditText = (EditText)findViewById(R.id.area_phone_number);
        TextView whyWeAsk = (TextView)findViewById(R.id.area_why_we_ask);
        TextView whyWeAskPhone = (TextView)findViewById(R.id.area_why_we_ask_phone_number);

        String[] arraySpinner = getResources().getStringArray(R.array.areas);
        String[] arraySpinnerPrivateSecurity = getResources().getStringArray(R.array.security_companies);
        String[] arrayOtherAreas = getResources().getStringArray(R.array.other_areas);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.simple_spinner_item, arraySpinner);
        ArrayAdapter<String> adapterPrivateSecurity = new ArrayAdapter<String>(this,
                R.layout.simple_spinner_item, arraySpinnerPrivateSecurity);
        ArrayAdapter<String> adapterOtherAreas = new ArrayAdapter<String>(this,
                R.layout.simple_spinner_item, arrayOtherAreas);

        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        adapterPrivateSecurity.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        adapterOtherAreas.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

        Typeface typeface = Typeface.createFromAsset(getAssets(), "Roboto-Regular.ttf");
        almostDone.setTypeface(typeface);

        underlineText(whyWeAsk);
        underlineText(whyWeAskPhone);

        whyWeAsk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogWithReason(R.string.why_we_ask);
            }
        });
        whyWeAskPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogWithReason(R.string.why_we_ask_phone);
            }
        });

        spinnerGeneralAreas.setAdapter(adapterOtherAreas);
        spinnerGeneralAreas.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = spinnerGeneralAreas.getSelectedItem().toString();

                if(item.equals("Can't find my location")){
                    yourLocation.setVisibility(View.VISIBLE);
                    yourLocation.requestFocus();
                }else{
                    yourLocation.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerPrivateSecurity.setAdapter(adapterPrivateSecurity);
        spinnerPrivateSecurity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String item = spinnerPrivateSecurity.getSelectedItem().toString();

                if (!item.equals("No") && !item.equals("Maybe Later")) {
                    linearLayoutCustomerId.setVisibility(View.VISIBLE);
                } else {
                    linearLayoutCustomerId.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String item = spinner.getSelectedItem().toString();

                if(item.equals("Other")){
                    otherLocation.setVisibility(View.VISIBLE);
                }else{
                    otherLocation.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String area = "";

                if(yourLocation.getVisibility() == View.VISIBLE){
                    area = yourLocation.getText().toString();
                    if(area.equals("") || area.equals("Other")){
                        Toast.makeText(getApplicationContext(), "Please enter your location name before proceeding",
                                Toast.LENGTH_SHORT).show();
                        return;
                    }

                }else{
                    if(otherLocation.getVisibility() == View.VISIBLE){
                        area = spinnerGeneralAreas.getSelectedItem().toString();
                    }else{
                        area = spinner.getSelectedItem().toString();
                    }
                }

                confirmChoiceDialog(area);

            }
        });

        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();
        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setTitle("Select Area");
        }

    }

    private void confirmChoiceDialog(final String area) {
        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(SelectAreaActivity.this);

        builder.setTitle("Confirm choices")
                .setMessage("Ensure that the following choices are correct.\n\nArea - " + area)
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        saveCountyAndAreaInServer(county, area);

                        if (!mUserHasSetCountyAndArea) {
                            mUserHasSetCountyAndArea = true;
                            saveToSharedPreferences(getApplicationContext(),
                                    KEY_USER_HAS_SET_COUNTY_AND_AREA, "true");
                        }
                    }
                });

        alertDialog = builder.create();//AlertDialog dialog; create like this outside onClick
        alertDialog.show();
    }

    private void underlineText(TextView whyWeAsk) {
        SpannableString content = new SpannableString(whyWeAsk.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, whyWeAsk.getText().length(), 0);
        whyWeAsk.setText(content);
    }

    private void showDialogWithReason(int why_we_ask) {
        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder
                .setTitle("Why we ask")
                .setMessage(why_we_ask)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        alertDialog = builder.create();//AlertDialog dialog; create like this outside onClick
        alertDialog.show();

    }

    private void saveCountyAndAreaInServer(String county, String area) {

        boolean pass1 = true;

        if(county != null){

            ParseUser.getCurrentUser().put("county", county);
            ParseUser.getCurrentUser().put("area", area);

            subScribeUserToPushNotifications(area);

            if(!spinnerPrivateSecurity.getSelectedItem().equals("No") &&
                    !spinnerPrivateSecurity.getSelectedItem().equals("Maybe Later")) {
                if (customerId.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please Enter a Customer ID", Toast.LENGTH_SHORT)
                            .show();
                    pass1 = false;
                } else {
                    ParseUser.getCurrentUser().put("privateSecurityCompany",
                            spinnerPrivateSecurity.getSelectedItem());
                    ParseUser.getCurrentUser().put("privateSecurityCustomerId", customerId.getText().toString());
                }
            }

            if(pass1 && !phoneNumberEditText.getText().toString().equals("")){

                if(validPhoneNumber(phoneNumberEditText.getText().toString().trim())){
                    ParseUser.getCurrentUser().put("phoneNumber", phoneNumberEditText.getText()
                            .toString().trim());

                }else{
                    Toast.makeText(getApplicationContext(), "Enter valid phone number."
                            , Toast.LENGTH_SHORT).show();
                    pass1 = false;
                }
            }

            if(pass1){
                ParseUser.getCurrentUser().saveInBackground();
                openActivityWithSlideShow();
//                clearBackStackAndOpenMain();
            }
        }

    }

    private void openActivityWithSlideShow() {
        Intent i = new Intent(getApplicationContext(), WelcomeActivity.class);
        startActivity(i);
    }

    private void subScribeUserToPushNotifications(String area) {

        if(area.equals("Jamhuri") || area.equals("Riara") || area.equals("Valley Arcade") ||
                area.equals("Hurlingham") || area.equals("Kileleshwa") || area.equals("Woodley")
                || area.equals("Caledona") || area.equals("Lavington")){

            ParsePush.subscribeInBackground("Kilimani");

        }else if(yourLocation.getVisibility() == View.VISIBLE){
            ParsePush.subscribeInBackground("All");
        }else{
            ParsePush.subscribeInBackground(area);
        }

    }

    private boolean validPhoneNumber(String phoneNumber) {

        if (phoneNumber.matches("[0-9]+") && phoneNumber.length() == 10) {
            return true;
        } else {
            // TODO more error checking for phone number
            return false;
        }

    }

    private void clearBackStackAndOpenMain() {
        Toast.makeText(SelectAreaActivity.this, "Profile Created", Toast.LENGTH_SHORT)
                .show();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }
}
