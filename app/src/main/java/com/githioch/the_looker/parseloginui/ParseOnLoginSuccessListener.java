package com.githioch.the_looker.parseloginui;

/**
 * Created by Githioch on 6/2/2015.
 */
public interface ParseOnLoginSuccessListener {
    void onLoginSuccess();
}
