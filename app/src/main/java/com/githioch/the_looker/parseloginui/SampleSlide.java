package com.githioch.the_looker.parseloginui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.githioch.the_looker.R;


/**
 * Created by Githiora Wamunyu on 9/27/2015.
 */
public class SampleSlide extends Fragment {

    private static final String ARG_LAYOUT_IMAGE_ID = "imageId";
    private static final String ARG_LAYOUT_BACKGROUND_COLOR = "backgroundColor";


    public static SampleSlide newInstance(int imageId, int backgroundColor) {
        SampleSlide sampleSlide = new SampleSlide();

        Bundle args = new Bundle();
        args.putInt(ARG_LAYOUT_IMAGE_ID, imageId);
        args.putInt(ARG_LAYOUT_BACKGROUND_COLOR, backgroundColor);
        sampleSlide.setArguments(args);

        return sampleSlide;
    }

    private int imageId;
    private static int backgroundColor;

    public SampleSlide() {}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments() != null && getArguments().containsKey(ARG_LAYOUT_IMAGE_ID) &&
                getArguments().containsKey(ARG_LAYOUT_BACKGROUND_COLOR))
            imageId = getArguments().getInt(ARG_LAYOUT_IMAGE_ID);
        backgroundColor = getArguments().getInt(ARG_LAYOUT_BACKGROUND_COLOR);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.layout_tutorial, container, false);
        ImageView imageView = (ImageView)rootView.findViewById(R.id.tutorial_image);
        RelativeLayout root  = (RelativeLayout)rootView.findViewById(R.id.tutorial_root);
        imageView.setImageDrawable(getResources().getDrawable(imageId));
//        root.setBackgroundColor(backgroundColor);
        return rootView;
    }
}
