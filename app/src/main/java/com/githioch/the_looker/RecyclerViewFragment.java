package com.githioch.the_looker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.githioch.the_looker.adapter.FeedListAdapter;
import com.githioch.the_looker.model.FeedItem;
import com.githioch.the_looker.util.ConnectionDetector;
import com.githioch.the_looker.util.MaterialViewPagerHelper;
import com.githioch.the_looker.util.RecyclerViewMaterialAdapter;
import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

import static com.githioch.the_looker.util.Utils.dpToPx;

/**
 * Created by Githioch on 6/22/2015.
 */
public class RecyclerViewFragment extends Fragment implements FeedListAdapter.RecyclerViewClickListener{

    private static final int RESULT_LOAD_IMG = 100;
    private static final int PIC_CROP = 99;
    private static final int TAKE_PHOTO = 98;
    private static final String TAG = RecyclerViewNewsFragment.class.getSimpleName();
    private boolean visiting;

    private android.support.v7.widget.RecyclerView.Adapter mAdapter;
    private List mContentItems;
    private RecyclerView mRecyclerView;
    private FeedListAdapter.RecyclerViewClickListener recyclerViewClickListener;
    private ProgressWheel progressWheel;
    private FeedListAdapter feedListAdapter;
    private ParseQuery<ParseObject> parseQuery;
    private ImageView largeImage;
    private RecyclerViewFragment mActivity;
    private FrameLayout recyclerViewFrame;
    private LinearLayout linearLayout;
    private boolean ocpd = false;
    private String area = "";
    private String profileName = "";
    private Button createFirstPost;
    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private LinearLayoutManager mLayoutManager;
    private ProgressWheel loadMoreProgressWheel;
    private int previousTotal = 0;
    private int visibleThreshold = 5;
    private int mLastFirstVisibleItem;
    private boolean mIsScrollingUp;
    private ArrayList<FeedItem> feedItems;
    private int skips;
    private String areaBeingVisited;


    public RecyclerViewFragment(){

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(parseQuery != null){
            parseQuery.cancel();
        }
    }

    public void displayLargeImage(Bitmap bitmap){
        if(ocpd){
            ((OCPDActivity)getActivity()).displayLargeImage(bitmap);
        }
        else if(!area.equals("") && visiting){
            ((AreaBeingVisitedActivity)getActivity()).displayLargeImage(bitmap);
        }
        else if(!profileName.equals("")){
            ((ProfileActivity)getActivity()).displayLargeImage(bitmap);
        }
        else{
            ((MainActivity)getActivity()).displayLargeImage(bitmap);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        recyclerViewClickListener = this;
        mActivity = RecyclerViewFragment.this;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        recyclerViewClickListener = this;
        mActivity = RecyclerViewFragment.this;
        if(getArguments() != null){
            if(getArguments().getString("ocpd") != null){
                ocpd = true;
            }
            else if(getArguments().getString("area") != null){
                visiting = getArguments().getBoolean("visiting");
                area = getArguments().getString("area");

            }
            else if(getArguments().getString("profile") != null){
                profileName = getArguments().getString("profile");
            }

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 10 && resultCode == Activity.RESULT_OK){
            Log.d("comment", "called");
            int position = data.getIntExtra("position", 0);
            int commentNumber = data.getIntExtra("comment", 0);
            Log.d("position", String.valueOf(position));
            Log.d("comment no", String.valueOf(commentNumber));
            incrementComments(position, commentNumber);
        }
        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                Log.d("comments", "closed");
            }
        }
    }

    private void incrementComments(int position, int commentNumber) {
        FeedItem feedItem = feedItems.get(position);
        feedItem.setNumberOfComments(feedItem.getNumberOfComments() + commentNumber);
        feedListAdapter.notifyDataSetChanged();
        mAdapter.notifyDataSetChanged();
    }

    public static RecyclerViewFragment newInstance() {
        return new RecyclerViewFragment();
    }

    public static Fragment newInstance(String area) {
        return new RecyclerViewFragment();
    }

    RecyclerView.OnScrollListener recyclerOnScroll = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            visibleItemCount = mLayoutManager.getChildCount();
            totalItemCount = mLayoutManager.getItemCount();
            pastVisiblesItems = mLayoutManager.findFirstVisibleItemPosition();

            if (loading) {
                if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                    loading = false;
                    Log.d("...", "Last Item Wow !");
                    loadMoreProgressWheel.setVisibility(View.VISIBLE);
                    loadMoreProgressWheel.spin();

                    loadMoreObjects();
                }
            }
        }

        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);

            if(newState == 0)
                Log.d("a", "scrolling stopped...");

                final int currentFirstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
                if (currentFirstVisibleItem > mLastFirstVisibleItem) {
                    mIsScrollingUp = false;
//                    Log.i("a", "scrolling down...");
                } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
                    mIsScrollingUp = true;
                    Log.i("a", "scrolling up...");
                    if(loadMoreProgressWheel != null){
                        loading = true;
                        loadMoreProgressWheel.stopSpinning();
                        loadMoreProgressWheel.setVisibility(View.GONE);
                    }
                }

                mLastFirstVisibleItem = currentFirstVisibleItem;
    }
    };

    private void loadMoreObjects() {

        parseQuery = new ParseQuery<ParseObject>("News_Feed_Test");
        skips += 5;
        parseQuery.setSkip(skips);
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                int size = 0;
                //TODO revert to calls for all
                if (ocpd) {
                    size = list.size();
                } else {

                    if(list.size() > 5){
                        size = 5;
                    }else{
                        size = list.size();
                    }
                }
                if(list.size() > 0){
                    for (int i = 0; i < size; i++) {
                        FeedItem feedItem = new FeedItem(
                                list.get(i).getString("tag"),
                                list.get(i).getString("userId"),
                                list.get(i).getObjectId(),
                                list.get(i).getString("parseUserWhoPostedId"),
                                list.get(i).getString("name"),
                                list.get(i).getString("status"),
                                list.get(i).getParseFile("image"),
                                list.get(i).getParseFile("parseIcon"),
                                list.get(i).getString("facebookId"),
                                list.get(i).getString("facebookName"),
                                list.get(i).getNumber("goteas"),
                                (List) list.get(i).getList("parseUsersWhoGotead"),
                                (List) list.get(i).getList("fbUsersWhoGotead"),
                                list.get(i).getCreatedAt(),
                                list.get(i).getInt("numberOfComments"));

                        feedItems.add(feedItem);
                    }
                }
            feedListAdapter.notifyDataSetChanged();

            loadMoreProgressWheel.stopSpinning();
            loadMoreProgressWheel.setVisibility(View.GONE);

            }
        });


    }

    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle){
        //fragment_recyclerview
        View rootView = layoutinflater.inflate(R.layout.layout_recyclerview, viewgroup, false);
        recyclerViewFrame = (FrameLayout)rootView.findViewById(R.id.recyclerVew_frame);
        mRecyclerView = (RecyclerView)rootView.findViewById(R.id.news_feed_recyclerview_test);
        linearLayout = (LinearLayout)rootView.findViewById(R.id.linearLayout_recyclerView);
        createFirstPost = (Button)rootView.findViewById(R.id.create_first_post);
        progressWheel = (ProgressWheel)rootView.findViewById(R.id.progress_wheel);
        loadMoreProgressWheel = (ProgressWheel)rootView.findViewById(R.id.progress_wheel_load_more);
        final Button retry = (Button)rootView.findViewById(R.id.retry);

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (new ConnectionDetector(getActivity()).isConnectingToInternet()) {
                    retry.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    getData();
                }
            }
        });

        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
//        mRecyclerView.setOnScrollListener(recyclerOnScroll);


        if(new ConnectionDetector(getActivity()).isConnectingToInternet()){
            getData();
        }else{
            retry.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }

        createFirstPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity().getApplicationContext(),
                        PolyPickerActivity.class);
                if(!area.equals("")){
                    intent.putExtra("area", area);
                    intent.putExtra("visiting", visiting);
                }
                startActivity(intent);

            }
        });
        if(ocpd || !profileName.equals("")){
            mRecyclerView.setPadding(0,0,0,0);
            linearLayout.setVisibility(View.GONE);
        }else if(!area.equals("") && visiting){
            mRecyclerView.setPadding(0,0,0,0);
        }
        else{
            if(linearLayout.getVisibility() == View.GONE){
                linearLayout.setVisibility(View.VISIBLE);
            }
            float padding  = dpToPx(120, getActivity());
            mRecyclerView.setPadding(0, (int) padding, 0, 0);
        }
//        largeImage = (ImageView)rootView.findViewById(R.id.feed_large_image);
        ImageView post = (ImageView)rootView.findViewById(R.id.btn_feed_post);
//        final ImageView takePhoto = (ImageView)rootView.findViewById(R.id.btn_feed_take_photo);
        ImageView imageFromGallery = (ImageView)rootView.findViewById(R.id.btn_feed_image_from_gallery);

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), PolyPickerActivity.class);
                if(!area.equals("")){
                    intent.putExtra("area", area);
                }
                intent.putExtra("visiting", visiting);
                startActivity(intent);
            }
        });

        /*takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if user has a camera installed
                if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
                    Intent takePicture = new Intent(getActivity(), PolyPickerActivity.class);
                    takePicture.putExtra("typeSource", TAKE_PHOTO);
                    startActivity(takePicture);
                }else{
                    Toast.makeText(getActivity().getApplicationContext(), "You do not have a camera" +
                            " unfortunately", Toast.LENGTH_LONG).show();
                }
            }
        });*/

        imageFromGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePicture = new Intent(getActivity(), PolyPickerActivity.class);
                takePicture.putExtra("typeSource", RESULT_LOAD_IMG);
                startActivity(takePicture);
            }
        });
        return rootView;
    }

    private void getData() {
        progressWheel.setVisibility(View.VISIBLE);
        progressWheel.spin();

        parseQuery = new ParseQuery<>("News_Feed_Test");

        if(!area.equals("")){
            parseQuery.whereEqualTo("area", area);
            queryData();
        }
        else if(ocpd){
            Log.d("ocpd", "passed");
            queryOcpdData();
        }
       else if(!profileName.equals("")){
            if(ParseUser.getCurrentUser().getJSONObject("profile") != null){
                parseQuery.whereEqualTo("facebookName", profileName);
            }else{
                parseQuery.whereEqualTo("name", profileName);
            }

            final ArrayList<FeedItem> feedItems = new ArrayList<>();
            parseQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                    if (list.size() == 0) {
                       Log.d(TAG, "news list null");
                    } else {
                        for (int i = 0; i < 50; i++) {
                            FeedItem feedItem = new FeedItem(
                                    list.get(i).getString("tag"),
                                    list.get(i).getString("userId"),
                                    list.get(i).getObjectId(),
                                    list.get(i).getString("parseUserWhoPostedId"),
                                    list.get(i).getString("name"),
                                    list.get(i).getString("status"),
                                    list.get(i).getParseFile("image"),
                                    list.get(i).getParseFile("parseIcon"),
                                    list.get(i).getString("facebookId"),
                                    list.get(i).getString("facebookName"),
                                    list.get(i).getNumber("goteas"),
                                    (List) list.get(i).getList("parseUsersWhoGotead"),
                                    (List) list.get(i).getList("fbUsersWhoGotead"),
                                    list.get(i).getCreatedAt(),
                                    list.get(i).getInt("numberOfComments"));

                            feedItems.add(feedItem);
                        }

                        parseQuery = new ParseQuery<ParseObject>("News");
                        parseQuery.orderByDescending("createdAt");
                        parseQuery.findInBackground(new FindCallback<ParseObject>() {
                            @Override
                            public void done(List<ParseObject> list, ParseException e) {
                                for (int i = 0; i < list.size(); i++) {
                                    FeedItem feedItem = new FeedItem();
                                    ParseObject parseObject = list.get(i);
                                    feedItem.setNewsTitle(parseObject.getString("newsTitle"));
                                    feedItem.setNewsStatus(parseObject.getString("newsStatus"));
                                    feedItem.setUrl(parseObject.getString("url"));
                                    feedItem.setNewsImageUrl(parseObject.getString("newsImageUrl"));
                                    feedItem.setIsFeed(false);

                                    feedItems.add(feedItem);
                                }

                            }
                        });
                    }

                    feedListAdapter = new FeedListAdapter(getActivity().getApplicationContext()
                            , recyclerViewClickListener, mActivity);
                    feedListAdapter.setFeedList(feedItems);
                    feedListAdapter.setRecyclerView(mRecyclerView);

                    mAdapter = new RecyclerViewMaterialAdapter(feedListAdapter);
                    mRecyclerView.setAdapter(mAdapter);
                    MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView, null);

                    progressWheel.stopSpinning();
                    progressWheel.setVisibility(View.GONE);

                    mRecyclerView.setVisibility(View.VISIBLE);

                }
            });
        }else{
        parseQuery.orderByDescending("createdAt");
        queryData();
        }
    }

    private void queryOcpdData() {
        feedItems = new ArrayList<>();
        ParseQuery<ParseObject> objectParseQuery = new ParseQuery<ParseObject>("News_Feed_Test");
        objectParseQuery.whereEqualTo("police", true);
        objectParseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (list.size() == 0) {
                    Log.d(TAG, "ocpd feed empty");
                } else {
                    for (int i = 0; i < list.size(); i++) {
                        FeedItem feedItem = new FeedItem(
                                list.get(i).getString("tag"),
                                list.get(i).getString("userId"),
                                list.get(i).getObjectId(),
                                list.get(i).getString("parseUserWhoPostedId"),
                                list.get(i).getString("name"),
                                list.get(i).getString("status"),
                                list.get(i).getParseFile("image"),
                                list.get(i).getParseFile("parseIcon"),
                                list.get(i).getString("facebookId"),
                                list.get(i).getString("facebookName"),
                                list.get(i).getNumber("goteas"),
                                (List) list.get(i).getList("parseUsersWhoGotead"),
                                (List) list.get(i).getList("fbUsersWhoGotead"),
                                list.get(i).getCreatedAt(),
                                list.get(i).getInt("numberOfComments"));

                        feedItems.add(feedItem);
                    }
                }
                Log.d("size", String.valueOf(feedItems.size()));
                feedListAdapter = new FeedListAdapter(getActivity(), recyclerViewClickListener, mActivity);
                feedListAdapter.setFeedList(feedItems);
                mAdapter = new RecyclerViewMaterialAdapter(feedListAdapter);
                mRecyclerView.setAdapter(mAdapter);
                MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView, null);

                progressWheel.stopSpinning();
                progressWheel.setVisibility(View.GONE);

                mRecyclerView.setVisibility(View.VISIBLE);

            }
        });
    }

    private void queryData() {
        Log.d("query data", "passed");
        feedItems = new ArrayList<>();
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (list == null || list.size() == 0) {
                    createFirstPost.setVisibility(View.VISIBLE);
                    createFirstPost.bringToFront();
                    mRecyclerView.setVisibility(View.GONE);
                } else {
                    for (int i = 0; i < list.size(); i++) {
                        FeedItem feedItem = new FeedItem(
                                list.get(i).getString("tag"),
                                list.get(i).getString("userId"),
                                list.get(i).getObjectId(),
                                list.get(i).getString("parseUserWhoPostedId"),
                                list.get(i).getString("name"),
                                list.get(i).getString("status"),
                                list.get(i).getParseFile("image"),
                                list.get(i).getParseFile("parseIcon"),
                                list.get(i).getString("facebookId"),
                                list.get(i).getString("facebookName"),
                                list.get(i).getNumber("goteas"),
                                (List) list.get(i).getList("parseUsersWhoGotead"),
                                (List) list.get(i).getList("fbUsersWhoGotead"),
                                list.get(i).getCreatedAt(),
                                list.get(i).getInt("numberOfComments"));

                        feedItems.add(feedItem);
                    }
                }
                Log.d("size", String.valueOf(feedItems.size()));
                feedListAdapter = new FeedListAdapter(getActivity(), recyclerViewClickListener, mActivity);
                feedListAdapter.setFeedList(feedItems);
                mAdapter = new RecyclerViewMaterialAdapter(feedListAdapter);
                mRecyclerView.setAdapter(mAdapter);
                MaterialViewPagerHelper.registerRecyclerView(getActivity(), mRecyclerView, null);

                progressWheel.stopSpinning();
                progressWheel.setVisibility(View.GONE);

                mRecyclerView.setVisibility(View.VISIBLE);

            }
        });

    }

    @Override
    public void recyclerViewListClicked(View v, String objectId) {

        Intent intent = new Intent(getActivity(), CommentsActivity.class);
        intent.putExtra("id", objectId);
        startActivity(intent);

    }

    @Override
    public void itemSelectedForDelete(String objectId, int position) {
        showDeleteDialog(objectId, position);
    }

    private void showDeleteDialog(final String objectId, final int position) {
        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder
                .setTitle("Delete post?")
                .setMessage("Are you sure you want to delete this post?")
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ParseQuery<ParseObject> parseObjectParseQuery = new ParseQuery<ParseObject>("News_Feed_Test");
                        parseObjectParseQuery.getInBackground(objectId, new GetCallback<ParseObject>() {
                            @Override
                            public void done(ParseObject parseObject, ParseException e) {
                                parseObject.deleteInBackground(new DeleteCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        feedItems.remove(position);
                                        mAdapter.notifyItemRemoved(position);
                                        mAdapter.notifyItemRangeChanged(position, feedItems.size());
                                        Toast.makeText(getActivity(), "Post deleted successfully", Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                });
                            }
                        });

                    }
                });

        alertDialog = builder.create();//AlertDialog dialog; create like this outside onClick
        alertDialog.show();
    }


}

