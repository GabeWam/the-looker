package com.githioch.the_looker;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

/**
 * Created by Githioch on 7/10/2015.
 */
public class OCPDActivity extends AppCompatActivity{


    private android.support.v7.app.ActionBar menu;
    private FrameLayout frameLayout;
    private RelativeLayout ocpdRelative;
    private ImageView largeImage;
    private Button sendPrivateMessage;
    private boolean messageRead;

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(largeImage.getVisibility() == View.VISIBLE){
            largeImage.setVisibility(View.GONE);
            ocpdRelative.setVisibility(View.VISIBLE);
            frameLayout.setVisibility(View.VISIBLE);
            sendPrivateMessage.setVisibility(View.VISIBLE);

        }else{
            Intent returnIntent = new Intent();
            returnIntent.putExtra("messageRead",messageRead);
            setResult(RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent returnIntent = new Intent();
                returnIntent.putExtra("messageRead",messageRead);
                setResult(RESULT_OK, returnIntent);
                finish();
        }
        return (super.onOptionsItemSelected(item));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ocpd);

        messageRead = false;

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();

        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setHomeAsUpIndicator(R.drawable.ic_back_arrow);
            menu.setTitle("");
        }

        RecyclerViewFragment recyclerViewFragment = new RecyclerViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("ocpd", "ocpd");
        recyclerViewFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().
                add(R.id.frame_main, recyclerViewFragment)
                .commit();

        ocpdRelative = (RelativeLayout)findViewById(R.id.ocpd_relative);
        frameLayout = (FrameLayout)findViewById(R.id.frame_main);
        largeImage = (ImageView)findViewById(R.id.feed_large_image);
        sendPrivateMessage = (Button)findViewById(R.id.ocpd_send_private_message);

        sendPrivateMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), OCPDPrivateMessageActivity.class);
                startActivityForResult(intent, 13);
            }
        });

    }

    public void displayLargeImage(Bitmap bitmap) {
        largeImage.setImageBitmap(bitmap);
        largeImage.setVisibility(View.VISIBLE);
        ocpdRelative.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
        sendPrivateMessage.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //TODO not being called from private message
        Log.d("ocpdactivity", "on activity result called");
        if(requestCode == 13 && resultCode == RESULT_OK && data != null){
            messageRead = data.getBooleanExtra("messageRead", false);
            if(messageRead){
                Log.d("ocpdactviity", "true");
            }else{
                Log.d("ocpdactviity", "false");
            }


        }
    }


}
