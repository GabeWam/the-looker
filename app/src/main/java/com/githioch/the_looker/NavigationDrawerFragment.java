package com.githioch.the_looker;


import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.githioch.the_looker.model.Information;
import com.githioch.the_looker.model.Notification;
import com.githioch.the_looker.util.AsyncTask;
import com.githioch.the_looker.util.ConnectionDetector;
import com.githioch.the_looker.util.ProfilePictureViewNavigationDrawer;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.pkmmte.view.CircularImageView;
import com.pnikosis.materialishprogress.ProgressWheel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NavigationDrawerFragment extends Fragment {


    private static final String TAG = NavigationDrawerFragment.class.getSimpleName();
    public static final String PREF_FILE_NAME = "testpref";
    public static final String KEY_USER_LEARNED_DRAWER = "user_learned_drawer";
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private boolean mUserLearnedDrawer;
    private boolean mFromSavedInstanceState;
    private View containerView;
    private ProfilePictureViewNavigationDrawer profilePictureView;
    private ParseUser parseUser;

    private ImageLoader imageLoader;
    private ProgressWheel progressWheel;
    private CircularImageView circleParse;
    private TextView userName;
    private TextView userLocation;
    private FragmentActivity activity;
    private ListView listView;
    private List<Information> data;
    private ArrayList<String> newsFeedIds;
    private int numberOfNotifs;
    private RelativeLayout drawerRelative;
    private int numberComments = 0;
    private int numberGoteas = 0;
    private ArrayList<ParseObject> parseObjectList;
    private ArrayList<Notification> notificationArrayList;
    private TextView drawerNotifications;
    private LinearLayout containerLayout;
    private LinearLayout nameAndLocation;


    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    private void getNotifsMethod(){
        newsFeedIds = new ArrayList<>();

        try {
            ParseUser.getCurrentUser().fetch();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        ParseQuery<ParseObject> parseObjectParseQuery = new ParseQuery<ParseObject>("notifications");
        parseObjectParseQuery.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
        JSONArray notifs = null;
        try {
            ParseObject parseObject = parseObjectParseQuery.getFirst();
            if(parseObject != null){
                notifs = parseObject.getJSONArray("notif");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


        if(notifs != null){
            for(int i = 0; i < notifs.length(); i++){
                JSONObject jsonObject = null;
                try {
                    Log.d("size", String.valueOf(notifs.length()));
                    jsonObject = notifs.getJSONObject(i);
                    Iterator<String> iter = jsonObject.keys();
                    while (iter.hasNext()) {
                        String keyNew = iter.next();
                        if(keyNew.equals("postId")){
                            int countCommentsNew = jsonObject.getInt("newComments");
                            int countCommentsOld = jsonObject.getInt("oldComments");
                            int countGoteasNew = jsonObject.getInt("newGoteas");
                            int countGoteasOld = jsonObject.getInt("oldGoteas");

                            Log.d("newComments", String.valueOf(countCommentsNew));
                            Log.d("oldComments", String.valueOf(countCommentsOld));
                            Log.d("newGoteas", String.valueOf(countGoteasNew));
                            Log.d("oldGoteas", String.valueOf(countGoteasOld));

                            if(countCommentsNew != countCommentsOld){

                                numberOfNotifs += Math.abs(countCommentsNew - countCommentsOld);
                            }

                            if(countGoteasNew != countGoteasOld){

                                numberOfNotifs += Math.abs(countGoteasOld - countGoteasNew);
                            }
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        numberOfNotifs = 0;
        parseObjectList = new ArrayList<>();

        if(notificationArrayList == null){
            notificationArrayList = new ArrayList<>();
        }

        getNotifications();

//        getNotificationsTest();

        imageLoader = ImageLoader.getInstance();
        activity = getActivity();

        mUserLearnedDrawer = Boolean.valueOf(readFromPreferences(getActivity(),
                KEY_USER_LEARNED_DRAWER, "false"));

        if(savedInstanceState != null) {
            mFromSavedInstanceState = true;
        }
    }

    private void getNotifications() {
        if(new ConnectionDetector(getActivity()).isConnectingToInternet()){

            if(ParseUser.getCurrentUser() != null){
                ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("News_Feed_Test");
                parseQuery.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
                //Only notifications for the last 10 posts will be retrieved.
                parseQuery.setLimit(10);
                List<ParseObject> parseObjectArrayList = new ArrayList<>();
                try {
                    parseObjectArrayList = parseQuery.find();
                } catch (ParseException e) {
                    Log.d(TAG, e.toString());
                }
                if(parseObjectArrayList.size() > 0){
                    for (int i = 0; i < parseObjectArrayList.size(); i++) {

                        ParseObject parseObject = parseObjectArrayList.get(i);

                        int newComments = parseObject.getInt("newComments");
                        int newGoteas = parseObject.getInt("newGoteas");

                        if(newComments != 0){
                            Notification notification = new Notification();
                            numberOfNotifs += newComments;
                            notification.setIsComment(true);
                            if(parseObject.getParseFile("image") != null){
                                notification.setImageUrl(parseObject.getParseFile("image").getUrl());
                            }
                            notification.setPost(parseObject.getString("status"));
                            notification.setNumberOfComments(newComments);
                            notification.setDateModified(parseObject.getUpdatedAt());
                            notification.setPostId(parseObject.getObjectId());
                            notificationArrayList.add(notification);
                        }

                        if(newGoteas != 0){
                            Notification notification = new Notification();
                            notification.setIsComment(false);
                            if(parseObject.getParseFile("image") != null){
                                notification.setImageUrl(parseObject.getParseFile("image").getUrl());
                            }
                            notification.setPost(parseObject.getString("status"));
                            notification.setNumberOfGoteas(newGoteas);
                            notification.setDateModified(parseObject.getUpdatedAt());
                            notification.setPostId(parseObject.getObjectId());
                            numberOfNotifs += newGoteas;
                            notificationArrayList.add(notification);
                        }
                    }
                }

                Log.d("number notifs", String.valueOf(numberOfNotifs));
            }
        }
    }
    private void getNotificationsTest() {

        Log.d("notif size", String.valueOf(notificationArrayList.size()));
        if(notificationArrayList.size() == 0){
            for (int i = 0; i < 2; i++) {

                Notification notification = new Notification();
                numberOfNotifs += 1;
                notification.setIsComment(true);

                notification.setIsClicked(false);

                if(i == 0){
                    notification.setImageUrl("https://scontent-lhr3-1.xx.fbcdn.net/hphotos-xfp1/v/t1.0-9/" +
                            "11887990_10206151009687382_7161262592402045587_n.jpg?oh=9f07d8a6d20c71391ec10dcaa91fab63&oe=5645B3C3");
                }

                if(i ==1){
                    notification.setImageUrl("https://fbcdn-sphotos-h-a.akamaihd.net/hphotos-ak-xp" +
                            "f1/v/t1.0-9/11899770_762619830549108_6197272766715222634_n.jpg?oh=30" +
                            "ec6da1e16d3da0b47637e792e0df5d&oe=5646DB74&__gda__=1451034654_b1bff2a27500d6f92bbb99c8ca256105");
                }

                notification.setPost("Hello there");

                notification.setNumberOfComments(5);
                notification.setDateModified(Calendar.getInstance().getTime());
                notificationArrayList.add(notification);

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 11 && resultCode == Activity.RESULT_OK && data != null){
            numberOfNotifs = data.getIntExtra("newNotificationNumber", 0);
            Log.d("notif result", String.valueOf(numberOfNotifs));
            notificationArrayList = (ArrayList<Notification>) data.getSerializableExtra("notifs");
            if(numberOfNotifs != 0) {
                drawerNotifications.setVisibility(View.VISIBLE);
                drawerNotifications.setText(String.valueOf(numberOfNotifs));
            }else{
                drawerNotifications.setVisibility(View.GONE);
            }

        }
    }

    //TODO put border around fb profile image
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);
        listView = (ListView) rootView.findViewById(R.id.drawerlist_recyclerview);
        drawerRelative = (RelativeLayout)rootView.findViewById(R.id.drawer_relative);
        TextView inviteFriend = (TextView)rootView.findViewById(R.id.drawer_invite_friend);
        TextView feedback = (TextView)rootView.findViewById(R.id.drawer_feedback);

        containerLayout = (LinearLayout)rootView.findViewById(R.id.container_layout);

        nameAndLocation = (LinearLayout)rootView.findViewById(R.id.drawer_layout_name_and_location);

        RelativeLayout looks = (RelativeLayout)rootView.findViewById(R.id.drawer_items_1);
        RelativeLayout notifications = (RelativeLayout)rootView.findViewById(R.id.drawer_items_2);
        RelativeLayout settings = (RelativeLayout)rootView.findViewById(R.id.drawer_items_3);
        RelativeLayout visiting = (RelativeLayout)rootView.findViewById(R.id.drawer_items_4);

        drawerNotifications  = (TextView)rootView.findViewById(R.id.drawer_notifications);

        //TODO change to > 0
        if(numberOfNotifs != 0){
            drawerNotifications.setText(String.valueOf(numberOfNotifs));
            drawerNotifications.setVisibility(View.VISIBLE);
        }else{
            drawerNotifications.setVisibility(View.GONE);
        }

        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Intent intent1  = new Intent(getActivity(), FeedbackActivity.class);
//                drawerNotifications.setText(0);
//                closeDrawersAndOpenActivity(intent1, mDrawerLayout);
                mDrawerLayout.closeDrawers();
                android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(intent1);
                    }
                }, 100);

            }
        });

        looks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                closeDrawersAndOpenActivity(intent, mDrawerLayout);
            }
        });

        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent1  = new Intent(getActivity(), ViewNotificationsActivity.class);
                intent1.putExtra("notifs", notificationArrayList);
                intent1.putExtra("notifSize", numberOfNotifs);
                mDrawerLayout.closeDrawers();
                android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivityForResult(intent1, 11);
                    }
                }, 100);
            }
        });

        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent2 = new Intent(getActivity(), SettingsActivity.class);
//                closeDrawersAndOpenActivity(intent2, mDrawerLayout);
                mDrawerLayout.closeDrawers();
                android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(intent2);
                    }
                }, 100);
            }
        });

        visiting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Intent intent3 = new Intent(getActivity(), VisitingActivity.class);
//                closeDrawersAndOpenActivity(intent3, mDrawerLayout);
                mDrawerLayout.closeDrawers();
                android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(intent3);
                    }
                }, 100);
            }
        });

        inviteFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "https://play.google.com/store/apps/details?id=com.githioch.the_looker";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Check out The Looker");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });

        profilePictureView = (ProfilePictureViewNavigationDrawer) rootView.findViewById(R.id.userProfilePicture);
        progressWheel  =(ProgressWheel)rootView.findViewById(R.id.progress_wheel);
         circleParse = (CircularImageView)rootView.findViewById(R.id.profile_image);
        userName = (TextView)rootView.findViewById(R.id.drawer_user_name);
        userLocation = (TextView)rootView.findViewById(R.id.drawer_user_location);

        circleParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
                mDrawerLayout.closeDrawers();
            }
        });

        profilePictureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("profile on click", "called");
                Intent intent = new Intent(getActivity().getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
                mDrawerLayout.closeDrawers();
            }
        });


        return rootView;
    }

    private void closeDrawersAndOpenActivity(final Intent intent, DrawerLayout mDrawerLayout) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        mDrawerLayout.closeDrawers();
        android.os.Handler handler = new android.os.Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
            }
        }, 100);
    }

    @Override
    public void onResume() {
        super.onResume();


        ParseUser parseUser = ParseUser.getCurrentUser();

        if(parseUser == null){
            if(circleParse.getVisibility() == View.VISIBLE){
                circleParse.setVisibility(View.GONE);
            }
            profilePictureView.setProfileId(null);
        }
        else if(parseUser.has("profile")){
            Log.d("parseuser","has image");
            String facebookId = null;
            facebookId = ParseUser.getCurrentUser()
                    .getJSONObject("profile").optString("facebookId");
            if(profilePictureView.getVisibility() == View.GONE){
                profilePictureView.setVisibility(View.VISIBLE);
            }
            userName.setText(ParseUser.getCurrentUser().getJSONObject("profile").optString("name"));
            profilePictureView.setProfileId(facebookId);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {

               new showBlurred().execute(profilePictureView.getImageContents());

            }else{
                nameAndLocation.setBackgroundColor(getResources().getColor(R.color.drawer_name_and_locatiom));
            }


            circleParse.setVisibility(View.GONE);
            progressWheel.setVisibility(View.GONE);
        }else{
            if(ParseUser.getCurrentUser().getString("name") != null){
                userName.setText(ParseUser.getCurrentUser().getString("name"));
            }else{
                userName.setText("User");
            }
            profilePictureView.setProfileId(null);
            if(parseUser.has("image")){
                profilePictureView.setVisibility(View.GONE);
                circleParse.setVisibility(View.GONE);
                progressWheel.setVisibility(View.VISIBLE);
                progressWheel.spin();

                String url = parseUser.getParseFile("image").getUrl();
                imageLoader.loadImage(url, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String s, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String s, View view, FailReason failReason) {

                    }

                    @Override
                    public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                        progressWheel.setVisibility(View.GONE);
                        circleParse.setImageBitmap(bitmap);
                        circleParse.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingCancelled(String s, View view) {

                    }
                });
            }
        }

        if(ParseUser.getCurrentUser() != null && ParseUser.getCurrentUser().getString("area") != null){
            userLocation.setText(ParseUser.getCurrentUser().getString("area"));
        }else{
            userLocation.setText("Location");
        }

    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private Bitmap blurRenderScript(Bitmap smallBitmap, int radius) {
        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }




        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);


        RenderScript renderScript = RenderScript.create(getActivity());


        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);


        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25

        blur.forEach(blurOutput);


        blurOutput.copyTo(bitmap);
        renderScript.destroy();


        return bitmap;
    }

    private Bitmap RGB565toARGB888(Bitmap img) throws Exception{
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];


        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());


        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);


        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {

        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mActionBarDrawerToggle = new ActionBarDrawerToggle(
                getActivity(), mDrawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        ) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                if (!mUserLearnedDrawer) {
                    mUserLearnedDrawer = true;
                    saveToSharedPreferences(getActivity(), KEY_USER_LEARNED_DRAWER, "true");
                }
                getActivity().invalidateOptionsMenu();

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                if(getActivity() != null){
                    getActivity().invalidateOptionsMenu();
                }

            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {

                /*if(slideOffset < 0.6){
                    toolbar.setAlpha(1 - slideOffset);
                }*/

            }
        };

        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout.openDrawer(containerView);
        }

            mDrawerLayout.setDrawerListener(mActionBarDrawerToggle);

            mDrawerLayout.post(new Runnable() {
                @Override
                public void run() {
                    mActionBarDrawerToggle.syncState();
                }
            });

    }

    public static void saveToSharedPreferences(Context context, String preferenceName, String preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }

    private class showBlurred extends AsyncTask<Bitmap, Void, Drawable>{

        @Override
        protected Drawable doInBackground(Bitmap... params) {
            Bitmap bitmap = params[0];
            Bitmap blurred = blurRenderScript(bitmap, 20);
            Drawable drawable = new BitmapDrawable(getResources(), blurred);
            return drawable;
        }

        @Override
        protected void onPostExecute(Drawable drawable) {
            containerLayout.setBackground(drawable);
        }
    }

}

