package com.githioch.the_looker;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.githioch.the_looker.util.ConnectionDetector;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.soundcloud.android.crop.Crop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Githioch on 6/6/2015.
 */
public class PostActivity extends Activity{

    private static final int RESULT_LOAD_IMG = 100;
    private static final int PIC_CROP = 99;
    private static final int TAKE_PHOTO = 98;
    private EditText editText;
    private Button post;
    private Button addPhoto;
    private String imgDecodableString;
    private ImageView imgView;
    private ProgressWheel progressWheel;
    private ImageLoader imageLoader;
    private String compressImage;
    private Button fromGallery;
    private Uri capturedImageURI;
    private String currentPhotoPath;

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory.
//      Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2,
                new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();*/
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }
    private void handleCrop(int resultCode, Intent result) {

        if (resultCode == RESULT_OK) {
            String imagePath = Crop.getOutput(result).getPath();
            String compressedImage  = compressImage(imagePath);
            Bitmap thePic = BitmapFactory.decodeFile(compressedImage);
            //retrieve a reference to the ImageView
//display the returned cropped image
            imgView.setImageBitmap(thePic);
            imgView.setVisibility(View.VISIBLE);

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
                beginCrop(data.getData());
            }else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
            // When an Image is picked
            else if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK
            && null != data) {
                // Get the Image from data
               Uri selectedImage = data.getData();
                performCrop(selectedImage);

                }
            else if (requestCode == TAKE_PHOTO && resultCode == RESULT_OK) {
                addPhotoToGallery();
                beginCrop(getCapturedImageURI());

                // Show the full sized image.
//                setFullImageFromFilePath(activity.getCurrentPhotoPath(), mImageView);
//                setFullImageFromFilePath(activity.getCurrentPhotoPath(), mThumbnailImageView);
            }
            else if(requestCode == TAKE_PHOTO && data != null){
                beginCrop(data.getData());
            }
            else if(requestCode == PIC_CROP && data != null){
                String filePath = Environment.getExternalStorageDirectory()
                        + "/temporary_holder.jpg";
                compressImage = compressImage(filePath);
                Bitmap thePic = BitmapFactory.decodeFile(compressImage);
                //retrieve a reference to the ImageView
//display the returned cropped image
                imgView.setImageBitmap(thePic);
                imgView.setVisibility(View.VISIBLE);

            }
            else {
                Toast.makeText(this, "You haven't picked Image",
                        Toast.LENGTH_LONG).show();
                }
            } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
            .show();
            }

    }

    private void addPhotoToGallery() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(getCurrentPhotoPath());
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        sendBroadcast(mediaScanIntent);
    }

    private void performCrop(Uri selectedImage) {
        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(selectedImage, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");

            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 800);
            cropIntent.putExtra("outputY", 800);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);

            File f = new File(Environment.getExternalStorageDirectory(),
                    "/temporary_holder.jpg");
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri = Uri.fromFile(f);
            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);

        }
        catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }

    }



    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialogfragment);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        // Get intent, action and MIME type
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                if (sharedText != null) {
                    // Update UI to reflect text being shared
                    final ParseObject parseObject = new ParseObject("News_Feed_Test");
                    if (ParseUser.getCurrentUser() != null) {
                        if (ParseUser.getCurrentUser().has("profile")) {
                            parseObject.put("facebookName", ParseUser.getCurrentUser().getJSONObject("profile")
                                    .optString("name"));
                            parseObject.put("facebookId", ParseUser.getCurrentUser().getJSONObject("profile")
                                    .optString("facebookId"));

                        } else {
                            parseObject.put("name", ParseUser.getCurrentUser().getString("name"));
                            //compress parse image and send

                        }
                        parseObject.put("userId", ParseUser.getCurrentUser().getObjectId());

                    } else {
                        parseObject.put("name", "not signed in");
                    }
                    parseObject.put("status", sharedText);

                    parseObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            Log.d("post saved", "without image");
                            try {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put(parseObject.getObjectId(), 0);
                                ParseUser.getCurrentUser().put("notifications", jsonObject);
                                ParseUser.getCurrentUser().put("notificationsOld", jsonObject);
                                ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        Log.d("user notif", "saved");
                                    }
                                });
                            } catch (JSONException e1) {
                                e1.printStackTrace();
                            }
                            progressWheel.setVisibility(View.GONE);
                            Intent returnIntent = new Intent(PostActivity.this, MainActivity.class);
                            startActivity(returnIntent);
                            finish();
                        }
                    });
                }


            }
        }



        imageLoader = ImageLoader.getInstance();

        editText = (EditText)findViewById(R.id.edit_post);
        post = (Button)findViewById(R.id.post);
        addPhoto = (Button)findViewById(R.id.btn_add_photo);
        imgView = (ImageView) findViewById(R.id.image_from_gallery);
        progressWheel = (ProgressWheel)findViewById(R.id.progress_wheel);

        if(getIntent().getExtras() != null){
            if(getIntent().getExtras().getInt("typeSource") == TAKE_PHOTO){
                Log.d("intent camera", "caught");
                dispatchTakePictureIntent();

            }
            if(getIntent().getExtras().getInt("typeSource") == RESULT_LOAD_IMG){
                Log.d("from gallery", "caught");
                Crop.pickImage(PostActivity.this);
            }
        }

        post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ConnectionDetector connectionDetector = new ConnectionDetector(PostActivity.this);

                if(connectionDetector.isConnectingToInternet()){
                    final String post = editText.getText().toString();

                    if(!post.equals("")) {
                        progressWheel.setVisibility(View.VISIBLE);
                        InputMethodManager imm = (InputMethodManager) getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);

                        final ParseObject parseObject = new ParseObject("News_Feed_Test");
                        if (ParseUser.getCurrentUser() != null) {
                            if (ParseUser.getCurrentUser().has("profile")) {
                                parseObject.put("facebookName", ParseUser.getCurrentUser().getJSONObject("profile")
                                        .optString("name"));
                                parseObject.put("facebookId", ParseUser.getCurrentUser().getJSONObject("profile")
                                        .optString("facebookId"));

                            } else {
                                parseObject.put("name", ParseUser.getCurrentUser().getString("name"));
                                parseObject.put("parseUserWhoPostedId", ParseUser.getCurrentUser().getObjectId());
                                if(ParseUser.getCurrentUser().getParseFile("parseIcon") != null){
                                    parseObject.put("parseIcon", ParseUser.getCurrentUser().getParseFile("parseIcon"));
                                }
                                //compress parse image and send

                            }
                            parseObject.put("userId", ParseUser.getCurrentUser().getObjectId());

                        } else {
                            parseObject.put("name", "not signed in");
                        }
                        parseObject.put("status", post);

                        if (imgView.getDrawable() != null) {
                            Drawable d = imgView.getDrawable(); // the drawable (Captain Obvious, to the rescue!!!)
                            Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            byte[] bitmapdata = stream.toByteArray();

                            final ParseFile parseFile = new ParseFile("file.jpg", bitmapdata);
                            parseObject.put("image", parseFile);
                            parseObject.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        Log.d("saving", "done");

                                        progressWheel.stopSpinning();
                                        progressWheel.setVisibility(View.GONE);
                                        Intent returnIntent = new Intent(PostActivity.this, MainActivity.class);
                                        returnIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(returnIntent);
                                        finish();

                                    } else {
                                        Toast.makeText(PostActivity.this, "Error sending post." +
                                                " Try again later.", Toast.LENGTH_SHORT)
                                                .show();
                                    }
                                }
                            });
                        }

                        parseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if(e == null){
                                    Log.d("post saved", "without image");

                                    ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("notifications");
                                    parseQuery.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
                                    parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                                        @Override
                                        public void done(ParseObject parseObjectParse, ParseException e) {
                                            if(parseObjectParse == null){
                                                ParseObject parseNotif  = new ParseObject("notifications");
                                                parseNotif.put("userId", ParseUser.getCurrentUser().getObjectId());
                                                JSONArray jsonArray = new JSONArray();
                                                JSONObject jsonObject = new JSONObject();
                                                try {
                                                    jsonObject.put("postId", parseObject.getObjectId());
                                                    jsonObject.put("new", 0);
                                                    jsonObject.put("old", 0);
                                                    jsonArray.put(jsonObject);
                                                    parseNotif.put("notif", jsonArray);
                                                    parseNotif.saveInBackground(new SaveCallback() {
                                                        @Override
                                                        public void done(ParseException e) {
                                                            Log.d("parseNotif", "saved");
                                                        }
                                                    });
                                                } catch (JSONException e1) {
                                                    e1.printStackTrace();
                                                }
                                            }else{
                                                JSONArray jsonArray = parseObjectParse.getJSONArray("notif");
                                                try {
                                                    JSONObject jsonObject = new JSONObject();
                                                    jsonObject.put("postId", parseObject.getObjectId());
                                                    jsonObject.put("new", 0);
                                                    jsonObject.put("old", 0);
                                                    jsonArray.put(jsonObject);
                                                    parseObjectParse.saveInBackground(new SaveCallback() {
                                                        @Override
                                                        public void done(ParseException e) {
                                                            Log.d("nonempty notif", "saved");
                                                        }
                                                    });
                                                } catch (JSONException e1) {
                                                    e1.printStackTrace();
                                                }
                                            }
                                        }
                                    });

                                    progressWheel.setVisibility(View.GONE);
                                    Intent returnIntent = new Intent(PostActivity.this, MainActivity.class);
                                    returnIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(returnIntent);
                                    finish();
                                }
                            }
                        });
                    }else{
                        Toast.makeText(PostActivity.this, "Please write a post",Toast.LENGTH_SHORT)
                                .show();
                    }

                }else{
                    Toast.makeText(PostActivity.this, "No Internet Connection Found",Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });


    }

    private void dispatchTakePictureIntent() {
        // Check if there is a camera.
        PackageManager packageManager = PostActivity.this.getPackageManager();
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Toast.makeText(PostActivity.this, "This device does not have a camera.", Toast.LENGTH_SHORT)
                    .show();
            return;
        }

        // Camera exists? Then proceed...
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(PostActivity.this.getPackageManager()) != null) {
            // Create the File where the photo should go.
            // If you don't do this, you may get a crash in some devices.
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast toast = Toast.makeText(PostActivity.this,
                        "There was a problem saving the photo...", Toast.LENGTH_SHORT);
                toast.show();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri fileUri = Uri.fromFile(photoFile);
                setCapturedImageURI(fileUri);
                setCurrentPhotoPath(fileUri.getPath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        fileUri);
                startActivityForResult(takePictureIntent, TAKE_PHOTO);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
//        setCurrentPhotoPath("file:" + image.getAbsolutePath());
        return image;
    }

    public void setCapturedImageURI(Uri capturedImageURI) {
        this.capturedImageURI = capturedImageURI;
    }

    public Uri getCapturedImageURI() {
        return capturedImageURI;
    }

    public void setCurrentPhotoPath(String currentPhotoPath) {
        this.currentPhotoPath = currentPhotoPath;
    }

    public String getCurrentPhotoPath() {
        return currentPhotoPath;
    }
}
