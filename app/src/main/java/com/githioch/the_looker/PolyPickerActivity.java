package com.githioch.the_looker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.githioch.the_looker.util.ConnectionDetector;
import com.githioch.the_looker.util.ImageInternalFetcher;
import com.githioch.the_looker.util.ImagePickerActivity;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;


/**
 * Created by Githioch on 7/7/2015.
 */
public class PolyPickerActivity extends AppCompatActivity{

    private static final String TAG = PolyPickerActivity.class.getSimpleName();

    private static final int INTENT_REQUEST_GET_IMAGES = 13;
    private static final int INTENT_REQUEST_GET_N_IMAGES = 14;
    private static final int TAKE_PHOTO = 98;
    private static final int RESULT_LOAD_IMG = 100;

    private Context mContext;

    private ViewGroup mSelectedImagesNone;
    HashSet<Uri> mMedia = new HashSet<Uri>();
    private boolean comingFromGallery = false;
    private ImageView post;
    private EditText captionField;
    private ImageView thumbnail;
    private ProgressWheel progressWheel;
    private android.support.v7.app.ActionBar menu;
    private ImageView pickAnotherImage;
    private String area = "";
    private ImageView removeImage;
    private Spinner spinner;
    private boolean visiting;

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    private void post()
    {
        ConnectionDetector connectionDetector = new ConnectionDetector(mContext);

        if(connectionDetector.isConnectingToInternet()){
            final String post = captionField.getText().toString();
            String tag = spinner.getSelectedItem().toString();

            if(!post.equals("")) {
                if(!tag.equals("Pick One")){
                    progressWheel.setVisibility(View.VISIBLE);
                    progressWheel.bringToFront();
                    progressWheel.spin();
                /*InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);*/

                    final HashMap<String, Object> params = new HashMap<String, Object>();
                    params.put("installationId", ParseInstallation.getCurrentInstallation().getInstallationId());


                    ParseObject parseObject = null;
                    parseObject = new ParseObject("News_Feed_Test");
                    if(!area.equals("")){
                        parseObject.put("area", area);
                        params.put("area", area);
                    }else{
                        if(ParseUser.getCurrentUser().getString("area") != null){
                            parseObject.put("area", ParseUser.getCurrentUser().getString("area"));
                            params.put("area", ParseUser.getCurrentUser().getString("area"));
                        }
                    }
                    parseObject.put("tag", tag);

                    if (ParseUser.getCurrentUser() != null) {
                        if (ParseUser.getCurrentUser().has("profile")) {
                            parseObject.put("facebookName", ParseUser.getCurrentUser().getJSONObject("profile")
                                    .optString("name"));
                            parseObject.put("facebookId", ParseUser.getCurrentUser().getJSONObject("profile")
                                    .optString("facebookId"));

                        } else {
                            parseObject.put("name", ParseUser.getCurrentUser().getString("name"));
                            parseObject.put("parseUserWhoPostedId", ParseUser.getCurrentUser().getObjectId());
                            if(ParseUser.getCurrentUser().getParseFile("parseIcon") != null){
                                parseObject.put("parseIcon", ParseUser.getCurrentUser().getParseFile("parseIcon"));
                            }
                            //compress parse image and send

                        }
                        parseObject.put("userId", ParseUser.getCurrentUser().getObjectId());

                    } else {
                        parseObject.put("name", "not signed in");
                    }
                    parseObject.put("status", post);
                    parseObject.put("newComments", 0);
                    parseObject.put("newGoteas", 0);

                    if (thumbnail.getDrawable() != null) {
//                    final long startTime = System.nanoTime();

                        Drawable d = thumbnail.getDrawable();// the drawable (Captain Obvious, to the rescue!!!)
                        Bitmap bitmap = drawableToBitmap(d);

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        byte[] bitmapData = stream.toByteArray();

                        final ParseFile parseFile = new ParseFile("file.jpg", bitmapData);
                        parseObject.put("image", parseFile);
                        parseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    Log.d("saving", "done");

                                ParseCloud.callFunctionInBackground("notification", params);

                                /*long endTime = System.nanoTime();

                                long duration = (endTime - startTime)/1000000000;
                                Log.d("time to post image", String.valueOf(duration));*/

                                    progressWheel.setVisibility(View.GONE);
                                    Toast.makeText(getApplicationContext(), "Posted successfully", Toast.LENGTH_SHORT)
                                            .show();
//                            finish();
                                    if(visiting){
                                        Intent returnIntent = new Intent(mContext, AreaBeingVisitedActivity.class);
                                        returnIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        returnIntent.putExtra("area", area);
                                        startActivity(returnIntent);
                                        finish();

                                    }else{
                                        Intent returnIntent = new Intent(mContext, MainActivity.class);
                                        returnIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        returnIntent.putExtra("reload", true);
                                        startActivity(returnIntent);
                                        finish();
                                    }

                                } else {
                                    Toast.makeText(mContext, "Error sending post." +
                                            " Try again later.", Toast.LENGTH_SHORT)
                                            .show();
                                }
                            }
                        });
                    }else{
                        parseObject.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if(e == null){
                                    Log.d("post saved", "without image");

                                    ParseCloud.callFunctionInBackground("notification", params);

                                    progressWheel.setVisibility(View.GONE);
                                    Toast.makeText(getApplicationContext(), "Posted successfully", Toast.LENGTH_SHORT)
                                            .show();

                                    if(visiting){
                                        Intent returnIntent = new Intent(mContext, AreaBeingVisitedActivity.class);
                                        returnIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        returnIntent.putExtra("area", area);
                                        startActivity(returnIntent);
                                        finish();
                                    }else{
                                        Intent returnIntent = new Intent(mContext, MainActivity.class);
                                        returnIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        returnIntent.putExtra("reload", true);
                                        startActivity(returnIntent);
                                        finish();
                                    }

                                }else{
                                    Toast.makeText(mContext, "Error sending post." +
                                            " Try again later.", Toast.LENGTH_SHORT)
                                            .show();
                                }
                            }
                        });
                    }
                } else{
                    Toast.makeText(mContext, "Please pick a tag",Toast.LENGTH_SHORT)
                            .show();
                }

            }else{
                Toast.makeText(mContext, "Please write a post",Toast.LENGTH_SHORT)
                        .show();
            }

        }else{
            Toast.makeText(mContext, "No Internet Connection Found",Toast.LENGTH_SHORT)
                    .show();
        }
    }
    private View.OnClickListener postClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            post();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poly_picker);
        mContext = PolyPickerActivity.this;

        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();

        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
            menu.setTitle("");
        }

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();


        if(getIntent().getExtras() != null){
            if(getIntent().getIntExtra("typeSource", 0) == TAKE_PHOTO ||
                    getIntent().getIntExtra("typeSource", 0) == RESULT_LOAD_IMG){
                getImage();
            }

            if(getIntent().getStringExtra("area") != null){
                area = getIntent().getStringExtra("area");
            }
            visiting = getIntent().getBooleanExtra("visiting", false);
        }

        LinearLayout addTag = (LinearLayout)findViewById(R.id.poly_picker_linear_add_tag);
        spinner = (Spinner)findViewById(R.id.poly_picker_spinner_select_area_to_post);
        String[] arraySpinner = getResources().getStringArray(R.array.types_of_crime);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.simple_spinner_item, arraySpinner);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        pickAnotherImage = (ImageView)findViewById(R.id.btn_choose_another_image);
        post = (ImageView)findViewById(R.id.poly_picker_post);
        captionField = (EditText)findViewById(R.id.poly_picker_edit);
         thumbnail = (ImageView)findViewById(R.id.media_image);
        progressWheel = (ProgressWheel)findViewById(R.id.progress_wheel);
        removeImage = (ImageView)findViewById(R.id.remove_image);

        if(Intent.ACTION_SEND.equals(action) && type != null){
            if("text/plain".equals(type)){
                String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
                if (sharedText != null){
                    captionField.setText(sharedText);
                }
            }
        }

        if(thumbnail.getDrawable() == null){
            removeImage.setVisibility(View.GONE);
        }
        removeImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(thumbnail != null){
                    thumbnail.setImageDrawable(null);
                    removeImage.setVisibility(View.GONE);
                }
            }
        });

        if(!area.equals("")){
            captionField.setHint("Enter post for " + area);
        }
        post.setOnClickListener(postClick);

        pickAnotherImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mMedia.clear();
                getImage();
            }
        });
    }

    private void getImage() {
        Intent intent = new Intent(mContext, ImagePickerActivity.class);
        startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES);
    }

    //TODO configs
    /*private void getNImages() {
        Intent intent = new Intent(mContext, ImagePickerActivity.class);
        Config config = new Config.Builder()
                .setTabBackgroundColor(R.color.white)    // set tab background color. Default white.
                .setTabSelectionIndicatorColor(R.color.blue)
                .setCameraButtonColor(R.color.green)
                .setSelectionLimit(1)    // set photo selection limit. Default unlimited selection.
                .build();
        ImagePickerActivity.setConfig(config);
        startActivityForResult(intent, INTENT_REQUEST_GET_N_IMAGES);
    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == INTENT_REQUEST_GET_IMAGES || requestCode == INTENT_REQUEST_GET_N_IMAGES) {
                Parcelable[] parcelableUris = data.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);

                if(data.getStringExtra("gallery") != null){
                    comingFromGallery = true;
                }

                if (parcelableUris == null) {
                    return;
                }

                // Java doesn't allow array casting, this is a little hack
                Uri[] uris = new Uri[parcelableUris.length];
                System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);

                if (uris != null) {
                    for (Uri uri : uris) {
                        Log.i(TAG, " uri: " + uri);
                        mMedia.add(uri);
                    }

                    showMedia();
                }

            }
        }

        if(resultCode == RESULT_CANCELED){
            captionField.setHint("Enter post...");
        }
    }
    private void showMedia() {
        // Remove all views before
        // adding the new ones.

        Iterator<Uri> iterator = mMedia.iterator();
        ImageInternalFetcher imageFetcher = new ImageInternalFetcher(this, 500);
        while (iterator.hasNext()) {
            Uri uri = iterator.next();

            // showImage(uri);
            Log.i(TAG, " uri: " + uri);
            if (mMedia.size() >= 1) {

                captionField.setHint("Enter Caption...");

            }

            // View removeBtn = imageHolder.findViewById(R.id.remove_media);
            // initRemoveBtn(removeBtn, imageHolder, uri);

            if (!uri.toString().contains("content://")) {
                // probably a relative uri
                uri = Uri.fromFile(new File(uri.toString()));
            }

            if(comingFromGallery){
                String compressedImage  = compressImage(uri.getPath());
                imageFetcher.loadImage(Uri.parse(compressedImage), thumbnail);

            }else{
               String compressedImage  = compressImage(uri.getPath());
                Bitmap thePic = BitmapFactory.decodeFile(compressedImage);
                //retrieve a reference to the ImageView
                //display the returned cropped image
                thumbnail.setImageBitmap(thePic);
//                imageFrame.setVisibility(View.VISIBLE);
                File file = new File(compressedImage);
                boolean deleted = file.delete();
                if(deleted){
                    Log.d("compressed image", "deleted");
                }
                File fileNew = new File(uri.getPath());
                boolean delete = fileNew.delete();
                if(delete){
                    Log.d("uri image", "deleted");
                }

            }

            removeImage.setVisibility(View.VISIBLE);

//            mSelectedImagesContainer.addView(postingHolder);

            // set the dimension to correctly
            // show the image thumbnail.
            /*int wdpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getResources().getDisplayMetrics());
            int htpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200, getResources().getDisplayMetrics());*/
            /*int wdpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 450, getResources().getDisplayMetrics());
            int htpx = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300, getResources().getDisplayMetrics());
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(wdpx, htpx);
            layoutParams.gravity = Gravity.CENTER;
            thumbnail.setLayoutParams(layoutParams);*/
        }
    }
    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory.
//      Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2,
                new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //delete image saved by camera so that the image saved above is not a duplicate
        /*File file = new File(imageUri);
        boolean deleted = file.delete();
        if(deleted){
            Log.d("image", "deleted");
        }*/


        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }
}
