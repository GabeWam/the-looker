package com.githioch.the_looker;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.widget.ProfilePictureView;
import com.githioch.the_looker.parseloginui.ParseLoginBuilder;
import com.githioch.the_looker.util.ConnectionDetector;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.ProgressCallback;
import com.parse.SaveCallback;
import com.pnikosis.materialishprogress.ProgressWheel;
import com.soundcloud.android.crop.Crop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


/**
 * Created by Githioch on 6/2/2015.
 *
 * Class for settings in the app
 * User can:
 *  Change image if Parse user
 *  Log out
 *  Add or change phone number
 *  Request to change location
 */
//TODO remove page from stack after user logs out
//TODO check if location is changed during on create so that the status of the change button is changed to normal
public class SettingsActivity extends AppCompatActivity {
    private static final int LOGIN_REQUEST = 0;
    private static final int RESULT_LOAD_IMG = 1;
    private static final int PIC_CROP = 2;
    private static final String PREF_FILE_NAME = "locationChange";
    private static final String KEY_USER_REQUESTED_TO_CHANGE_LOCATION = "user_requested";

    private TextView titleTextView;
    private TextView emailTextView;
    private TextView nameTextView;
    private Button loginOrLogoutButton;
    private ImageView parseImageView;
    private Button addParseImage;
    private Button okay;
    private android.support.v7.app.ActionBar menu;
    private boolean userRequestedToChangeLocation;


    private ParseUser currentUser;
    private ProfilePictureView userProfilePictureView;
    private String imgDecodableString;
    private ImageLoader imageLoader;
    private ProgressWheel progressWheel;
    private DrawerLayout mDrawerLayout;
    private NavigationDrawerFragment navigationDrawerFragment;
    private Button btnDeleteParseImage;
    private Button changeLocation;

    public String compressImage(String imageUri) {

        String filePath = getRealPathFromURI(imageUri);
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;

    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("on activity result", "called");

        try {
            if (requestCode == Crop.REQUEST_PICK && resultCode == RESULT_OK) {
                beginCrop(data.getData());
            }else if (requestCode == Crop.REQUEST_CROP) {
                handleCrop(resultCode, data);
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void handleCrop(int resultCode, Intent result) {
        if (resultCode == RESULT_OK) {
            String imagePath = Crop.getOutput(result).getPath();
            String compressedImage  = compressImage(imagePath);
            Bitmap thePic = BitmapFactory.decodeFile(compressedImage);
            //retrieve a reference to the ImageView
//display the returned cropped image
            if(userProfilePictureView.getVisibility() == View.VISIBLE){
                userProfilePictureView.setVisibility(View.GONE);
            }
            parseImageView.setImageBitmap(thePic);
            parseImageView.setVisibility(View.VISIBLE);
            okay.setVisibility(View.VISIBLE);

        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void beginCrop(Uri source) {
        Log.d("begin crop", "called");
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    private void performCrop(Uri selectedImage) {

        try {
            //call the standard crop action intent (the user device may not support it)
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            //indicate image type and Uri
            cropIntent.setDataAndType(selectedImage, "image/*");
            //set crop properties
            cropIntent.putExtra("crop", "true");

            //indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            //indicate output X and Y
            cropIntent.putExtra("outputX", 800);
            cropIntent.putExtra("outputY", 800);
            //retrieve data on return
            cropIntent.putExtra("return-data", true);

            File f = new File(Environment.getExternalStorageDirectory(),
                    "/temporary_holder.jpg");
            if(f.exists()){
                Log.d("file","exists");
                boolean deleted = f.delete();
                Log.d("deleted", String.valueOf(deleted));
            }
            try {
               boolean created =  f.createNewFile();
                Log.d("created", String.valueOf(created));
            } catch (IOException ex) {
                Log.e("io", ex.getMessage());
            }

            Uri uri = Uri.fromFile(f);

            cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            //start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);

        }
        catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    public static void saveToSharedPreferences(Context context, String preferenceName, String preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }

    private boolean validPhoneNumber(String phoneNumber) {

        if (phoneNumber.matches("[0-9]+") && phoneNumber.length() == 10) {
            return true;
        } else {
            // TODO more error checking for phone number
            return false;
        }

    }

    private void showDialogWithReason(int why_we_ask) {
        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder
                .setTitle("Why we ask")
                .setMessage(why_we_ask)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        alertDialog = builder.create();//AlertDialog dialog; create like this outside onClick
        alertDialog.show();

    }

    private void underlineText(TextView whyWeAsk) {
        SpannableString content = new SpannableString(whyWeAsk.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, whyWeAsk.getText().length(), 0);
        whyWeAsk.setText(content);
    }

    @Override
    protected void onResume() {
        super.onResume();
       /* currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
            showProfileLoggedIn();
        } else {
            showProfileLoggedOut();
        }*/

    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

//        deleteSharedPreferences();

        // http://stackoverflow.com/questions/2480288/programmatically-obtain-the-phone-number-of-the-android-phone
        // getting this weird result - [ 08-06 11:46:54.684 14226:14236 D/dalvikvm ]
        // does not always work and depends on the carrier and SIM apparently
        /*TelephonyManager tMgr = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();
        Log.d("phone number", mPhoneNumber);*/

        String area = readFromPreferences(this, KEY_USER_REQUESTED_TO_CHANGE_LOCATION, "area");
        if(ParseUser.getCurrentUser() != null &&
        !area.equals(ParseUser.getCurrentUser().getString("area"))){
            userRequestedToChangeLocation  = false;
        }else {
            userRequestedToChangeLocation = true;
        }

        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();

        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
            menu.setTitle("Settings");
        }

        imageLoader = ImageLoader.getInstance();
        titleTextView = (TextView) findViewById(R.id.profile_title);
        nameTextView = (TextView) findViewById(R.id.profile_name);
        loginOrLogoutButton = (Button) findViewById(R.id.login_or_logout_button);
        userProfilePictureView = (ProfilePictureView) findViewById(R.id.userProfilePicture);
        titleTextView.setText(R.string.profile_title_logged_in);
        parseImageView = (ImageView) findViewById(R.id.parse_image);
        addParseImage = (Button) findViewById(R.id.add_parse_image);
        okay = (Button) findViewById(R.id.choose_image);
        progressWheel = (ProgressWheel)findViewById(R.id.progress_wheel);
        progressWheel.setBarColor(getResources().getColor(R.color.primary_dark));
        changeLocation = (Button)findViewById(R.id.change_location_button);
        Button btnPhoneNumber = (Button)findViewById(R.id.settings_phone_number);
        TextView whyWeAsk = (TextView)findViewById(R.id.area_why_we_ask);
        btnDeleteParseImage = (Button)findViewById(R.id.delete_parse_image);

        btnDeleteParseImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alertDialog;

                AlertDialog.Builder builder = new AlertDialog.Builder(SettingsActivity.this);

                builder.setTitle("Delete Image")
                        .setMessage("Are you sure you want to delete the image?")
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                progressWheel.setVisibility(View.VISIBLE);
                                progressWheel.spin();

                                ParseUser.getCurrentUser().remove("image");
                                ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        Toast.makeText(getApplicationContext(), "Image deleted", Toast.LENGTH_SHORT)
                                                .show();

                                        userProfilePictureView.setVisibility(View.VISIBLE);
                                        userProfilePictureView.setProfileId(null);

                                        parseImageView.setVisibility(View.GONE);

                                        addParseImage.setVisibility(View.VISIBLE);

                                        progressWheel.stopSpinning();
                                        progressWheel.setVisibility(View.GONE);
                                    }
                                });

                            }
                        });

                alertDialog = builder.create();//AlertDialog dialog; create like this outside onClick
                alertDialog.show();

            }
        });

        whyWeAsk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogWithReason(R.string.why_we_ask_phone);
            }
        });

        underlineText(whyWeAsk);

        if(userRequestedToChangeLocation && ParseUser.getCurrentUser() != null){
            changeLocation.setText("Location change pending...");
        }

        btnPhoneNumber.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                changePhoneNumberWithDialog();

            }
        });
        changeLocation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                changeLocationWithDialog();

            }
        });


        currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
            showProfileLoggedIn();
        } else {
            showProfileLoggedOut();
        }

            okay.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {

                    ConnectionDetector connectionDetector = new ConnectionDetector(SettingsActivity.this);
                    if(connectionDetector.isConnectingToInternet()){
                        progressWheel.setVisibility(View.VISIBLE);

                        Drawable d = parseImageView.getDrawable(); // the drawable (Captain Obvious, to the rescue!!!)
                        Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        byte[] bitmapdata = stream.toByteArray();

                        final ParseUser parseUser = ParseUser.getCurrentUser();
                        final ParseFile parseFile = new ParseFile("profilePic.jpg", bitmapdata);

                        ParseFile icon = createIcon(parseFile);
                        parseUser.put("parseIcon", icon);

                        parseFile.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                Log.d("profile image", "saved");
                                parseUser.put("image", parseFile);
                                parseUser.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        Log.d("image", "saved");
                                        progressWheel.setVisibility(View.GONE);
                                        okay.setVisibility(View.GONE);
                                        Toast.makeText(SettingsActivity.this, "Image Saved", Toast.LENGTH_SHORT)
                                                .show();
                                        Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                    }
                                });
                            }
                        }, new ProgressCallback() {
                            @Override
                            public void done(Integer integer) {
                                progressWheel.setProgress((float) integer);
                            }
                        });


                    }else{
                        Toast.makeText(SettingsActivity.this,"No Internet connection found",Toast.LENGTH_SHORT)
                                .show();
                    }
                    }
                });

            addParseImage.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Create intent to Open Image applications like Gallery, Google Photos
                    Crop.pickImage(SettingsActivity.this);
                }
            });

            loginOrLogoutButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (currentUser != null) {
                        // User clicked to log out.
                        deleteSharedPreferences();
                        ParseUser.logOut();
        currentUser = null;
        /** on your logout method:**/
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.package.ACTION_LOGOUT");
        sendBroadcast(broadcastIntent);

        showProfileLoggedOut();

        changeLocation.setText("Change Location");
    } else {
        // User clicked to log in.
        ParseLoginBuilder loginBuilder = new ParseLoginBuilder(
                SettingsActivity.this);
        startActivityForResult(loginBuilder.build(), LOGIN_REQUEST);
    }
}
});
        }

    private void deleteSharedPreferences() {
        this.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE).edit().clear().commit();
    }

    private void changeLocationWithDialog() {
        if(userRequestedToChangeLocation){
            Toast.makeText(getApplicationContext(), "Location change still pending. Await successful" +
                    " change.", Toast.LENGTH_SHORT).show();
        }else{
            // Creating alert Dialog with one Button
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(SettingsActivity.this);

            //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

            // Setting Dialog Title
            alertDialog.setTitle("Reason for changing");

            // Setting Dialog Message
            alertDialog.setMessage("Enter new location and reason for changing.");

//                alertDialog.setIcon(R.drawable.key);

            LinearLayout layout = new LinearLayout(SettingsActivity.this);
            layout.setOrientation(LinearLayout.VERTICAL);

            final EditText locationBox = new EditText(SettingsActivity.this);
            locationBox.setTextColor(getResources().getColor(R.color.primary_dark_material_dark));
            locationBox.setHint("New Location");
            layout.addView(locationBox);

            final EditText descriptionBox = new EditText(SettingsActivity.this);
            descriptionBox.setTextColor(getResources().getColor(R.color.primary_dark_material_dark));
            descriptionBox.setHint("Reason for changing");
            layout.addView(descriptionBox);

            alertDialog.setView(layout);


            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("Okay",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            if (locationBox.getText().toString().equals("")) {
                                Toast.makeText(getApplicationContext(), "Please enter a new location" +
                                        " and reason for changing.", Toast.LENGTH_SHORT).show();
                            } else {
                                if (descriptionBox.getText().toString().equals("")) {
                                    Toast.makeText(getApplicationContext(), "Please enter a " +
                                            "reason for changing.", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (new ConnectionDetector(SettingsActivity.this).isConnectingToInternet()) {
                                        ParseObject parseObject = new ParseObject("Changing_Location");
                                        parseObject.put("userId", ParseUser.getCurrentUser().getObjectId());
                                        parseObject.put("newLocation", locationBox.getText().toString());
                                        parseObject.put("reasonForChanging", descriptionBox.getText().toString());
                                        parseObject.saveInBackground(new SaveCallback() {
                                            @Override
                                            public void done(ParseException e) {
                                                if (!userRequestedToChangeLocation) {
                                                    userRequestedToChangeLocation = true;
                                                    saveToSharedPreferences(SettingsActivity.this,
                                                            KEY_USER_REQUESTED_TO_CHANGE_LOCATION,
                                                            ParseUser.getCurrentUser().getString("area"));
                                                }

                                                changeLocation.setText("Location change pending...");
                                            }
                                        });
                                        Toast.makeText(getApplicationContext(), "Your location will be changed after " +
                                                "verification.", Toast.LENGTH_SHORT).show();
                                    } else {
                                        Toast.makeText(getApplicationContext(), "No Internet Connection.",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }


                            }
                        }
                    });
            // Setting Negative "NO" Button
            alertDialog.setNegativeButton("Cancel",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Write your code here to execute after dialog
                            dialog.cancel();
                        }
                    });

            // closed

            // Showing Alert Message
            alertDialog.show();
        }
    }

    private void changePhoneNumberWithDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(SettingsActivity.this);

        //AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Add or Change Phone Number");

        // Setting Dialog Message
        alertDialog.setMessage("Enter phone number");
        final EditText input = new EditText(SettingsActivity.this);
        input.setHint("e.g. 0722123456");
        input.setTextColor(getResources().getColor(R.color.primary_dark_material_dark));
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        input.setInputType(InputType.TYPE_CLASS_PHONE);
        alertDialog.setView(input);

//                alertDialog.setIcon(R.drawable.key);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("Okay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int which) {
                        // Write your code here to execute after dialog
                        if(input.getText().toString().equals("")){
                            Toast.makeText(getApplicationContext(), "Please enter a phone" +
                                    " number", Toast.LENGTH_SHORT).show();
                        }else if(!validPhoneNumber(input.getText().toString().trim())){

                            Toast.makeText(getApplicationContext(), "Please enter a valid" +
                                    " phone number", Toast.LENGTH_SHORT).show();
                        }
                        else{
                            if(new ConnectionDetector(SettingsActivity.this).isConnectingToInternet()){
                                ParseUser.getCurrentUser().put("phoneNumber", input.getText().toString().trim());
                                ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        Toast.makeText(getApplicationContext(),
                                                "Phone number savec",
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });

                            }else{
                                Toast.makeText(getApplicationContext(), "No Internet Connection.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                });
        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Write your code here to execute after dialog
                        dialog.cancel();
                    }
                });

        // closed

        // Showing Alert Message
        alertDialog.show();
    }

    private ParseFile createIcon(ParseFile parseFile) {

        byte[] bitmapdata = new byte[0];
        try {
            bitmapdata = parseFile.getData();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
        bitmap = bitmap.createScaledBitmap(bitmap, 50, 50, true);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();

        return new ParseFile("iconImage.png", byteArray);

//        parseObject.put("parseIcon", newParse);

    }


    /**
     * Shows the profile of the given user.
     */
    private void showProfileLoggedIn() {
        Log.d("show logged in", "called");
        if (currentUser.has("profile")) {
            JSONObject userProfile = currentUser.getJSONObject("profile");
            try {
                if (userProfile.has("facebookId")) {
                    userProfilePictureView.setProfileId(userProfile.getString("facebookId"));
                } else {
                    // Show the default, blank user profile picture
                    userProfilePictureView.setProfileId(null);
                }

                if (userProfile.has("name")) {
                    nameTextView.setText(userProfile.getString("name"));
                } else {
                    nameTextView.setText("");
                }
            } catch (JSONException e) {
                Log.d(TheLookerApplication.TAG, "Error parsing saved user data.");
            }
            titleTextView.setText(R.string.profile_title_logged_in);
            loginOrLogoutButton.setText(R.string.profile_logout_button_label);
        }
        else{
            userProfilePictureView.setVisibility(View.GONE);
            btnDeleteParseImage.setVisibility(View.VISIBLE);
            addParseImage.setVisibility(View.VISIBLE);
            if(currentUser.has("name")){
                nameTextView.setText(currentUser.getString("name"));
            }else{
                nameTextView.setText("");
            }
            if(currentUser.has("image")){
                ParseFile parseFile = currentUser.getParseFile("image");

                byte[] bitmapdata = new byte[0];
                try {
                    bitmapdata = parseFile.getData();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);

                parseImageView.setImageBitmap(bitmap);
                parseImageView.setVisibility(View.VISIBLE);
            }
            else{
                parseImageView.setVisibility(View.GONE);
                addParseImage.setVisibility(View.VISIBLE);
                btnDeleteParseImage.setVisibility(View.GONE);
                userProfilePictureView.setVisibility(View.VISIBLE);
                userProfilePictureView.setProfileId(null);
            }
        }
    }

    /**
     * Show a message asking the user to log in, toggle login/logout button text.
     */
    private void showProfileLoggedOut() {
        if(parseImageView.getVisibility() == View.VISIBLE){
            parseImageView.setVisibility(View.GONE);
        }
        if(userProfilePictureView.getVisibility() == View.GONE){
            userProfilePictureView.setVisibility(View.VISIBLE);
        }
        if(addParseImage.getVisibility() == View.VISIBLE){
            addParseImage.setVisibility(View.GONE);
        }
        if(btnDeleteParseImage.getVisibility() == View.VISIBLE){
            btnDeleteParseImage.setVisibility(View.GONE);
        }
        userProfilePictureView.setProfileId(null);
        titleTextView.setText("");
        nameTextView.setText("");
        loginOrLogoutButton.setText(R.string.profile_login_button_label);
    }
}
