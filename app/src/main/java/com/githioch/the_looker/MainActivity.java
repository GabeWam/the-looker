package com.githioch.the_looker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.githioch.the_looker.adapter.FeedbackAdapter;
import com.githioch.the_looker.adapter.ImageAdapter;
import com.githioch.the_looker.parseloginui.ParseLoginActivity;
import com.githioch.the_looker.parseloginui.SelectAreaActivity;
import com.githioch.the_looker.parseloginui.SelectCountyActivity;
import com.githioch.the_looker.parseloginui.TutorialActivity;
import com.githioch.the_looker.parseloginui.WelcomeActivity;
import com.githioch.the_looker.util.ConnectionDetector;
import com.githioch.the_looker.util.HeaderDesign;
import com.githioch.the_looker.util.MaterialViewPager;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.GridHolder;
import com.orhanobut.dialogplus.OnClickListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.orhanobut.dialogplus.ViewHolder;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Githioch on 6/22/2015.
 */
public class MainActivity extends AppCompatActivity {

    private static final int RESULT_LOAD_IMG = 100;
    private static final int PIC_CROP = 2;
    private static final int TAKE_PHOTO = 99;
    private static final String PREF_FILE_NAME = "locationChange";
    private static final String KEY_USER_REQUESTED_TO_CHANGE_LOCATION = "user_requested";

    private static final String PREF_FILE_NAME_FEEDBACK = "feedbackPref";
    public static final String KEY_NUMBER_APP_OPENS = "key_number_app_opens";
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String KEY_USER_FEEDBACK_PREF = "key_user_feedback_pref";

    private MaterialViewPager mViewPager;
    private DrawerLayout mDrawerLayout;
    private BroadcastReceiver broadcastReceiver;
    private int count;
    private Bitmap[] thumbnails;
    private String[] arrPath;
    private ImageAdapter imageAdapter;
    private Toolbar toolbar;
    private ViewPager viewPager;
    private ImageView largeImage;
    private NavigationDrawerFragment navigationDrawerFragment;
    private int headerColor;
    private int drawableId;
    private Menu menu;
    private int numberOfAppOpens;

    public Toolbar getToolbar(){
        return toolbar;
    }
    public MaterialViewPager getmViewPager(){
        return mViewPager;
    }

    public ViewPager getViewPager(){
        return viewPager;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if(intent.getBooleanExtra("reload", false)){
            loadContent();
        }
        /*progressWheel.setVisibility(View.VISIBLE);
        progressWheel.spin();*/
        ViewPager viewPager = mViewPager.getViewPager();

//        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());

    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                Log.d("comments", "closed");
            }
        }

        if(resultCode == RESULT_OK){
            Log.d("load", "image");

            viewPager = mViewPager.getViewPager();

            viewPager.setAdapter(new MyPageAdapter(getSupportFragmentManager()));
            mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());

        }

        *//*if (resultCode == RESULT_OK){

            progressWheel.setVisibility(View.VISIBLE);
            progressWheel.spin();
            ViewPager viewPager = mViewPager.getViewPager();
            List<Fragment> fragments = getFragments();

            viewPager.setAdapter(new MyPageAdapter(getSupportFragmentManager(), fragments));
            mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
            progressWheel.setVisibility(View.GONE);
        }
        if (resultCode == RESULT_CANCELED) {
            //Do nothing?
            Log.d("result","cancelled");
        }*//*
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }

//        saveToSharedPreferencesFeedback(this, KEY_NUMBER_APP_OPENS, ++numberOfAppOpens);

    }

    public void displayLargeImage(Bitmap bitmap){
        largeImage.setImageBitmap(bitmap);
        largeImage.setVisibility(View.VISIBLE);
        mViewPager.setVisibility(View.GONE);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        if(largeImage != null && largeImage.getVisibility() == View.VISIBLE) {
            largeImage.setVisibility(View.GONE);
            mViewPager.setVisibility(View.VISIBLE);
        }else{
            if(numberOfAppOpens == 10){

                String feedbackPref = readFromPreferences(getApplicationContext(),
                        KEY_USER_FEEDBACK_PREF, "yes");

                if(feedbackPref.equals("no")){
                    //close app
                    super.onBackPressed();

                }else{
                    //user had picked maybe later so we show dialog

                    DialogPlus dialog = DialogPlus.newDialog(this)
                            .setContentHolder(new ViewHolder(R.layout.adapter_feedback))
                            .setOnClickListener(new OnClickListener() {
                                @Override
                                public void onClick(final DialogPlus dialog, View view) {
                                    switch (view.getId()){
                                        case R.id.feedback_no:
                                            dialog.dismiss();
                                            saveToSharedPreferences(getApplicationContext(), KEY_USER_FEEDBACK_PREF, "no");
                                            break;
                                        case R.id.feedback_yes:
                                            //won't ask user for feedback again so we set it to no
                                            dialog.dismiss();
                                            saveToSharedPreferences(getApplicationContext(), KEY_USER_FEEDBACK_PREF, "no");
                                            Intent intent = new Intent(getApplicationContext(), FeedbackActivity.class);
                                            startActivity(intent);
                                            break;
                                        case R.id.feedback_maybe_later:
                                            dialog.dismiss();
                                            Toast.makeText(getApplicationContext(), "Okay thanks!", Toast.LENGTH_SHORT)
                                                    .show();
                                            numberOfAppOpens = 0;
                                            saveToSharedPreferencesFeedback(getApplicationContext(), KEY_NUMBER_APP_OPENS, numberOfAppOpens);
                                            saveToSharedPreferences(getApplicationContext(), KEY_USER_FEEDBACK_PREF, "maybe later");
                                            break;
                                    }
                                }
                            })
                            .setExpanded(true)  // This will enable the expand feature, (similar to android L share dialog)
                            .create();
                    dialog.show();
                    numberOfAppOpens = 0;
                    saveToSharedPreferencesFeedback(getApplicationContext(), KEY_NUMBER_APP_OPENS, numberOfAppOpens);
                }
            }else{
                saveToSharedPreferencesFeedback(getApplicationContext(), KEY_NUMBER_APP_OPENS, ++numberOfAppOpens);
                super.onBackPressed();
            }
        }




    }

    public static void saveToSharedPreferences(Context context, String preferenceName, String preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    private View.OnClickListener retryListnener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(new ConnectionDetector(MainActivity.this).isConnectingToInternet()){
                setContentView(R.layout.activity_main);
                loadContent();
            }
        }
    };

    public static void showHashKey(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    "com.githioch.the_looker", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.i("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException | NoSuchAlgorithmException e) {
        }
    }

    private void subScribeUserToPushNotifications(String area) {
        ParsePush.subscribeInBackground(area);
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(preferenceName, defaultValue);
    }

    public static int readFromPreferencesFeedback(Context context, String preferenceName, int defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(preferenceName, defaultValue);
    }

    public static void saveToSharedPreferencesFeedback(Context context, String preferenceName, int preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = sharedPreferences.edit();
        editor.putInt(preferenceName, preferenceValue);
        editor.apply();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String area = readFromPreferences(this, KEY_USER_REQUESTED_TO_CHANGE_LOCATION, "area");
        if(ParseUser.getCurrentUser() != null &&
                area.equals(ParseUser.getCurrentUser().getString("area"))){
            checkForLocationChanges();
        }

//        resetFeedback();

        numberOfAppOpens = readFromPreferencesFeedback(this, KEY_NUMBER_APP_OPENS, 0);



        Log.d(TAG, String.valueOf(numberOfAppOpens));



//        showHashKey(this);

        Intent intent2 = new Intent(getApplicationContext(), SplashScreenActivity.class);
        startActivity(intent2);

//        setContentView(R.layout.activity_main);

        /*setContentView(R.layout.test_button);

        Button button = (Button) findViewById(R.id.test_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TutorialActivity.class);
                startActivity(intent);
            }
        });*/

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.package.ACTION_LOGOUT");
        broadcastReceiver =  new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d("onReceive","Logout in progress");
                //At this point you should start the login activity and finish this one
                Intent intent1 = new Intent(getApplicationContext(), SettingsActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent1);
                finish();
            }
        };
        registerReceiver(broadcastReceiver, intentFilter);


        if(ParseUser.getCurrentUser() != null){
            if(new ConnectionDetector(this).isConnectingToInternet()){
                setContentView(R.layout.activity_main);
                loadContent();
            }else{
                setContentView(R.layout.no_internet);
                Button retry = (Button) findViewById(R.id.retry);
                retry.setOnClickListener(retryListnener);

            }

        }else{
            Intent intent = new Intent(this, ParseLoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void resetFeedback() {
        saveToSharedPreferencesFeedback(this, KEY_NUMBER_APP_OPENS, 0);
        saveToSharedPreferences(this, KEY_USER_FEEDBACK_PREF, "maybe later");
    }

    private void checkForLocationChanges() {
        try {
            ParseUser.getCurrentUser().fetch();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    private void loadContent() {
        largeImage = (ImageView)findViewById(R.id.feed_large_image);
        mViewPager = (MaterialViewPager) findViewById(R.id.materialViewPager);

        TextView textView = (TextView) mViewPager.getLogoContainer().findViewById(R.id.logo_white);
        Log.d("text", textView.getText().toString());

        String area = ParseUser.getCurrentUser().getString("area");
        if(area != null){
            if(area.contains(" (general")){
                area = area.replace(" (general)", "");
            }
            textView.setText(area);
            if(area.equals("Kilimani")){
                headerColor = R.color.accent;
                drawableId = R.drawable.kilimani;
            }
            else if(area.equals("Kileleshwa")){
                headerColor = R.color.blue;
                drawableId = R.drawable.kileleshwa;
            }
            else if(area.equals("Hurlingham")){
                headerColor = R.color.green;
                drawableId = R.drawable.hurlingham;
            }
            else{
                headerColor = R.color.orange;
                drawableId = R.drawable.kilimani;
            }

        }else{
            textView.setText("");
            headerColor = R.color.green;
            drawableId = R.drawable.kilimani;
        }

        if(new ConnectionDetector(this).isConnectingToInternet()){
            mViewPager.setImageDrawable(ContextCompat.getDrawable(MainActivity.this,
                    R.drawable.default_place), 5);

            mViewPager.setMaterialViewPagerListener(new MaterialViewPager.MaterialViewPagerListener() {
                @Override
                public HeaderDesign getHeaderDesign(int i) {
                    switch (i) {
                        case 0:
                            return HeaderDesign.fromColorAndDrawable(
                                    getResources().getColor(headerColor),
                                    ContextCompat.getDrawable(MainActivity.this, R.drawable.default_place)
                            );
                        case 1:
                            return HeaderDesign.fromColorAndDrawable(
                                    getResources().getColor(headerColor),
                                    ContextCompat.getDrawable(MainActivity.this, drawableId)
                            );

                        case 2:
                            return HeaderDesign.fromColorAndDrawable(
                                    getResources().getColor(headerColor),
                                    ContextCompat.getDrawable(MainActivity.this, R.drawable.news)
                            );

                    }
                    return null;
                }
            });
        }
        toolbar = mViewPager.getToolbar();

        if (toolbar != null) {
            setSupportActionBar(toolbar);

            ActionBar actionBar = getSupportActionBar();
            actionBar.setTitle("");
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setHomeButtonEnabled(true);
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        navigationDrawerFragment.setUp(R.id.fragment_navigation_drawer,mDrawerLayout, toolbar);

        viewPager = mViewPager.getViewPager();

        //TODO what if area is null?
        if(!Arrays.asList(getResources().getStringArray(R.array.areas)).contains(area) &&
                !Arrays.asList(getResources().getStringArray(R.array.other_areas)).contains(area)
                && area!= null && !area.equals("Other") && !area.equals("Can't find my location")){

            viewPager.setAdapter(new MyPageAdapterNoSpecificLocation(getSupportFragmentManager()));
        }else{
            viewPager.setAdapter(new MyPageAdapter(getSupportFragmentManager()));
        }


        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());
        mViewPager.getPagerTitleStrip().bringToFront();
        mViewPager.getPagerTitleStrip().setTextColor(getResources().getColor(R.color.primary_dark));
    }

    /*@Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.main, menu);

        this.menu = menu;

        if(new ConnectionDetector(this).isConnectingToInternet()){

            Log.d("senderId", ParseUser.getCurrentUser().getObjectId());

            ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("Messages_To_Ocpd");
            parseQuery.whereEqualTo("senderId", ParseUser.getCurrentUser().getObjectId());
            parseQuery.whereEqualTo("isMessage", false);
            parseQuery.whereEqualTo("readByUser", false);
            parseQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                    if(e == null){
                        if(list != null && list.size() > 0){
                            menu.clear();
                            getMenuInflater().inflate(R.menu.main_notif, menu);
                        }else{
                            Log.d("list", "null");
                        }
                    }
                }
            });
        }


        return true;

    }*/

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_private_message) {
            Intent intent = new Intent(getApplicationContext(), OCPDActivity.class);
            startActivityForResult(intent, 14);
            return true;
        }

        return super.onOptionsItemSelected(item);

    }*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("main onactivity", "passed");
        if(requestCode == 14 && resultCode == RESULT_OK && data != null){

            boolean messageRead = data.getBooleanExtra("messageRead", false);
            if(messageRead){
                resetMenuMessage();
            }
        }
    }

    private void resetMenuMessage() {
        menu.clear();
        getMenuInflater().inflate(R.menu.main, menu);
    }

    class MyPageAdapter extends FragmentStatePagerAdapter {

        public MyPageAdapter(FragmentManager fm) {
            super(fm);
            }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                default:
                    return "";

                case 0:
                    if(ParseUser.getCurrentUser().getString("area") != null){
                        return ParseUser.getCurrentUser().getString("area");
                    }
                    return "Default";

                case 1: // '\001'
                    return "All Posts";
                case 2:
                    return "News";

            }
        }

        @Override
        public Fragment getItem(int position) {

            if(position == 0){
                if(ParseUser.getCurrentUser() != null && ParseUser.getCurrentUser().getString("area") != null){
                    RecyclerViewFragment recyclerViewFragment = new RecyclerViewFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("area", ParseUser.getCurrentUser().getString("area"));
                    bundle.putBoolean("areaDesired", true);
                    recyclerViewFragment.setArguments(bundle);
                    return recyclerViewFragment;
                }else{
                    return RecyclerViewFragment.newInstance();
                }
            }else if(position == 1){
                return RecyclerViewFragment.newInstance();
            }else{
                return RecyclerViewNewsFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return 3;
            }
    }

    class MyPageAdapterNoSpecificLocation extends FragmentStatePagerAdapter {

        public MyPageAdapterNoSpecificLocation(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                default:
                    return "";
                case 0: // '\001'
                    return "All Posts";
                case 1:
                    return "News";

            }
        }

        @Override
        public Fragment getItem(int position) {

            if(position == 0){
                return RecyclerViewFragment.newInstance();
            }else{
                return RecyclerViewNewsFragment.newInstance();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }

}
