package com.githioch.the_looker;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.githioch.the_looker.adapter.FeedListAdapter;
import com.githioch.the_looker.adapter.NotificationsAdapter;
import com.githioch.the_looker.model.Notification;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Githioch on 6/24/2015.
 */
public class ViewNotificationsActivity extends AppCompatActivity
        implements NotificationsAdapter.NotificationsClickListener {

    private FeedListAdapter.RecyclerViewClickListener recyclerViewClickListener;
    private NotificationsAdapter.NotificationsClickListener notificationsClickListener;
    private TextView noNotifications;
    private ListView listView;
    private ArrayList<Notification> notificationArrayList;
    private android.support.v7.app.ActionBar menu;
    private RecyclerView recyclerList;
    private NotificationsAdapter notificationsAdapter;
    private DrawerLayout mDrawerLayout;
    private NavigationDrawerFragment navigationDrawerFragment;
    private Integer numberOfNotifsToReturnToDrawer;
    private ImageView notificationLargeImage;

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(notificationLargeImage.getVisibility() == View.VISIBLE){
            notificationLargeImage.setVisibility(View.GONE);
            recyclerList.setVisibility(View.VISIBLE);
        }else{
            Intent returnIntent = new Intent();
            returnIntent.putExtra("newNotificationNumber", numberOfNotifsToReturnToDrawer);
            returnIntent.putExtra("notifs", notificationArrayList);
            setResult(RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent returnIntent = new Intent();
                returnIntent.putExtra("newNotificationNumber", numberOfNotifsToReturnToDrawer);
                returnIntent.putExtra("notifs", notificationArrayList);
                setResult(RESULT_OK, returnIntent);
                finish();
        }
        return (super.onOptionsItemSelected(item));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_view_notifications);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();

        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
            menu.setTitle("Notifications");
        }

        notificationsClickListener = this;
        notificationArrayList = new ArrayList<>();

        notificationLargeImage = (ImageView)findViewById(R.id.notification_large_image);
        noNotifications = (TextView)findViewById(R.id.notifications_no_notification);
        recyclerList = (RecyclerView)findViewById(R.id.notifications_recycler);
        recyclerList.setLayoutManager(new LinearLayoutManager(this));

        getNotifications();

        if(numberOfNotifsToReturnToDrawer != null && numberOfNotifsToReturnToDrawer == 0){
            noNotifications.setVisibility(View.VISIBLE);
            recyclerList.setVisibility(View.GONE);
        }else{
            notificationsAdapter = new NotificationsAdapter(this,notificationArrayList,
                    getApplicationContext(), notificationsClickListener);
            recyclerList.setAdapter(notificationsAdapter);
        }
    }

    private void getNotifications() {
        if(getIntent().getExtras() != null){
            notificationArrayList = (ArrayList<Notification>)
                    getIntent().getSerializableExtra("notifs");
            numberOfNotifsToReturnToDrawer = getIntent().getIntExtra("notifSize",0);

        }

    }

    private void updateOldGoteaNotifs(ArrayList<String> notifs) {

        for (int i = 0; i < notifs.size(); i++) {

            final String keyId  = notifs.get(i);
            ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("notifications");
            parseQuery.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
            parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if(parseObject != null){
                        JSONArray jsonArray = parseObject.getJSONArray("notif");
                        for(int j = 0; j < jsonArray.length(); j++){
                            try {
                                JSONObject jsonObject = jsonArray.getJSONObject(j);
                                Iterator<String> iterator = jsonObject.keys();
                                while(iterator.hasNext()){
                                    String key  = iterator.next();
                                    if(key.equals("postId")){
                                        Log.d("passed", "key check");
                                        String postId = jsonObject.getString(key);
                                        if(postId.equals(keyId)){
                                            Log.d("passed", "post id check");
                                            jsonObject.put("newGoteas", 0);
                                            jsonObject.put("oldGoteas", 0);
                                            parseObject.save();
                                        }
                                    }

                                }
                            } catch (JSONException | ParseException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                }
            });
        }


    }

    private void updateOldNotifs(ArrayList<String> notifs) {

            for (int i = 0; i < notifs.size(); i++) {

                final String keyId  = notifs.get(i);
                ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("notifications");
                parseQuery.whereEqualTo("userId", ParseUser.getCurrentUser().getObjectId());
                parseQuery.getFirstInBackground(new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        if(parseObject != null){
                            JSONArray jsonArray = parseObject.getJSONArray("notif");
                            for(int j = 0; j < jsonArray.length(); j++){
                                try {
                                    JSONObject jsonObject = jsonArray.getJSONObject(j);
                                    Iterator<String> iterator = jsonObject.keys();
                                    while(iterator.hasNext()){
                                        String key  = iterator.next();
                                        if(key.equals("postId")){
                                            Log.d("passed", "key check");
                                            String postId = jsonObject.getString(key);
                                            if(postId.equals(keyId)){
                                                Log.d("passed", "post id check");
                                                jsonObject.put("newComments", 0);
                                                jsonObject.put("oldComments", 0);
                                                parseObject.save();
                                            }
                                        }

                                    }
                                } catch (JSONException | ParseException e1) {
                                    e1.printStackTrace();
                                }
                            }
                        }
                    }
                });
            }


    }


    @Override
    public void NotificationClicked(String id, boolean isComment, int position) {
        Intent intent  = new Intent(getApplicationContext(), CommentsActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
        numberOfNotifsToReturnToDrawer -= 1;
        Log.d("clicked", String.valueOf(numberOfNotifsToReturnToDrawer));
        notificationArrayList.get(position).setIsClicked(true);
        resetNotificationsForThisPost(id, isComment);


    }

    private void resetNotificationsForThisPost(String id, final boolean isComment) {

    ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("News_Feed_Test");
    parseQuery.getInBackground(id, new GetCallback<ParseObject>() {
        @Override
        public void done(ParseObject parseObject, ParseException e) {
            if (isComment) {
                parseObject.put("newComments", 0);
            } else {
                parseObject.put("newGoteas", 0);
            }
            parseObject.saveInBackground();
        }
    });

    }

    public void displayLargeImage(Bitmap bitmap) {
        recyclerList.setVisibility(View.GONE);
        notificationLargeImage.setVisibility(View.VISIBLE);
        notificationLargeImage.setImageBitmap(bitmap);

    }


}
