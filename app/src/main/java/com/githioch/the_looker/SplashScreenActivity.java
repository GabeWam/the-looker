package com.githioch.the_looker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

/**
 * Created by Githiora Wamunyu on 9/21/2015.
 */
public class SplashScreenActivity extends Activity{

    /**
     * The thread to process splash screen events
     */
    private Thread mSplashThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        final SplashScreenActivity sPlashScreen = this;

        // The thread to wait for splash screen events
        mSplashThread =  new Thread(){
            @Override
            public void run(){
                try {
                    synchronized(this){
                        // Wait given period of time or exit on touch
                        wait(2000);
                    }
                }
                catch(InterruptedException ex){
                }

                finish();

                // Run next activity
                /*Intent intent = new Intent();
                intent.setClass(sPlashScreen, MainActivity.class);
                startActivity(intent);
                this.interrupt();*/
            }
        };

        mSplashThread.start();
    }
}
