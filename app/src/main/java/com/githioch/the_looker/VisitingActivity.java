package com.githioch.the_looker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Githioch on 7/12/2015.
 * Class for visiting another location
 */
public class VisitingActivity extends AppCompatActivity{
    private android.support.v7.app.ActionBar menu;
    private DrawerLayout mDrawerLayout;
    private NavigationDrawerFragment navigationDrawerFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();

        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
            menu.setTitle("Visit another location");
        }

        final Spinner placeToVisit = (Spinner)findViewById(R.id.spinner_select_place_to_visit);
        Button ok  = (Button)findViewById(R.id.btn_visiting_ok);

        String[] areas = getResources().getStringArray(R.array.areas);
        ArrayList<String> arrayList = new ArrayList<String>(Arrays.asList(areas));

        if(ParseUser.getCurrentUser().getString("area") != null){
            if(arrayList.contains(ParseUser.getCurrentUser().getString("area"))){
                arrayList.remove(ParseUser.getCurrentUser().getString("area"));
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        placeToVisit.setAdapter(adapter);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String area = placeToVisit.getSelectedItem().toString();
                Intent intent = new Intent(getApplicationContext(), AreaBeingVisitedActivity.class);
                intent.putExtra("area", area);

                startActivity(intent);

            }
        });
    }
}
