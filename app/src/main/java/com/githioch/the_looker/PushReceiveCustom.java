package com.githioch.the_looker;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * Created by Githiora Wamunyu on 9/29/2015.
 */
public class PushReceiveCustom extends ParsePushBroadcastReceiver {

    private static final String TAG = PushReceiveCustom.class.getSimpleName();
    private int numMessages;
    private static int notificationId  = 111;
    private NotificationCompat.Builder mBuilder;
    private NotificationCompat.Builder parseBuilder;

    private static final String PREF_FILE_NAME = "notificationPref";
    public static final String KEY_NUMBER_NOTIFICATIONS = "key_number_notifications";



    public static int readFromPreferences(Context context, String preferenceName, int defaultValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getInt(preferenceName, defaultValue);
    }

    public static void saveToSharedPreferences(Context context, String preferenceName, int preferenceValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor  editor = sharedPreferences.edit();
        editor.putInt(preferenceName, preferenceValue);
        editor.apply();
    }

    public PushReceiveCustom() {


    }


    @Override
    protected void onPushReceive(Context context, Intent intent) {
//        super.onReceive(context, intent);
         JSONObject pushData = null;

            try {
                pushData = new JSONObject(intent.getStringExtra(KEY_PUSH_DATA));
            } catch (JSONException e) {
                Log.e(TAG, "Unexpected JSONException when receiving push data: ", e);
            }

            // If the push data includes an action string, that broadcast intent is fired.
            String action = null;
            if (pushData != null) {
                action = pushData.optString("action", null);
            }
            if (action != null) {
                Bundle extras = intent.getExtras();
                Intent broadcastIntent = new Intent();
                broadcastIntent.putExtras(extras);
                broadcastIntent.setAction(action);
                broadcastIntent.setPackage(context.getPackageName());
                context.sendBroadcast(broadcastIntent);
            }

            Notification notification = getNotification(context, intent);

            if (notification != null) {
                showNotification(context, notification);
            }
    }

    @Override
    protected Notification getNotification(Context context, Intent intent) {

        JSONObject jsonObject = null;
        try {
            jsonObject =  new JSONObject(intent.getStringExtra(KEY_PUSH_DATA));
        } catch (JSONException e) {
            Log.e(TAG, "Unexpected JSONException when receiving push data: ", e);
            return null;
        }
        if (jsonObject == null || (!jsonObject.has("alert") && !jsonObject.has("title"))) {
            return null;
        }

        String title = jsonObject.optString("title", "The Looker");
        String alert = jsonObject.optString("alert", "Notification received.");
        String tickerText = String.format(Locale.getDefault(), "%s: %s", title, alert);

        Bundle extras = intent.getExtras();

//        Random random = new Random();
        int contentIntentRequestCode = 10;
        int deleteIntentRequestCode = 11;

        // Security consideration: To protect the app from tampering, we require that intent filters
        // not be exported. To protect the app from information leaks, we restrict the packages which
        // may intercept the push intents.
        String packageName = context.getPackageName();

        Intent contentIntent = new Intent(ParsePushBroadcastReceiver.ACTION_PUSH_OPEN);
        contentIntent.putExtras(extras);
        contentIntent.setPackage(packageName);

        Intent deleteIntent = new Intent(ParsePushBroadcastReceiver.ACTION_PUSH_DELETE);
        deleteIntent.putExtras(extras);
        deleteIntent.setPackage(packageName);

        PendingIntent pContentIntent = PendingIntent.getBroadcast(context, contentIntentRequestCode,
                contentIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        PendingIntent pDeleteIntent = PendingIntent.getBroadcast(context, deleteIntentRequestCode,
                deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT);



        numMessages = readFromPreferences(context, KEY_NUMBER_NOTIFICATIONS, 0);
        numMessages++;

        if(numMessages > 1){

            alert = numMessages +" new security posts in your area";

        }

        Log.d("num messages", String.valueOf(numMessages));
        // The purpose of setDefaults(Notification.DEFAULT_ALL) is to inherit notification properties
        // from system defaults
         parseBuilder = new NotificationCompat.Builder(context);
        parseBuilder.setContentTitle(title)
                .setContentText(alert)
                .setTicker(tickerText)
                .setSmallIcon(this.getSmallIconId(context, intent))
                .setLargeIcon(this.getLargeIcon(context, intent))
                .setContentIntent(pContentIntent)
                .setDeleteIntent(pDeleteIntent)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_ALL);

        saveToSharedPreferences(context, KEY_NUMBER_NOTIFICATIONS, numMessages);

        return parseBuilder.build();

    }

    private void showNotification(Context context, Notification notification) {
        if (context != null && notification != null) {
//            notificationCount.incrementAndGet();

            // Fire off the notification
            NotificationManager nm =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            // Pick an id that probably won't overlap anything


            try {
                nm.notify(notificationId, notification);
            } catch (SecurityException e) {
                // Some phones throw an exception for unapproved vibration
                notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND;
                nm.notify(notificationId, notification);
            }

        }
    }


    @Override
    protected void onPushOpen(Context context, Intent intent) {
        super.onPushOpen(context, intent);

        ParseAnalytics.trackAppOpened(intent);

        saveToSharedPreferences(context, KEY_NUMBER_NOTIFICATIONS, 0);

    }
}
