package com.githioch.the_looker;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * Created by Githioch on 7/12/2015.
 */
public class AreaBeingVisitedActivity extends AppCompatActivity{

    private ImageView largeImage;
    private FrameLayout frameLayout;
    private android.support.v7.app.ActionBar menu;
    private Toolbar mToolbar;

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        if(largeImage.getVisibility() == View.VISIBLE){
            mToolbar.setVisibility(View.VISIBLE);
            largeImage.setVisibility(View.GONE);
            frameLayout.setVisibility(View.VISIBLE);
        }else{
            super.onBackPressed();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_being_visited);

        mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();

        String area = getIntent().getStringExtra("area");

        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
            menu.setTitle("Visiting " + area);
        }

        frameLayout = (FrameLayout)findViewById(R.id.frame_main);
        largeImage = (ImageView)findViewById(R.id.feed_large_image);
        RecyclerViewFragment recyclerViewFragment = new RecyclerViewFragment();
        Bundle bundle = new Bundle();
        bundle.putString("area", area);
        bundle.putBoolean("visiting", true);
        recyclerViewFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().
                add(R.id.frame_main, recyclerViewFragment)
                .commit();
    }

    public void displayLargeImage(Bitmap bitmap) {
        mToolbar.setVisibility(View.GONE);
        largeImage.setImageBitmap(bitmap);
        largeImage.setVisibility(View.VISIBLE);
        frameLayout.setVisibility(View.GONE);
    }
}
