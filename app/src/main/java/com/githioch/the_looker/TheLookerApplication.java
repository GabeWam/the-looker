package com.githioch.the_looker;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseCrashReporting;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;

//TODO after deleting post ensure it reflects in other tab
//TODO in ocpd messages tell user to enter first message if no messages have been previously sent
//TODO test disappearance of "create first post"
//TODO when user changes photo run cloud code that updates all places in the server
//TODO image for failure to load
public class TheLookerApplication extends Application {

  static final String TAG = TheLookerApplication.class.getSimpleName();


    @Override
  public void onCreate() {
    super.onCreate();


      FacebookSdk.sdkInitialize(getApplicationContext());

      Parse.enableLocalDatastore(this);

        // Enable Crash Reporting
        ParseCrashReporting.enable(this);

      Parse.initialize(this,
              "K6RHCrSridvlj4iTpMX2QE87U5ooznTbevPC5qXO",
              "QdoRBwulr2Ic8Wqy0kZspkZDPBDbao1joRpDiwkF"
      );
        ParseInstallation.getCurrentInstallation().saveInBackground();

        ParseFacebookUtils.initialize(this);

    // Create global configuration and initialize ImageLoader with this
    // configuration
    ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
            .threadPriority(Thread.NORM_PRIORITY - 2)
            .denyCacheImageMultipleSizesInMemory()
            .diskCacheFileNameGenerator(new Md5FileNameGenerator())
            .tasksProcessingOrder(QueueProcessingType.LIFO)
            .build();

    ImageLoader.getInstance().init(config);

  }
}
//TODO show toast for all parseexceptions that could arise for all parse queries
//TODO performing stop of activity that is not resumed error
//TODO throw some notifications code to the cloud

//TODO going to the next field when creating account is problematic - user has to close keyboard to go to next
//TODO Mas' phone cropping function failing
//TODO Commenting counter should update automatically after commenting
//TODO Copy pasting when commenting not working - SEEMS TO WORK; ASK MAS WHAT THE PROBLEM WAS
//TODO no internet code
//TODO if self comments it should not be added to notifications table
//TODO add notification implementation to liking
//TODO add dialog that asks user if he/she wants to share through app
//TODO cancel all background processes in the case that user navigates away before they are finished
