package com.githioch.the_looker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.githioch.the_looker.adapter.OcpdMessageAdapter;
import com.githioch.the_looker.model.Message;
import com.githioch.the_looker.util.ConnectionDetector;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Githioch on 7/10/2015.
 * Class for sending private messages to OCPD
 */
public class OCPDPrivateMessageActivity extends AppCompatActivity{

    private final String TAG = OCPDPrivateMessageActivity.class.getSimpleName();
    private android.support.v7.app.ActionBar menu;
    private EditText editMessage;
    private Context mContext;
    private TextView time;
    private TextView comment;
    private TextView warningMessage;
    private RecyclerView recyclerView;
    private ArrayList<Message> messageArrayList;
    private OcpdMessageAdapter ocpdMessageAdapter;
    private Activity mActivity;
    private ProgressBar progressBar;
    private boolean messageRead;

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent returnIntent = new Intent();
        returnIntent.putExtra("messageRead",messageRead);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent returnIntent = new Intent();
                returnIntent.putExtra("messageRead",messageRead);
                setResult(RESULT_OK, returnIntent);
                finish();
                return true;
        }
        return (super.onOptionsItemSelected(item));

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ocpd_private_message);

        mActivity = OCPDPrivateMessageActivity.this;

        messageRead = false;

        messageArrayList = new ArrayList<>();

        mContext = OCPDPrivateMessageActivity.this;
        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();

        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
            menu.setTitle("");
        }
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        editMessage = (EditText)findViewById(R.id.editMessage);
        warningMessage = (TextView)findViewById(R.id.ocpd_warning_message);
        recyclerView = (RecyclerView)findViewById(R.id.scrollView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        final Button retry = (Button)findViewById(R.id.retry);
        ImageView imageSend = (ImageView) findViewById(R.id.imageSend);
        imageSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comment();
            }
        });

        if(new ConnectionDetector(this).isConnectingToInternet()){
            getMessages();
            indicateNewMessagesAreRead();
        }else{
            retry.setVisibility(View.VISIBLE);
            warningMessage.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(new ConnectionDetector(OCPDPrivateMessageActivity.this).isConnectingToInternet()){
                    retry.setVisibility(View.GONE);
                    warningMessage.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.VISIBLE);
                    getMessages();
                    indicateNewMessagesAreRead();
                }
            }
        });

    }

    private void indicateNewMessagesAreRead() {
        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("Messages_To_Ocpd");
        parseQuery.whereEqualTo("senderId", ParseUser.getCurrentUser().getObjectId());
        parseQuery.whereEqualTo("isMessage", false);
        parseQuery.whereEqualTo("readByUser", false);
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e == null && list.size() > 0){
                    for (int i = 0; i < list.size(); i++) {
                        ParseObject parseObject = list.get(i);
                        parseObject.put("readByUser", true);
                        parseObject.saveInBackground();
                    }
                }
            }
        });
    }

    private void getMessages() {
        ParseQuery<ParseObject> parseQuery =  new ParseQuery<ParseObject>("Messages_To_Ocpd");
        parseQuery.orderByAscending("createdAt");
        //TODO parse users
        if(ParseUser.getCurrentUser().has("profile")){
            parseQuery.whereEqualTo("senderName", ParseUser.getCurrentUser().getJSONObject("profile")
                    .optString("name"));
        }else{
            parseQuery.whereEqualTo("senderName", ParseUser.getCurrentUser().getString("name"));
        }
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if(e == null){
                    if(list.size() > 0){
                        for (int i = 0; i < list.size(); i++) {
                            ParseObject parseObject = list.get(i);
                            if(parseObject.getBoolean("isMessage")){
                                Message message = new Message();
                                message.setMessage(parseObject.getString("message"));

                                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
                                formatter.setTimeZone(TimeZone.getTimeZone("Africa/Nairobi"));
                                Date d = parseObject.getCreatedAt();

                                try {
                                    d = formatter.parse(formatter.format(d));
                                } catch (java.text.ParseException e1) {
                                    e1.printStackTrace();
                                }

                                CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                                        d.getTime(),
                                        System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);

                                message.setDate(timeAgo);
                                message.setIsMessage(true);

                                messageArrayList.add(message);

                            }else {
                                Message response = new Message();
                                response.setMessage(parseObject.getString("response"));

                                DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
                                formatter.setTimeZone(TimeZone.getTimeZone("Africa/Nairobi"));
                                Date d = parseObject.getCreatedAt();

                                try {
                                    d = formatter.parse(formatter.format(d));
                                } catch (java.text.ParseException e1) {
                                    e1.printStackTrace();
                                }

                                CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                                        d.getTime(),
                                        System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);

                                response.setDate(timeAgo);
                                response.setIsMessage(false);

                                messageArrayList.add(response);
                            }
                        }
                    }
                }

                ocpdMessageAdapter = new OcpdMessageAdapter(getApplicationContext(),
                        messageArrayList, mActivity);

                messageRead = true;

                recyclerView.setAdapter(ocpdMessageAdapter);
                warningMessage.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                recyclerView.scrollToPosition(messageArrayList.size() - 1);

            }
        });

    }

    private void comment() {
        if (editMessage.getText().toString().equals("")) {
            Toast.makeText(mContext, "Please write something.", Toast.LENGTH_SHORT).show();
        } else {
            Message message = new Message();
            message.setNewMessage(editMessage.getText().toString());

            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
            formatter.setTimeZone(TimeZone.getTimeZone("Africa/Nairobi"));
            Date d = Calendar.getInstance().getTime();

            try {
                d = formatter.parse(formatter.format(d));
            } catch (java.text.ParseException e1) {
                e1.printStackTrace();
            }

            CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                    d.getTime(),
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);

            message.setDate(timeAgo);
            message.setIsNewMessage(true);

            messageArrayList.add(message);
            ocpdMessageAdapter.notifyDataSetChanged();


            warningMessage.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

            editMessage.setText("");

            InputMethodManager imm = (InputMethodManager) getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editMessage.getWindowToken(), 0);

            recyclerView.scrollToPosition(messageArrayList.size() - 1);
        }
    }
}
