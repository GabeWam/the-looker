package com.githioch.the_looker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pnikosis.materialishprogress.ProgressWheel;

/**
 * Created by Githiora Wamunyu on 8/16/2015.
 */
public class FeedbackActivity extends AppCompatActivity{
    private android.support.v7.app.ActionBar menu;

    private void underlineText(TextView whyWeAsk) {
        SpannableString content = new SpannableString(whyWeAsk.getText().toString());
        content.setSpan(new UnderlineSpan(), 0, whyWeAsk.getText().length(), 0);
        whyWeAsk.setText(content);
    }

    private void showDialogWithReason(int why_we_ask) {
        AlertDialog alertDialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder
                .setTitle("Why we ask")
                .setMessage(why_we_ask)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        alertDialog = builder.create();//AlertDialog dialog; create like this outside onClick
        alertDialog.show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();

        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
            menu.setTitle("Feedback");
        }

        final EditText editText = (EditText)findViewById(R.id.feedback_edit);
        Button send = (Button)findViewById(R.id.feedback_send);
        final ProgressWheel progressWheel = (ProgressWheel)findViewById(R.id.progress_wheel);
        progressWheel.setBarColor(getResources().getColor(R.color.progress_wheel_blue));
        TextView whyWeAsk = (TextView)findViewById(R.id.why_we_ask);

        underlineText(whyWeAsk);

        whyWeAsk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogWithReason(R.string.why_we_ask_for_feedback);
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String feedback = editText.getText().toString();

                if(feedback.equals("")){
                    Toast.makeText(getApplicationContext(), "Please enter feedback", Toast.LENGTH_SHORT).show();
                }else{

                    InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);

                    progressWheel.setVisibility(View.VISIBLE);
                    progressWheel.spin();

                    ParseObject parseObject = new ParseObject("feedback");
                    parseObject.put("userId", ParseUser.getCurrentUser().getObjectId());
                    parseObject.put("feedback", feedback);
                    parseObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e == null){
                                progressWheel.stopSpinning();
                                progressWheel.setVisibility(View.GONE);

                                editText.setText("");

                                Toast.makeText(getApplicationContext(), "Feedback Sent. Thank you!",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });


    }
}
