package com.githioch.the_looker;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.githioch.the_looker.adapter.FeedListAdapter;
import com.githioch.the_looker.model.FeedItem;
import com.githioch.the_looker.util.ConnectionDetector;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Githioch on 6/2/2015.
 */
//TODO onrefreshlistener
public class TestingFragment extends Fragment{

    private static final String TAG = TestingFragment.class.getSimpleName();
    private ListView listView;
    private ArrayList<FeedItem> feedItems;
    private String URL_FEED = "http://api.androidhive.info/feed/feed.json";
    private ParseQueryAdapter parseQueryAdapter;
    private RecyclerView newsFeed;
    private FeedListAdapter feedListAdapter;
    private String picturePath;
    private boolean onResumeCalled = false;
    private RelativeLayout myLayout;
    private FeedListAdapter.RecyclerViewClickListener recyclerViewClickListener;
    private OnLookSelectedListener onLookSelectedListener;
    private ProgressWheel progressWheel;
    private RelativeLayout noInternet;
    private Button retry;


    public interface OnLookSelectedListener {
        void onLookSelected(String objectId);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            onLookSelectedListener = (OnLookSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement onLookSelectedListener");
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*feedItems = new ArrayList<FeedItem>();
        recyclerViewClickListener = this;*/

    }


    @Override
    public void onResume() {
        super.onResume();
        if(myLayout != null){
            myLayout.requestFocus();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.layout_testing_fragment, container, false);
        myLayout = (RelativeLayout) rootView.findViewById(R.id.my_layout);
        noInternet = (RelativeLayout)rootView.findViewById(R.id.no_internet);
        retry = (Button)rootView.findViewById(R.id.retry);
        myLayout.requestFocus();


        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("retry","clicked");
                ConnectionDetector connectionDetector  = new ConnectionDetector(getActivity());
                if(connectionDetector.isConnectingToInternet()){
                    newsFeed.setVisibility(View.VISIBLE);
                    noInternet.setVisibility(View.GONE);
                    getData();
                }
            }
        });

        // These two lines not needed,
        // just to get the look of facebook (changing background color & hiding the icon)
        /*getActivity().getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#3b5998")));
        getActivity().getActionBar().setIcon(
                new ColorDrawable(getResources().getColor(android.R.color.transparent)));*/


        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("result", "called");
        getData();
        feedListAdapter.notifyDataSetChanged();

    }

    private void getData() {

        Log.d("getdata", "alled");
        progressWheel.setVisibility(View.VISIBLE);
        progressWheel.spin();

        ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("News_Feed_Test");
        parseQuery.orderByDescending("createdAt");
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                for (int i = 0; i < list.size(); i++) {
                    FeedItem feedItem = new FeedItem(
                            list.get(i).getString("tag"),
                            list.get(i).getString("userId"),
                            list.get(i).getObjectId(),
                            list.get(i).getString("parseUserWhoPosted"),
                            list.get(i).getString("name"),
                            list.get(i).getString("status"),
                            list.get(i).getParseFile("image"),
                            list.get(i).getParseFile("parseIcon"),
                            list.get(i).getString("facebookId"),
                            list.get(i).getString("facebookName"),
                            list.get(i).getNumber("goteas"),
                            (List)list.get(i).getList("parseUsersWhoGotead"),
                            (List)list.get(i).getList("fbUsersWhoGotead"),
                            list.get(i).getCreatedAt(),
                            list.get(i).getInt("numberOfComments"));

                    feedItems.add(feedItem);
                }
                    /*feedListAdapter = new FeedListAdapter(getActivity(), recyclerViewClickListener, mActivity);
                    feedListAdapter.setFeedList(feedItems);
                    newsFeed.setAdapter(feedListAdapter);
                progressWheel.setVisibility(View.GONE);
                progressWheel.stopSpinning();*/

            }
        });
    }
}
