package com.githioch.the_looker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.githioch.the_looker.adapter.CommentsAdapter;
import com.githioch.the_looker.model.Comment;
import com.githioch.the_looker.util.ConnectionDetector;
import com.githioch.the_looker.util.ProfilePictureView;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.pnikosis.materialishprogress.ProgressWheel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Githioch on 7/9/2015.
 */
public class CommentsActivity extends AppCompatActivity {
    private android.support.v7.app.ActionBar menu;
    private final String TAG = CommentsActivity.class.getSimpleName();
    private EditText editMessage;
    private Context mContext;
    private ProfilePictureView profilePictureView;
    private TextView time;
    private TextView userName;
    private TextView comment;
    private TextView name;
    private String commentId;
    private TextView noComments;
    private CommentsAdapter commentsAdapter;
    private ArrayList<Comment> commentArrayList;
    private RecyclerView recyclerView;
    private ProgressWheel progressWheel;
    private Activity activity;
    private int comments;
    private int position;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent returnIntent = new Intent();
                returnIntent.putExtra("position", position);
                returnIntent.putExtra("comment", comments);
                returnIntent.putExtra("reload", false);
                setResult(RESULT_OK, returnIntent);
                finish();
        }
        return (super.onOptionsItemSelected(item));

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Intent returnIntent = new Intent();
        returnIntent.putExtra("position", position);
        returnIntent.putExtra("comment", comments);
        returnIntent.putExtra("reload", false);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        this.activity = CommentsActivity.this;

        if(getIntent().getExtras() != null){
            commentId = getIntent().getStringExtra("id");
            position = getIntent().getIntExtra("position", 0);
        }


        mContext = CommentsActivity.this;
        Toolbar mToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(mToolbar);

        menu = getSupportActionBar();

        if (menu != null) {
            menu.setDisplayHomeAsUpEnabled(true);
            menu.setHomeAsUpIndicator(R.drawable.ic_action_cancel);
            menu.setTitle("");
        }

        progressWheel = (ProgressWheel)findViewById(R.id.progress_wheel);
        recyclerView = (RecyclerView)findViewById(R.id.comments_listview);
        editMessage = (EditText)findViewById(R.id.editMessage);
        ImageView imageSend = (ImageView) findViewById(R.id.imageSend);
        noComments = (TextView)findViewById(R.id.comments_no_comments);
        final Button retry = (Button)findViewById(R.id.retry);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(new ConnectionDetector(CommentsActivity.this).isConnectingToInternet()){
                    retry.setVisibility(View.GONE);
                    fetchComments(commentId);
                }
            }
        });
        imageSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                comment();
            }
        });

        if(new ConnectionDetector(this).isConnectingToInternet()){
            fetchComments(commentId);
        }else{
            retry.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    private void fetchComments(String commentId) {
        progressWheel.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        final ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("comments");
        parseQuery.whereEqualTo("objectIdRef", commentId);
        parseQuery.orderByAscending("createdAt");
        parseQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                commentArrayList = new ArrayList<Comment>();
                if(list.size() != 0){
                    for (int i = 0; i < list.size(); i++) {
                        ParseObject parseObject = list.get(i);
                        Comment comment = new Comment();
                        if(parseObject.getString("facebookId") != null){
                            comment.setFacebookId(parseObject.getString("facebookId"));
                            comment.setName(parseObject.getString("facebookName"));
                        }else{
                            comment.setName(parseObject.getString("parseName"));
                            if(parseObject.getParseFile("parseIcon") != null){
                                comment.setParseImage(parseObject.getParseFile("parseIcon"));
                            }
                        }
                        comment.setTime(parseObject.getCreatedAt());

                        comment.setComment(parseObject.getString("comment"));
                        comment.setIsNewComment(false);
                        commentArrayList.add(comment);
                    }

                    progressWheel.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    displayComments(commentArrayList);

                }else{
                    Log.d(TAG, "no comments");
                    recyclerView.setVisibility(View.GONE);
                    noComments.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    private void displayComments(ArrayList<Comment> list) {
        commentsAdapter = new CommentsAdapter(activity, list, getApplicationContext());
        recyclerView.setAdapter(commentsAdapter);

    }

    private void comment() {
        if (editMessage.getText().toString().equals("")) {
            Toast.makeText(mContext, "Please write something.", Toast.LENGTH_SHORT).show();
        } else {
            Comment comment = new Comment();
            comment.setComment(editMessage.getText().toString());
            if(ParseUser.getCurrentUser().has("profile")){
                comment.setName(ParseUser.getCurrentUser().getJSONObject("profile").optString("name"));
                comment.setFacebookId(ParseUser.getCurrentUser().getJSONObject("profile").optString("facebookId"));
            }else{
                comment.setName(ParseUser.getCurrentUser().getString("name"));
                if(ParseUser.getCurrentUser().has("image")){
                    comment.setParseImage(ParseUser.getCurrentUser().getParseFile("image"));
                }
            }

            comment.setIsNewComment(true);
            if(commentArrayList.size() == 0){
//                parseObjects.add(parseObject);
                commentArrayList.add(comment);
                displayComments(commentArrayList);
//                commentsAdapter.add(parseObject);
                noComments.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }else{
//                commentsAdapter.add(parseObject);
                commentArrayList.add(comment);
//                parseObjects.add(parseObject);
                commentsAdapter.notifyDataSetChanged();
            }
            commentsAdapter.setId(commentId);

            editMessage.setText("");
            InputMethodManager imm = (InputMethodManager) getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editMessage.getWindowToken(), 0);

            recyclerView.scrollToPosition(commentArrayList.size() - 1);

            ParseQuery<ParseObject> parseQuery = new ParseQuery<ParseObject>("News_Feed_Test");
            parseQuery.getInBackground(commentId, new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    //no need to add notify myself that I commented
                    if (!parseObject.getString("userId").equals(ParseUser.getCurrentUser().getObjectId())) {
                        parseObject.increment("newComments");
                    }
                    parseObject.increment("numberOfComments");
                    parseObject.saveInBackground();

                }
            });
        }
    }

    public void incrementComment() {
        comments++;
    }
}
