package com.githioch.the_looker.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.widget.ProfilePictureView;
import com.githioch.the_looker.CommentsActivity;
import com.githioch.the_looker.R;
import com.githioch.the_looker.RecyclerViewFragment;
import com.githioch.the_looker.model.FeedItem;
import com.githioch.the_looker.util.ConnectionDetector;
import com.githioch.the_looker.util.ExpandableTextView;
import com.githioch.the_looker.util.ProfilePictureViewNavigationDrawer;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseImageView;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.pkmmte.view.CircularImageView;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by Githioch on 6/2/2015.
 * Adapter for items that appear in the main feed
 * It has different layouts for parse and fb users and another two for the same types but with added
 * images.
 */
//TODO update feedList after successful comment
    //TODO colour the gotea gotea if this user goteas (first get proper colour)
public class FeedListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FACEBOOK_NO_IMAGE = 1;
    private static final int TYPE_FACEBOOK_WITH_IMAGE = 2;
    private static final int TYPE_PARSE_NO_IMAGE = 3;
    private static final int TYPE_PARSE_WITH_IMAGE = 4;
    private final RecyclerViewFragment mActivity;
    private boolean parseLogin = false;
    private boolean fbLogin = false;
    private final LayoutInflater layoutInflater;
    private final Context context;
    private RecyclerViewClickListener itemListener;
    private final ImageLoader imageLoader;

    private ArrayList<FeedItem> feedItemArrayList = new ArrayList<>();
    private final int[] ints = new int[4];
    private RecyclerView recyclerView;

    public FeedListAdapter(Context context, RecyclerViewClickListener recyclerViewClickListener,
                           RecyclerViewFragment mActivity){
        this.context = context;
        this.itemListener = recyclerViewClickListener;
        this.mActivity = mActivity;
        layoutInflater = LayoutInflater.from(this.context);

        ints[0] = R.color.red;
        ints[1] = R.color.green;
        ints[2] = R.color.blue;
        ints[3] = R.color.lime;

        imageLoader = ImageLoader.getInstance();

        if(ParseUser.getCurrentUser().getJSONObject("profile") != null &&
                ParseUser.getCurrentUser().getJSONObject("profile").optString("name") != null){
            Log.d("assessed","as true");
            this.fbLogin = true;
        }
        else{
            this.parseLogin = true;
        }
    }

    public void setRecyclerView(RecyclerView recyclerView){
        this.recyclerView = recyclerView;
    }

    private RecyclerView getRecyclerView(){
        return this.recyclerView;
    }


    private FeedItem getItem(int position) {
        return feedItemArrayList.get(position);
    }

    public void setFeedList(ArrayList<FeedItem> arrayList){
        this.feedItemArrayList = arrayList;
        notifyItemRangeChanged(0, feedItemArrayList.size());
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == TYPE_FACEBOOK_NO_IMAGE) {
            //inflate your layout and pass it to view holder
            View view = layoutInflater.inflate(R.layout.feed_item_facebook_no_image, parent, false);
            return new ViewHolderFacebookNoImage(view);

        }
        else if(viewType == TYPE_FACEBOOK_WITH_IMAGE){
            View view = layoutInflater.inflate(R.layout.feed_item_facebook_with_image, parent, false);
            return new ViewHolderFacebookWithImage(view);
        }
        else if(viewType == TYPE_PARSE_NO_IMAGE){
            View view = layoutInflater.inflate(R.layout.feed_item_parse_no_image, parent, false);
            return new ViewHolderParseNoImage(view);
        }
        else if(viewType == TYPE_PARSE_WITH_IMAGE){
            View view = layoutInflater.inflate(R.layout.feed_item_parse_with_image, parent, false);
            return new ViewHolderParseWithImage(view);
        }
        throw new RuntimeException("there is no type that matches the type "
                + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        FeedItem feedItem = getItem(position);

        if(holder instanceof ViewHolderParseWithImage){
            bindParseWithImage((ViewHolderParseWithImage)holder, feedItem, position);
        }

        else if(holder instanceof ViewHolderParseNoImage){
            bindParseNoImage((ViewHolderParseNoImage)holder, feedItem, position);
        }

        else if(holder instanceof ViewHolderFacebookWithImage){
            bindFacebookWithImage((ViewHolderFacebookWithImage) holder, feedItem, position);
        }

        else {
            //cast holder to VHItem and set data
            bindFacebookNoImage((ViewHolderFacebookNoImage) holder, feedItem, position);
        }

    }

    private void bindFacebookNoImage(ViewHolderFacebookNoImage holder, FeedItem feedItem, int position) {

        if(position == 0){
            holder.sideColor.setBackgroundResource(ints[0]);
        }else{
            if(position <= 3){
                holder.sideColor.setBackgroundResource(ints[position]);
            }else{
                holder.sideColor.setBackgroundResource(ints[position % 4]);
            }
        }

        if(feedItem.getTag() != null){
            holder.tag.setText("#" + feedItem.getTag());
            holder.tag.setTextColor(mActivity.getResources().getColor(getTagColor(feedItem)));
        }

        holder.commentsCounter.setText(String.valueOf(feedItem.getNumberOfComments()));

        if(fbLogin){
            if(feedItem.getFbUsersWhoGotead() != null && feedItem.getFbUsersWhoGotead()
                    .contains(feedItem.getId() + ParseUser.getCurrentUser().getObjectId())){
                holder.gotea.setImageResource(R.drawable.ic_gotea_coloured);
            }else{
                holder.gotea.setImageResource(R.drawable.ic_gotea_uncoloured);
            }
        }else{
            if(feedItem.getParseUsersWhoGotead() != null && feedItem.getParseUsersWhoGotead()
                    .contains(feedItem.getId() + ParseUser.getCurrentUser().getObjectId())){
                holder.gotea.setImageResource(R.drawable.ic_gotea_coloured);
            }else{
                holder.gotea.setImageResource(R.drawable.ic_gotea_uncoloured);
            }
        }

        holder.position = position;

        holder.goteaCounter.setText(String.valueOf(feedItem.getGoteas()));

        holder.objectId = feedItem.getId();

        if(feedItem.getFacebookId() != null && feedItem.getFacebookName() != null){
            holder.profilePic.setProfileId(feedItem.getFacebookId());
            holder.name.setText(feedItem.getFacebookName());
        }

        if(feedItem.getDate() != null){

            CharSequence timeAgo = getRelativeDate(feedItem.getDate());
            holder.time.setText(timeAgo);
        }
        else{
            holder.time.setText("");
        }

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        holder.status.setTypeface(typeface);
        holder.status.setText(feedItem.getStatus());
    }

    private void bindFacebookWithImage(final ViewHolderFacebookWithImage holder, FeedItem feedItem,
                                       int position) {

        holder.position = position;

        if(position == 0){
            holder.sideColor.setBackgroundResource(ints[0]);
        }else{
            if(position <= 3){
                holder.sideColor.setBackgroundResource(ints[position]);
            }else{
                holder.sideColor.setBackgroundResource(ints[position % 4]);
            }
        }

        if(feedItem.getTag() != null){
            holder.tag.setText("#" + feedItem.getTag());
            holder.tag.setTextColor(mActivity.getResources().getColor(getTagColor(feedItem)));
        }

        holder.commentsCounter.setText(String.valueOf(feedItem.getNumberOfComments()));

        if(fbLogin){
            if(feedItem.getFbUsersWhoGotead() != null && feedItem.getFbUsersWhoGotead()
                    .contains(feedItem.getId() + ParseUser.getCurrentUser().getObjectId())){
                holder.gotea.setImageResource(R.drawable.ic_gotea_coloured);
            }else{
                holder.gotea.setImageResource(R.drawable.ic_gotea_uncoloured);
            }
        }else{
            if(feedItem.getParseUsersWhoGotead() != null && feedItem.getParseUsersWhoGotead()
                    .contains(feedItem.getId() + ParseUser.getCurrentUser().getObjectId())){
                holder.gotea.setImageResource(R.drawable.ic_gotea_coloured);
            }else{
                holder.gotea.setImageResource(R.drawable.ic_gotea_uncoloured);
            }
        }

        holder.goteaCounter.setText(String.valueOf(feedItem.getGoteas()));

        holder.objectId = feedItem.getId();

        if(feedItem.getFacebookId() != null && feedItem.getFacebookName() != null){
            holder.profilePic.setProfileId(feedItem.getFacebookId());
            holder.name.setText(feedItem.getFacebookName());
        }
        else{
            holder.profilePic.setProfileId(null);
        }

        if(feedItem.getDate() != null){

            CharSequence timeAgo = getRelativeDate(feedItem.getDate());
            holder.time.setText(timeAgo);
        }
        else{
            holder.time.setText("");
        }

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        holder.status.setTypeface(typeface);
        holder.status.setText(feedItem.getStatus());

        if(feedItem.getImageUri() != null){
            // Feed image
//                ParseFile parseFile = feedItem.getImage()
            holder.image.setParseFile(feedItem.getImge());
            holder.image.loadInBackground(new GetDataCallback() {
                @Override
                public void done(final byte[] bytes, ParseException e) {
                    holder.spinner.setVisibility(View.INVISIBLE);
                    holder.image.setVisibility(View.VISIBLE);

                    holder.image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            mActivity.displayLargeImage(bitmap);
                        }
                    });
                }
            });

        }else{
            holder.image.setImageDrawable(null);
        }
    }

    private void bindParseNoImage(final ViewHolderParseNoImage holder, FeedItem feedItem, int position) {
        if(position == 0){
            holder.sideColor.setBackgroundResource(ints[0]);
        }else{
            if(position <= 3){
                holder.sideColor.setBackgroundResource(ints[position]);
            }else{
                holder.sideColor.setBackgroundResource(ints[position % 4]);
            }
        }

        if(feedItem.getTag() != null){
         holder.tag.setText("#" + feedItem.getTag());
            holder.tag.setTextColor(mActivity.getResources().getColor(getTagColor(feedItem)));
        }

        holder.objectId = feedItem.getId();

        holder.position = position;

        holder.commentsCounter.setText(String.valueOf(feedItem.getNumberOfComments()));

        holder.goteaCounter.setText(String.valueOf(feedItem.getGoteas()));

        if(fbLogin){
            if(feedItem.getFbUsersWhoGotead() != null && feedItem.getFbUsersWhoGotead()
                    .contains(feedItem.getId() + ParseUser.getCurrentUser().getObjectId())){
                holder.gotea.setImageResource(R.drawable.ic_gotea_coloured);
            }else{
                holder.gotea.setImageResource(R.drawable.ic_gotea_uncoloured);
            }
        }else{
            if(feedItem.getParseUsersWhoGotead() != null && feedItem.getParseUsersWhoGotead()
                    .contains(feedItem.getId() + ParseUser.getCurrentUser().getObjectId())){
                holder.gotea.setImageResource(R.drawable.ic_gotea_coloured);
            }else{
                holder.gotea.setImageResource(R.drawable.ic_gotea_uncoloured);
            }
        }

        if(feedItem.getName() != null){
            holder.name.setText(feedItem.getName());
        }

        if(!fbLogin && feedItem.getParseUserWhoPostedId() != null &&
                feedItem.getParseUserWhoPostedId().equals(ParseUser.getCurrentUser()
                        .getObjectId()) && ParseUser.getCurrentUser().getParseFile("parseIcon") != null){
            holder.parseImage.setParseFile(ParseUser.getCurrentUser().getParseFile("parseIcon"));
            holder.parseImage.loadInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    holder.parseImageView.setImageBitmap(bitmap);
                }
            });
        }
        else if(feedItem.getIconUri() != null){
            holder.parseImage.setParseFile(feedItem.getIcon());
            holder.parseImage.loadInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    holder.parseImageView.setImageBitmap(bitmap);
                }
            });
        }
        else{
            holder.parseImageView.setImageResource(R.drawable.no_profile);
        }

        if(feedItem.getDate() != null){

            CharSequence timeAgo = getRelativeDate(feedItem.getDate());
            holder.time.setText(timeAgo);
        }
        else{
            holder.time.setText("");
        }

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        holder.status.setTypeface(typeface);
        holder.status.setText(feedItem.getStatus());
    }

    private int getTagColor(FeedItem feedItem) {
        String tag = feedItem.getTag();
        switch (tag){
            case "City Council":
                return R.color.red;

            case "Crime":
                return R.color.orange;

            case "Accident":
                return R.color.material_deep_teal_200;

            case "Medical Alert":
                return R.color.green;

            case "Fire":
                return R.color.blue;

            case "Other":
                return R.color.material_deep_teal_200;
        }
        return R.color.black;
    }

    private void bindParseWithImage(final ViewHolderParseWithImage holder, FeedItem feedItem, int position) {

        holder.objectId = feedItem.getId();

        holder.position = position;

        if(position == 0){
            holder.sideColor.setBackgroundResource(ints[0]);
        }else{
            if(position <= 3){
                holder.sideColor.setBackgroundResource(ints[position]);
            }else{
                holder.sideColor.setBackgroundResource(ints[position % 4]);
            }
        }

        if(feedItem.getTag() != null){
            holder.tag.setText("#" + feedItem.getTag());
            holder.tag.setTextColor(mActivity.getResources().getColor(getTagColor(feedItem)));
        }

        holder.commentsCounter.setText(String.valueOf(feedItem.getNumberOfComments()));

        if(fbLogin){
            if(feedItem.getFbUsersWhoGotead() != null && feedItem.getFbUsersWhoGotead()
                    .contains(feedItem.getId() + ParseUser.getCurrentUser().getObjectId())){
                holder.gotea.setImageResource(R.drawable.ic_gotea_coloured);
            }else{
                holder.gotea.setImageResource(R.drawable.ic_gotea_uncoloured);
            }
        }else{
            if(feedItem.getParseUsersWhoGotead() != null && feedItem.getParseUsersWhoGotead()
                    .contains(feedItem.getId() + ParseUser.getCurrentUser().getObjectId())){
                holder.gotea.setImageResource(R.drawable.ic_gotea_coloured);
            }else{
                holder.gotea.setImageResource(R.drawable.ic_gotea_uncoloured);
            }
        }

        holder.goteaCounter.setText(String.valueOf(feedItem.getGoteas()));

        if(feedItem.getName() != null){
            holder.name.setText(feedItem.getName());
        }

        if(!fbLogin && feedItem.getParseUserWhoPostedId() != null &&
                feedItem.getParseUserWhoPostedId().equals(ParseUser.getCurrentUser()
                        .getObjectId()) && ParseUser.getCurrentUser().getParseFile("parseIcon") != null){
            imageLoader.loadImage(ParseUser.getCurrentUser().getParseFile("parseIcon").getUrl(),
                    new ImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String s, View view) {
                            holder.parseImageView
                                    .setImageDrawable(mActivity.getResources().
                                            getDrawable(R.drawable.no_profile));
                        }

                        @Override
                        public void onLoadingFailed(String s, View view, FailReason failReason) {

                        }

                        @Override
                        public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                            holder.parseImageView.setImageBitmap(bitmap);
                        }

                        @Override
                        public void onLoadingCancelled(String s, View view) {

                        }
                    });
        }else if(feedItem.getIconUri() != null){
            imageLoader.loadImage(feedItem.getIconUri(), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {
                    holder.parseImageView
                            .setImageDrawable(mActivity.getResources().
                                    getDrawable(R.drawable.no_profile));
                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    holder.parseImageView.setImageBitmap(bitmap);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });
        }
        else{
            holder.parseImageView.setImageResource(R.drawable.no_profile);
        }

        if(feedItem.getDate() != null){

            CharSequence timeAgo = getRelativeDate(feedItem.getDate());
            holder.time.setText(timeAgo);
        }
        else{
            holder.time.setText("");
        }

        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        holder.status.setTypeface(typeface);
        holder.status.setText(feedItem.getStatus());

        if(feedItem.getImageUri() != null){
            holder.image.setParseFile(feedItem.getImge());
            holder.image.loadInBackground(new GetDataCallback() {
                @Override
                public void done(final byte[] bytes, ParseException e) {

                    holder.spinner.setVisibility(View.INVISIBLE);
                    holder.image.setVisibility(View.VISIBLE);

                    holder.image.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            mActivity.displayLargeImage(bitmap);
                        }
                    });
                }
            });

        }else{
            holder.image.setImageDrawable(null);
        }

    }

    @Override
    public int getItemViewType(int position) {

        if(isFaceBookAndHasImage(position)){
            return TYPE_FACEBOOK_WITH_IMAGE;
        }
        else if(isFaceBookAndNoImage(position)){
            return TYPE_FACEBOOK_NO_IMAGE;
        }
        else if(isParseAndNoImage(position)){
            return TYPE_PARSE_NO_IMAGE;
        }
        else{
            return TYPE_PARSE_WITH_IMAGE;
        }

    }

    private CharSequence getRelativeDate(Date date) {

        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
        formatter.setTimeZone(TimeZone.getTimeZone("Africa/Nairobi"));

        try {
            date = formatter.parse(formatter.format(date));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                date.getTime(),
                System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);

        return timeAgo;
    }

    private boolean isParseAndNoImage(int position) {
        if(feedItemArrayList.get(position).getImageUri() == null
                && feedItemArrayList.get(position).getFacebookId() == null){
            return true;
        }
        return false;

    }

    private boolean isFaceBookAndNoImage(int position) {
        return feedItemArrayList.get(position).getImageUri() == null
                && feedItemArrayList.get(position).getFacebookId() != null;

    }

    private boolean isFaceBookAndHasImage(int position){
        return feedItemArrayList.get(position).getImageUri() != null
                && feedItemArrayList.get(position).getFacebookId() != null;
    }

    @Override
    public int getItemCount() {
        return feedItemArrayList.size();
    }

    private void showReportDialog(final String objectId) {
        AlertDialog alertDialog;

        final CharSequence[] items = {"Inappropriate","Not relevant to your region"
                ,"Business Advertisement"};
        // arrayList to keep the selected items
        final ArrayList<Integer> selectedItems = new ArrayList();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Select Reason");
        builder.setMultiChoiceItems(items, null,
                new DialogInterface.OnMultiChoiceClickListener() {
                    // indexSelected contains the index of item (of which checkbox checked)
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected,
                                        boolean isChecked) {
                        if (isChecked) {
                            // If the user checked the item, add it to the selected items
                            // write your code when user checked the checkbox
                            selectedItems.add(indexSelected);
                        } else if (selectedItems.contains(indexSelected)) {
                            // Else, if the item is already in the array, remove it
                            // write your code when user Unchecked the checkbox
                            selectedItems.remove(Integer.valueOf(indexSelected));
                        }
                    }
                })
                // Set the action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on OK
                        //  You can write the code  to save the selected item here
                        ParseObject parseObject = new ParseObject("reports");
                        parseObject.put("newsFeedId", objectId);
                        for (int i = 0; i < selectedItems.size(); i++) {
                            parseObject.put(String.valueOf(items[i]).replaceAll(" ","_"),true);
                        }
                        if(new ConnectionDetector(mActivity.getActivity()).isConnectingToInternet()){
                            parseObject.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    Log.d("report"," saved");
                                    Toast.makeText(context, "Report sent successfully", Toast.LENGTH_SHORT)
                                            .show();
                                }
                            });
                        }else{
                            Toast.makeText(mActivity.getActivity(), "Please connect to the Internet and try again.",
                                    Toast.LENGTH_SHORT).show();
                        }


                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        //  Your code when user clicked on Cancel
                        dialog.dismiss();

                    }
                });

        alertDialog = builder.create();//AlertDialog dialog; create like this outside onClick
        alertDialog.show();
    }

    private void gotea(final int position, String objectId,
                       TextView goteaCounter, ImageView gotea) {

        final int currentGoteas;
        if(fbLogin){
            Log.d("fb", "passed");
            if(getItem(position).getFbUsersWhoGotead() == null){

                List<String> list = new ArrayList<>();

                list.add(objectId + ParseUser.getCurrentUser().
                        getObjectId());
                getItem(position).setFbUsersWhoGotead(list);
                getItem(position).setGoteas(getItem(position).getGoteas() + 1);

                currentGoteas = Integer.valueOf(goteaCounter.getText().toString()) + 1;
                goteaCounter.setText(String.valueOf(currentGoteas));
                goteaCounter.setTextColor(mActivity.getResources().getColor(R.color.user_gotead_color));
                gotea.setImageResource(R.drawable.ic_gotea_coloured);

                ParseQuery<ParseObject>parseQuery = new ParseQuery<>("News_Feed_Test");
                parseQuery.getInBackground(objectId, new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        parseObject.put("fbUsersWhoGotead", getItem(position).getFbUsersWhoGotead());
                        //no need to notify myself that I gotead on my own post
                        if(!parseObject.getString("userId").equals(ParseUser.getCurrentUser().getObjectId())){
                            parseObject.put("newGoteas", currentGoteas);
                        }
                        parseObject.put("goteas", currentGoteas);
                        parseObject.saveInBackground();
                    }
                });
            }
            else if(getItem(position).getFbUsersWhoGotead().
                    contains(objectId+ParseUser.getCurrentUser().getObjectId())){

                getItem(position).getFbUsersWhoGotead().remove(objectId +
                        ParseUser.getCurrentUser().getObjectId());

                currentGoteas  = Integer.valueOf(goteaCounter.getText().toString()) - 1;
                ParseQuery<ParseObject>parseQuery = new ParseQuery<>("News_Feed_Test");
                parseQuery.getInBackground(objectId, new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        parseObject.put("fbUsersWhoGotead", getItem(position).getFbUsersWhoGotead());
                        if(!parseObject.getString("userId").equals(ParseUser.getCurrentUser().getObjectId())){
                            parseObject.put("newGoteas", currentGoteas);
                        }
                        parseObject.put("goteas", currentGoteas);
                        parseObject.saveInBackground();
                    }
                });

                getItem(position).setGoteas(currentGoteas);
                goteaCounter.setText(String.valueOf(currentGoteas));
                goteaCounter.setTextColor(mActivity.getResources().getColor(R.color.primary_dark_material_light));
                gotea.setImageResource(R.drawable.ic_gotea_uncoloured);
            }
            else{
                for (int i = 0; i < getItem(position).getFbUsersWhoGotead().size(); i++) {
                    Log.d(String.valueOf(i), getItem(position).getFbUsersWhoGotead().get(i));
                }
                currentGoteas  = Integer.valueOf(goteaCounter.getText().toString()) + 1;
                goteaCounter.setText(String.valueOf(currentGoteas));
                goteaCounter.setTextColor(mActivity.getResources().getColor(R.color.user_gotead_color));
                gotea.setImageResource(R.drawable.ic_gotea_coloured);

                getItem(position).setGoteas(currentGoteas);

                getItem(position).getFbUsersWhoGotead().add(objectId + ParseUser.
                        getCurrentUser().getObjectId());

                ParseQuery<ParseObject>parseQuery = new ParseQuery<>("News_Feed_Test");
                parseQuery.getInBackground(objectId, new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        parseObject.put("fbUsersWhoGotead", getItem(position).getFbUsersWhoGotead());
                        if(!parseObject.getString("userId").equals(ParseUser.getCurrentUser().getObjectId())){
                            parseObject.put("newGoteas", currentGoteas);
                        }
                        parseObject.put("goteas", currentGoteas);
                        parseObject.saveInBackground();
                    }
                });
            }
        }
        else{
            Log.d("passed", "parse");
            if(getItem(position).getParseUsersWhoGotead() == null){ //no one has liked this yet
                List<String> list = new ArrayList<>();

                list.add(objectId + ParseUser.getCurrentUser().getObjectId());
                getItem(position).setParseUsersWhoGotead(list);

                currentGoteas  = Integer.valueOf(goteaCounter.getText().toString()) + 1;
                goteaCounter.setTextColor(mActivity.getResources().getColor(R.color.user_gotead_color));
                goteaCounter.setText(String.valueOf(currentGoteas));
                gotea.setImageResource(R.drawable.ic_gotea_coloured);

                ParseQuery<ParseObject>parseQuery = new ParseQuery<>("News_Feed_Test");
                parseQuery.getInBackground(objectId, new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        parseObject.put("parseUsersWhoGotead", getItem(position).getParseUsersWhoGotead());
                        if(!parseObject.getString("userId").equals(ParseUser.getCurrentUser().getObjectId())){
                            parseObject.put("newGoteas", currentGoteas);
                        }
                        parseObject.put("goteas", currentGoteas);
                        parseObject.saveInBackground();
                    }
                });

                getItem(position).setGoteas(currentGoteas);
            }
            else if(getItem(position).getParseUsersWhoGotead().
                    contains(objectId+ParseUser.getCurrentUser().getObjectId())){

                getItem(position).getParseUsersWhoGotead().remove(objectId +
                        ParseUser.getCurrentUser().getObjectId());

                currentGoteas  = Integer.valueOf(goteaCounter.getText().toString()) - 1;
                ParseQuery<ParseObject>parseQuery = new ParseQuery<>("News_Feed_Test");
                parseQuery.getInBackground(objectId, new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        parseObject.put("parseUsersWhoGotead", getItem(position).getParseUsersWhoGotead());
                        if(!parseObject.getString("userId").equals(ParseUser.getCurrentUser().getObjectId())){
                            parseObject.put("newGoteas", currentGoteas);
                        }
                        parseObject.put("goteas", currentGoteas);
                        parseObject.saveInBackground();
                    }
                });

                goteaCounter.setText(String.valueOf(currentGoteas));
                goteaCounter.setTextColor(mActivity.getResources().getColor(R.color.primary_dark_material_light));
                gotea.setImageResource(R.drawable.ic_gotea_uncoloured);
                getItem(position).setGoteas(currentGoteas);
            }
            else {
                currentGoteas  = Integer.valueOf(goteaCounter.getText().toString()) + 1;
                goteaCounter.setText(String.valueOf(currentGoteas));
                goteaCounter.setTextColor(mActivity.getResources().getColor(R.color.user_gotead_color));
                getItem(position).setGoteas(currentGoteas);

                getItem(position).getParseUsersWhoGotead().add(objectId +
                        ParseUser.getCurrentUser().getObjectId());

                ParseQuery<ParseObject>parseQuery = new ParseQuery<>("News_Feed_Test");
                parseQuery.getInBackground(objectId, new GetCallback<ParseObject>() {
                    @Override
                    public void done(ParseObject parseObject, ParseException e) {
                        parseObject.put("parseUsersWhoGotead", getItem(position).getParseUsersWhoGotead());
                        if(!parseObject.getString("userId").equals(ParseUser.getCurrentUser().getObjectId())){
                            parseObject.put("newGoteas", currentGoteas);
                        }
                        parseObject.put("goteas", currentGoteas);
                        parseObject.saveInBackground();
                    }
                });

                gotea.setImageResource(R.drawable.ic_gotea_coloured);

            }
        }
    }

    public interface RecyclerViewClickListener {

        void recyclerViewListClicked(View v, String objectId);

        void itemSelectedForDelete(String objectId, int position);

    }

    public class ViewHolderParseWithImage extends RecyclerView.ViewHolder{

        private final ImageView gotea;
        private final TextView goteaCounter;
        private final LinearLayout replyLinear;
        private final LinearLayout shareLinear;
        private final LinearLayout goteaLinear;
        private final ProgressBar spinner;
        private final TextView tag;
        private ProfilePictureView profilePic;
        private final TextView name;
        private final TextView time;
        private final ExpandableTextView status;
        private final ParseImageView image;
        private String objectId;
        private final CircularImageView parseImageView;
        private int position;
        private int currentGoteas;
        private TextView commentsCounter;
        private View sideColor;
        private ImageView menu;
        private ImageView reply;
        private ImageView share;




        public ViewHolderParseWithImage(View itemView) {
            super(itemView);


            spinner = (ProgressBar)itemView.findViewById(R.id.progressBar1);
            menu = (ImageView)itemView.findViewById(R.id.feed_menu);
            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.timestamp);
            status = (ExpandableTextView) itemView.findViewById(R.id.txtStatusMsg);
            image = (ParseImageView) itemView.findViewById(R.id.feedImage1);
            parseImageView = (CircularImageView) itemView.findViewById(R.id.parse_image_adapter);
            gotea = (ImageView)itemView.findViewById(R.id.gotea);
            goteaCounter = (TextView)itemView.findViewById(R.id.gotea_counter);
            commentsCounter = (TextView)itemView.findViewById(R.id.commentsCounter);
            sideColor = itemView.findViewById(R.id.post_side_color);
            replyLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_reply);
            shareLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_share);
            goteaLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_gotea);
            tag = (TextView)itemView.findViewById(R.id.feed_tag);

            shareLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = status.getText().toString() + "\nhttp://tatucreatives.com/";
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "The Looker");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    mActivity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });

            replyLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CommentsActivity.class);
                    intent.putExtra("id", objectId);
                    intent.putExtra("position", position);
                    mActivity.startActivityForResult(intent, 10);
                }
            });

            menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("user id", getItem(position).getUserId());
                    if(getItem(position).getUserId().equals(ParseUser.getCurrentUser().getObjectId())){
                        final PopupMenu popupMenu = new PopupMenu(context, menu);
                        MenuInflater menuInflater = popupMenu.getMenuInflater();
                        menuInflater.inflate(R.menu.feed_menu_pop_up_is_users_post, popupMenu.getMenu());
                        popupMenu.show();

                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                itemListener.itemSelectedForDelete(objectId, position);
                                return true;
                            }
                        });
                    }else{
                        PopupMenu popupMenu = new PopupMenu(context, menu);
                        MenuInflater menuInflater = popupMenu.getMenuInflater();
                        menuInflater.inflate(R.menu.feed_menu_pop_up_not_users_post, popupMenu.getMenu());
                        popupMenu.show();

                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                showReportDialog(objectId);
                                return true;
                            }
                        });
                    }
                }
            });

            gotea.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotea(position, objectId, goteaCounter, gotea);
                }
            });
        }

    }


    public class ViewHolderFacebookWithImage extends RecyclerView.ViewHolder{

        private final ImageView gotea;
        private final TextView goteaCounter;
        private final TextView commentsCounter;
        private final com.githioch.the_looker.util.ProfilePictureViewNavigationDrawer profilePic;
        private final View sideColor;
        private final LinearLayout replyLinear;
        private final LinearLayout shareLinear;
        private final LinearLayout goteaLinear;
        private final TextView tag;
        private TextView name;
        private TextView time;
        private ExpandableTextView status;
        private ParseImageView image;
        private String objectId;
        private ImageView parseImageView;
        private int position;
        private int currentGoteas;
        private ImageView menu;
        private ImageView reply;
        private ImageView share;
        private ProgressBar spinner;

        public ViewHolderFacebookWithImage(View itemView) {
            super(itemView);

            tag = (TextView)itemView.findViewById(R.id.feed_tag);
            spinner = (ProgressBar)itemView.findViewById(R.id.progressBar1);
            menu = (ImageView)itemView.findViewById(R.id.feed_menu);
            profilePic = (ProfilePictureViewNavigationDrawer) itemView.findViewById(R.id.profilePic);
            profilePic.setStrokeWidth(10f);
            profilePic.setStrokeColor(mActivity.getResources().getColor(R.color.blue));
            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.timestamp);
            status = (ExpandableTextView) itemView.findViewById(R.id.txtStatusMsg);
            image = (ParseImageView) itemView.findViewById(R.id.feedImage1);
            parseImageView = (ImageView) itemView.findViewById(R.id.parse_image_adapter);
            gotea = (ImageView)itemView.findViewById(R.id.gotea);
            goteaCounter = (TextView)itemView.findViewById(R.id.gotea_counter);
            commentsCounter = (TextView)itemView.findViewById(R.id.commentsCounter);
            sideColor = itemView.findViewById(R.id.post_side_color);
            replyLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_reply);
            shareLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_share);
            goteaLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_gotea);

            spinner.setIndeterminate(true);

            shareLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = status.getText().toString() + "\nhttp://tatucreatives.com/";
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "The Looker");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    mActivity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });

            replyLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CommentsActivity.class);
                    intent.putExtra("id", objectId);
                    intent.putExtra("position", position);
                    mActivity.startActivityForResult(intent, 10);
                }
            });

            menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("user id", getItem(position).getUserId());
                    if(getItem(position).getUserId().equals(ParseUser.getCurrentUser().getObjectId())){
                        PopupMenu popupMenu = new PopupMenu(context, menu);
                        MenuInflater menuInflater = popupMenu.getMenuInflater();
                        menuInflater.inflate(R.menu.feed_menu_pop_up_is_users_post, popupMenu.getMenu());
                        popupMenu.show();

                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                itemListener.itemSelectedForDelete(objectId, position);
                                return true;
                            }
                        });
                    }else{
                        PopupMenu popupMenu = new PopupMenu(context, menu);
                        MenuInflater menuInflater = popupMenu.getMenuInflater();
                        menuInflater.inflate(R.menu.feed_menu_pop_up_not_users_post, popupMenu.getMenu());
                        popupMenu.show();

                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                showReportDialog(objectId);
                                return true;
                            }
                        });
                    }
                }
            });

            gotea.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotea(position, objectId, goteaCounter, gotea);
                }
            });
        }

    }

    public class ViewHolderParseNoImage extends RecyclerView.ViewHolder{

        private final ImageView gotea;
        private final TextView goteaCounter;
        private final TextView commentsCounter;
        private final View sideColor;
        private TextView name;
        private TextView time;
        private ExpandableTextView status;
        private ImageView image;
        private String objectId;
        private CircularImageView parseImageView;
        private int position;
        private ImageView menu;
        private ImageView share;
        private LinearLayout replyLinear;
        private LinearLayout shareLinear;
        private LinearLayout goteaLinear;
        private TextView tag;
        private ParseImageView parseImage;

        public ViewHolderParseNoImage(View itemView) {
            super(itemView);

            parseImage = new ParseImageView(context);
            menu = (ImageView)itemView.findViewById(R.id.feed_menu);
            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.timestamp);
            status = (ExpandableTextView) itemView.findViewById(R.id.txtStatusMsg);
            parseImageView = (CircularImageView) itemView.findViewById(R.id.parse_image_adapter);
            gotea = (ImageView)itemView.findViewById(R.id.gotea);
            goteaCounter = (TextView)itemView.findViewById(R.id.gotea_counter);
            commentsCounter = (TextView)itemView.findViewById(R.id.commentsCounter);
            sideColor = itemView.findViewById(R.id.post_side_color);
            share = (ImageView)itemView.findViewById(R.id.feed_share);
            replyLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_reply);
            shareLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_share);
            goteaLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_gotea);
            tag = (TextView)itemView.findViewById(R.id.feed_tag);

            replyLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CommentsActivity.class);
                    intent.putExtra("id", objectId);
                    intent.putExtra("position", position);
                    mActivity.startActivityForResult(intent, 10);
                }
            });

            shareLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = status.getText().toString() + "\nhttp://tatucreatives.com/";
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "The Looker");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    mActivity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });

            menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("user id", getItem(position).getUserId());
                    if(getItem(position).getUserId().equals(ParseUser.getCurrentUser().getObjectId())){
                        PopupMenu popupMenu = new PopupMenu(context, menu);
                        MenuInflater menuInflater = popupMenu.getMenuInflater();
                        menuInflater.inflate(R.menu.feed_menu_pop_up_is_users_post, popupMenu.getMenu());
                        popupMenu.show();

                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                itemListener.itemSelectedForDelete(objectId, position);
                                return true;
                            }
                        });
                    }else{
                        PopupMenu popupMenu = new PopupMenu(context, menu);
                        MenuInflater menuInflater = popupMenu.getMenuInflater();
                        menuInflater.inflate(R.menu.feed_menu_pop_up_not_users_post, popupMenu.getMenu());
                        popupMenu.show();

                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                showReportDialog(objectId);
                                return true;
                            }
                        });
                    }
                }
            });

            gotea.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotea(position, objectId, goteaCounter, gotea);

                }
            });
        }

    }

    public class ViewHolderFacebookNoImage extends RecyclerView.ViewHolder{

        private final TextView goteaCounter;
        private final TextView commentsCounter;
        private final View sideColor;
        private final TextView tag;
        private ProfilePictureViewNavigationDrawer profilePic;
        private TextView name;
        private TextView time;
        private ExpandableTextView status;
        private ImageView image;
        private String objectId;
        private ImageView parseImageView;
        private ImageView gotea;
        private int position;
        private int currentGoteas;
        private ImageView menu;
        private ImageView reply;
        private ImageView share;
        private LinearLayout replyLinear;
        private LinearLayout shareLinear;
        private LinearLayout goteaLinear;


        public ViewHolderFacebookNoImage(View itemView) {
            super(itemView);


            tag = (TextView)itemView.findViewById(R.id.feed_tag);
            menu = (ImageView)itemView.findViewById(R.id.feed_menu);
            profilePic = (ProfilePictureViewNavigationDrawer) itemView.findViewById(R.id.profilePic);
            profilePic.setStrokeWidth(10f);
            profilePic.setStrokeColor(mActivity.getResources().getColor(R.color.blue));
            name = (TextView) itemView.findViewById(R.id.name);
            time = (TextView) itemView.findViewById(R.id.timestamp);
            status = (ExpandableTextView) itemView.findViewById(R.id.txtStatusMsg);
            parseImageView = (ImageView) itemView.findViewById(R.id.parse_image_adapter);
            gotea = (ImageView)itemView.findViewById(R.id.gotea);
            goteaCounter = (TextView)itemView.findViewById(R.id.gotea_counter);
            commentsCounter = (TextView)itemView.findViewById(R.id.commentsCounter);
            sideColor = itemView.findViewById(R.id.post_side_color);
            reply = (ImageView)itemView.findViewById(R.id.feed_reply);
            share = (ImageView)itemView.findViewById(R.id.feed_share);
            replyLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_reply);
            shareLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_share);
            goteaLinear = (LinearLayout)itemView.findViewById(R.id.feed_linear_gotea);


            shareLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                    sharingIntent.setType("text/plain");
                    String shareBody = status.getText().toString() + "\nhttp://tatucreatives.com/";
                    sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "The Looker");
                    sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                    mActivity.startActivity(Intent.createChooser(sharingIntent, "Share via"));
                }
            });

            replyLinear.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, CommentsActivity.class);
                    intent.putExtra("id", objectId);
                    intent.putExtra("position", position);
                    mActivity.startActivityForResult(intent, 10);
                }
            });

            menu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d("user id", getItem(position).getUserId());
                    if(getItem(position).getUserId().equals(ParseUser.getCurrentUser().getObjectId())){
                        PopupMenu popupMenu = new PopupMenu(context, menu) ;
                        MenuInflater menuInflater = popupMenu.getMenuInflater();
                        menuInflater.inflate(R.menu.feed_menu_pop_up_is_users_post, popupMenu.getMenu());
                        popupMenu.show();

                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                itemListener.itemSelectedForDelete(objectId, position);
                                return true;
                            }
                        });
                    }else{
                        PopupMenu popupMenu = new PopupMenu(context, menu);
                        MenuInflater menuInflater = popupMenu.getMenuInflater();
                        menuInflater.inflate(R.menu.feed_menu_pop_up_not_users_post, popupMenu.getMenu());
                        popupMenu.show();

                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem menuItem) {
                                showReportDialog(objectId);
                                return true;
                            }
                        });
                    }
                }
            });

            gotea.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    gotea(position, objectId, goteaCounter, gotea);
                }
            });
        }
    }


}


