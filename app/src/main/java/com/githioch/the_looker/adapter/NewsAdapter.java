package com.githioch.the_looker.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.githioch.the_looker.R;
import com.githioch.the_looker.model.NewsItem;
import com.githioch.the_looker.util.ExpandableTextView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseImageView;

import java.util.ArrayList;

/**
 * Created by Githioch on 6/2/2015.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private final Activity activity;
    private boolean parseLogin = false;
    private boolean fbLogin = false;
    private LayoutInflater layoutInflater;
    private Context context;

    private ImageLoader imageLoader;

    private ArrayList<NewsItem> newsItemArrayList = new ArrayList<>();

    public NewsAdapter(Activity activity){
        layoutInflater = LayoutInflater.from(activity);
        this.activity = activity;
        this.context = activity;
        imageLoader = ImageLoader.getInstance();
    }

    public NewsItem getItem(int position) {
        return newsItemArrayList.get(position);
    }

    public void setNewsList(ArrayList<NewsItem> arrayList){
        this.newsItemArrayList = arrayList;
        notifyItemRangeChanged(0, newsItemArrayList.size());
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //inflate your layout and pass it to view holder
        View view = layoutInflater.inflate(R.layout.layout_news, parent, false);
        return new NewsViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final NewsViewHolder holder, int position) {

        NewsItem newsItem = newsItemArrayList.get(position);

        Typeface typeface = Typeface.createFromAsset(activity.getAssets(), "Roboto-Regular.ttf");

        holder.name.setTypeface(typeface);
        holder.name.setText(newsItem.getNewsTitle());

        holder.status.setTypeface(typeface);
        holder.status.setText(newsItem.getNewsStatus());
        // Checking for null feed url
        if (newsItem.getUrl() != null) {
            holder.url.setTypeface(typeface);
            holder.url.setText(Html.fromHtml("<a href=\"" + newsItem.getUrl() + "\">"
                    + newsItem.getUrl() + "</a> "));

            // Making url clickable
            holder.url.setMovementMethod(LinkMovementMethod.getInstance());
            holder.url.setVisibility(View.VISIBLE);
        } else {
            // url is null, remove from the view
            holder.url.setVisibility(View.GONE);
        }

        if(newsItem.getImage() != null){
            holder.parseImageView.setParseFile(newsItem.getImage());
            holder.parseImageView.loadInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    if(bytes == null){
                        Log.d("bytes", "null");
                    }else{
                        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        holder.image.setImageBitmap(bitmap);
                    }

                }
            });
        }else{
            holder.image.setImageDrawable(null);
        }


    }


    @Override
    public int getItemCount() {
        return newsItemArrayList.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder{

        private TextView name;
        private ExpandableTextView status;
        private ImageView image;
        private TextView url;
        private String objectId;
        private ParseImageView parseImageView;


        public NewsViewHolder(View itemView) {
            super(itemView);

            parseImageView = new ParseImageView(context);
            name = (TextView) itemView.findViewById(R.id.name);
            status = (ExpandableTextView)itemView.findViewById(R.id.txtStatusMsg);
            image = (ImageView) itemView.findViewById(R.id.feedImage1);
            url = (TextView)itemView.findViewById(R.id.txtUrl);

        }


    }
}


