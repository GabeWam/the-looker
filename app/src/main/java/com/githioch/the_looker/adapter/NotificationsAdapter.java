package com.githioch.the_looker.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.githioch.the_looker.R;
import com.githioch.the_looker.ViewNotificationsActivity;
import com.githioch.the_looker.model.Notification;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Githioch on 7/13/2015.
 */
public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.NotificationsViewHolder> {

    private final Context mContext;
    private final ArrayList<Notification> notifications;
    private final ImageLoader imageLoader;
    private final ViewNotificationsActivity mActivity;
    private NotificationsClickListener clickListener;

    public interface NotificationsClickListener{
        void NotificationClicked(String id, boolean isComment, int position);

    }

    public NotificationsAdapter(ViewNotificationsActivity mActivity, ArrayList<Notification> notifications, Context applicationContext,
                                NotificationsClickListener clickListner) {

        this.mActivity = mActivity;
      this.notifications = notifications;
        this.mContext = applicationContext;
        imageLoader = ImageLoader.getInstance();
        this.clickListener = clickListner;
    }


    @Override
    public NotificationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //inflate your layout and pass it to view holder
        View view = LayoutInflater.from(mContext).inflate(R.layout.notifications_item_layout,
                parent, false);
        return new NotificationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final NotificationsViewHolder holder, int position) {
        final Notification notification = notifications.get(position);

        holder.post.setText(notification.getPost());

        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
        formatter.setTimeZone(TimeZone.getTimeZone("Africa/Nairobi"));
        Date d = notification.getDateModified();

        try {
            d = formatter.parse(formatter.format(d));
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }

        CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                d.getTime(),
                System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
        holder.time.setText(timeAgo);

        holder.id = notification.getPostId();

        holder.position = position;

        if(notification.isClicked()){
            holder.relativeLayoutRoot.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }

        if(notification.isComment()){
            if(notification.getNumberOfComments() == 1){
                holder.notification.setText("You have "+ notification.getNumberOfComments()+
                        " new comment");
            }else{
                holder.notification.setText("You have "+ notification.getNumberOfComments()+
                        " new comments");
            }

            holder.isComment = true;
        }else{
            holder.isComment = false;
            if(notification.getNumberOfGoteas() == 1){
                holder.notification.setText(notification.getNumberOfGoteas()+ " new person" +
                        " has gotead you");
            } else {
                holder.notification.setText(notification.getNumberOfGoteas()+ " new people" +
                        " have gotead you");
            }

        }

        if(notification.getImageUrl() != null){

            String url = notification.getImageUrl();
//            Log.d("id", notification.getPostId());
            Log.d("url", url);

            imageLoader.loadImage(url, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {
                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String s, View view, final Bitmap bitmap) {
                    holder.imageView.setImageBitmap(bitmap);
                    holder.imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mActivity.displayLargeImage(bitmap);
                        }
                    });

                }

                @Override
                public void onLoadingCancelled(String s, View view) {

                }
            });

        }

    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    class NotificationsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView time;
        TextView notification;
        ImageView imageView;
        TextView post;
        String id;
        boolean isComment;
        int position;
        RelativeLayout relativeLayoutRoot;

        public NotificationsViewHolder(View itemView) {
            super(itemView);

            time = (TextView)itemView.findViewById(R.id.notification_time);
            notification = (TextView)itemView.findViewById(R.id.notification_notification);
            imageView = (ImageView)itemView.findViewById(R.id.notification_image);
            post = (TextView)itemView.findViewById(R.id.notification_post);
            relativeLayoutRoot = (RelativeLayout)itemView.findViewById(R.id.notification_item_relative_root);


            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            clickListener.NotificationClicked(id, isComment, position);
            relativeLayoutRoot.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
    }
}
