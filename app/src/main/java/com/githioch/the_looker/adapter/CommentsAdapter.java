package com.githioch.the_looker.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.githioch.the_looker.CommentsActivity;
import com.githioch.the_looker.R;
import com.githioch.the_looker.model.Comment;
import com.githioch.the_looker.util.ConnectionDetector;
import com.githioch.the_looker.util.ProfilePictureView;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.pkmmte.view.CircularImageView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Githioch on 7/10/2015.
 * Adapter for loading comments
 */
public class CommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int IS_PARSE_LOGIN = 1;
    private static final int IS_FACEBOOK_LOGIN = 2;
    private static final int IS_COP = 3;
    private final ArrayList<Comment> list;
    private final CommentsActivity activity;
    private Context mContext;
    private CommentsAdapter commentsAdapter;
    private String id;

    public CommentsAdapter(Activity activity, ArrayList<Comment> list, Context context) {
        this.list = list;
        this.mContext = context;
        commentsAdapter = this;
        this.activity = (CommentsActivity) activity;

    }

    public void add(Comment comment){
        list.add(comment);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(viewType == IS_FACEBOOK_LOGIN){
            View view = LayoutInflater.from(mContext).inflate(R.layout.comment_layout_facebook, parent, false);
            return new ViewHolderFacebook(view);
        }
        else if(viewType == IS_COP){
            View view = LayoutInflater.from(mContext).inflate(R.layout.comment_layout_cop, parent, false);
            return new ViewHolderCop(view);
        }
        else{
            View view = LayoutInflater.from(mContext).inflate(R.layout.comment_layout_parse, parent, false);
            return new ViewHolderParse(view);
        }
    }

    @Override
    public int getItemViewType(int position) {

        if(isFaceBookLogin(position)){
            return IS_FACEBOOK_LOGIN;
        }
        else if(isPoliceOfficer(position)){
            return IS_COP;
        }
        return IS_PARSE_LOGIN;
    }

    private boolean isPoliceOfficer(int position) {
        return list.get(position).getName().contains("Police Officer");
    }

    private boolean isFaceBookLogin(int position) {
        return list.get(position).getFacebookId() != null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if(holder instanceof ViewHolderFacebook){
            bindFacebook((ViewHolderFacebook) holder, position);
        }
        else if(holder instanceof ViewHolderCop){
            bindCop((ViewHolderCop)holder, position);
        }
        else{
            bindParse((ViewHolderParse) holder, position);
        }
    }

    private void bindCop(ViewHolderCop holder, int position) {

        final Comment comment  = list.get(position);

        holder.position = position;

        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
        formatter.setTimeZone(TimeZone.getTimeZone("Africa/Nairobi"));
        Date d;
        if(comment.isNewComment()){
            Calendar c = Calendar.getInstance();
            d = c.getTime();

        }else{
            d = comment.getTime();
        }
        if(d != null){
            try {
                d = formatter.parse(formatter.format(d));
            } catch (java.text.ParseException e1) {
                e1.printStackTrace();
            }

            CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                    d.getTime(),
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
            holder.time.setText(timeAgo);
        }

        holder.name.setText(comment.getName());
        holder.comment.setText(comment.getComment());

        if(comment.isNewComment()) {
            sendCommentCop(holder, getItem(position));
            comment.setIsNewComment(false);

        }
    }

    private void sendCommentCop(final ViewHolderCop holder, Comment comment) {
        holder.sending.setVisibility(View.VISIBLE);
        holder.name.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_light));
        holder.comment.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_light));
        ParseObject parseObject = new ParseObject("comments");
        parseObject.put("comment", comment.getComment());
        parseObject.put("parseName", comment.getName());
        parseObject.put("objectIdRef", getId());
        if(new ConnectionDetector(mContext).isConnectingToInternet()) {
            parseObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    Log.d("comment", "saved");
                    activity.incrementComment();
                    holder.sending.setText("Sent");
                    holder.isSent = true;
                    holder.sending.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_dark));
                    holder.name.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_dark));
                    holder.comment.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_dark));

                }
            });
        }else{
            holder.isSent = false;
            holder.sending.setText("Sending failed. Tap to retry.");
        }
    }

    private void bindParse(final ViewHolderParse holder, int position) {

        final Comment comment  = list.get(position);

        holder.position = position;

        if(comment.getParseImage() != null){
            byte[] bitmapdata = new byte[0];
            try {
                bitmapdata = comment.getParseImage().getData();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Bitmap bitmap = BitmapFactory.decodeByteArray(bitmapdata, 0, bitmapdata.length);
            holder.circularImageView.setImageBitmap(bitmap);
        }else{
            holder.circularImageView.setImageDrawable(mContext.getResources()
                    .getDrawable(R.drawable.no_profile));
        }

        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
        formatter.setTimeZone(TimeZone.getTimeZone("Africa/Nairobi"));
        Date d;
        if(comment.isNewComment()){
            Calendar c = Calendar.getInstance();
            d = c.getTime();

        }else{
            d = comment.getTime();
        }
        if(d != null){
            try {
                d = formatter.parse(formatter.format(d));
            } catch (java.text.ParseException e1) {
                e1.printStackTrace();
            }

            CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                    d.getTime(),
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
            holder.time.setText(timeAgo);
        }

        holder.name.setText(comment.getName());
        holder.comment.setText(comment.getComment());

        if(comment.isNewComment()) {
            sendCommentParse(holder, getItem(position));
            comment.setIsNewComment(false);

        }


    }

    private void sendCommentParse(final ViewHolderParse holder, Comment comment) {

        holder.sending.setVisibility(View.VISIBLE);
        holder.name.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_light));
        holder.comment.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_light));
        ParseObject parseObject = new ParseObject("comments");
        parseObject.put("comment", comment.getComment());
        if(comment.getParseImage() != null){
            parseObject.put("parseIcon", comment.getParseImage());
        }
        parseObject.put("parseName", comment.getName());
        parseObject.put("objectIdRef", getId());
        if(new ConnectionDetector(mContext).isConnectingToInternet()) {
            parseObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    Log.d("comment", "saved");
                    activity.incrementComment();
                    holder.sending.setText("Sent");
                    holder.isSent = true;
                    holder.sending.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_dark));
                    holder.name.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_dark));
                    holder.comment.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_dark));

                }
            });
        }else{
            holder.isSent = false;
            holder.sending.setText("Sending failed. Tap to retry.");
        }

    }

    private void bindFacebook(final ViewHolderFacebook holder, int position) {

        final Comment comment  = list.get(position);

        holder.position = position;

        holder.profilePictureView.setProfileId(comment.getFacebookId());

        DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss Z");
        formatter.setTimeZone(TimeZone.getTimeZone("Africa/Nairobi"));
        Date d;
        if(comment.isNewComment()){
            Calendar c = Calendar.getInstance();
            d = c.getTime();

        }else{
            d = comment.getTime();
        }
        if(d != null){
            try {
                d = formatter.parse(formatter.format(d));
            } catch (java.text.ParseException e1) {
                e1.printStackTrace();
            }

            CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(
                    d.getTime(),
                    System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);
            holder.time.setText(timeAgo);
        }

        holder.name.setText(comment.getName());
        holder.comment.setText(comment.getComment());

        if(comment.isNewComment()) {
            sendCommentFacebook(holder, getItem(position));
            comment.setIsNewComment(false);

        }

        holder.sending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!holder.isSent) {
                    sendCommentFacebook(holder, comment);
                }
            }
        });
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    private void sendCommentFacebook(final ViewHolderFacebook holder, Comment comment) {
        holder.sending.setVisibility(View.VISIBLE);
        holder.name.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_light));
        holder.comment.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_light));
        ParseObject parseObject = new ParseObject("comments");
        parseObject.put("comment", comment.getComment());
        parseObject.put("facebookId", comment.getFacebookId());
        parseObject.put("facebookName", comment.getName());
        parseObject.put("objectIdRef", getId());
        if(new ConnectionDetector(mContext).isConnectingToInternet()) {
            parseObject.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    Log.d("comment", "saved");
                    activity.incrementComment();
                    holder.sending.setText("Sent");
                    holder.isSent = true;
                    holder.sending.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_dark));
                    holder.name.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_dark));
                    holder.comment.setTextColor(mContext.getResources().getColor(R.color.primary_dark_material_dark));

                }
            });
        }else{
            holder.isSent = false;
            holder.sending.setText("Sending failed. Tap to retry.");
        }
    }

    public void setId(String id) {
        this.id = id;
    }

    private String getId(){
        return this.id;
    }

    public Comment getItem(int position) {
        return list.get(position);
    }

    class ViewHolderFacebook extends RecyclerView.ViewHolder{

        ProfilePictureView profilePictureView;
        TextView time;
        TextView name;
        TextView comment;
        TextView sending;
        boolean isSent = false;
        int position;

        public ViewHolderFacebook(View itemView) {
            super(itemView);

            profilePictureView = (ProfilePictureView) itemView.findViewById(R.id.comments_facebook_image);
            time = (TextView)itemView.findViewById(R.id.comments_time);
            name = (TextView)itemView.findViewById(R.id.comments_user_name);
            comment = (TextView)itemView.findViewById(R.id.comments_comment);
            sending = (TextView)itemView.findViewById(R.id.comments_sending);


        }
    }

    class ViewHolderCop extends RecyclerView.ViewHolder{

        TextView time;
        TextView name;
        TextView comment;
        TextView sending;
        boolean isSent = false;
        int position;

        public ViewHolderCop(View itemView) {
            super(itemView);

            time = (TextView)itemView.findViewById(R.id.comments_time);
            name = (TextView)itemView.findViewById(R.id.comments_user_name);
            comment = (TextView)itemView.findViewById(R.id.comments_comment);
            sending = (TextView)itemView.findViewById(R.id.comments_sending);


        }
    }

    class ViewHolderParse extends RecyclerView.ViewHolder{

        CircularImageView circularImageView;
        TextView time;
        TextView name;
        TextView comment;
        TextView sending;
        boolean isSent = false;
        int position;

        public ViewHolderParse(View itemView) {
            super(itemView);

            circularImageView = (CircularImageView) itemView.findViewById(R.id.comments_parse_image);
            time = (TextView)itemView.findViewById(R.id.comments_time);
            name = (TextView)itemView.findViewById(R.id.comments_user_name);
            comment = (TextView)itemView.findViewById(R.id.comments_comment);
            sending = (TextView)itemView.findViewById(R.id.comments_sending);


        }


    }
}
