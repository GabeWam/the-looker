package com.githioch.the_looker.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.githioch.the_looker.R;
import com.githioch.the_looker.model.Message;
import com.githioch.the_looker.util.ConnectionDetector;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;

/**
 * Created by Githioch on 7/15/2015.
 * Adapter that loads messages that appear when conversing with OCPD
 */
public class OcpdMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int IS_MESSAGE = 1;
    private static final int IS_NEW_MESSAGE = 2;
    private static final int IS_RESPONSE = 3;
    private final Context mContext;
    private ArrayList<Message> arrayList = new ArrayList<>();
    private final Activity mActivity;

    public OcpdMessageAdapter(Context mContext, ArrayList<Message> arrayList, Activity mActivity){
        this.mContext  = mContext;
        this.arrayList = arrayList;
        this.mActivity  = mActivity;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == IS_RESPONSE){
            View view = LayoutInflater.from(mContext).inflate(R.layout.
                    ocpd_private_message_layout_is_response, parent, false);
            return new Response(view);
        }else if(viewType == IS_MESSAGE){
            View view = LayoutInflater.from(mContext).inflate(R.layout.
                    ocpd_private_message_is_message, parent, false);
            return new MessageFromOcpd(view);
        }else{
            View view = LayoutInflater.from(mContext).inflate(R.layout.
                    ocpd_private_message_layout_is_new_message, parent, false);
            return new NewMessageToOcpd(view);
        }

    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        Message message = arrayList.get(position);

        if(holder instanceof MessageFromOcpd){

            ((MessageFromOcpd)holder).comment.setText(message.getMessage());
            ((MessageFromOcpd)holder).time.setText(message.getDate());

        }else if(holder instanceof Response){
            ((Response)holder).comment.setText(message.getMessage());
            ((Response)holder).time.setText(message.getDate());

        }else{
            ((NewMessageToOcpd)holder).comment.setText(message.getNewMessage());
            ((NewMessageToOcpd)holder).time.setText(message.getDate());
            final ParseObject parseObject = new ParseObject("Messages_To_Ocpd");
            /*if(message.isOcpdComment()){
                parseObject.put("isMessage", false);
                parseObject.put("response", message.getNewMessage());
                parseObject.put("read", false);
            }else{
                parseObject.put("isMessage", true);
                parseObject.put("message", message.getNewMessage());
            }*/
            parseObject.put("isMessage", true);
            parseObject.put("message", message.getNewMessage());
            parseObject.put("readByCop", false);

            if(ParseUser.getCurrentUser().has("profile")){
                parseObject.put("senderName", ParseUser.getCurrentUser().getJSONObject("profile")
                        .optString("name"));
            }else{
                parseObject.put("senderName", ParseUser.getCurrentUser().getString("name"));
            }

            parseObject.put("senderId", ParseUser.getCurrentUser().getObjectId());

            if(new ConnectionDetector(mActivity).isConnectingToInternet()){
                parseObject.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        ((NewMessageToOcpd)holder).sendingStatus.setText("Sent");
                    }
                });
            }else{
                ((NewMessageToOcpd)holder).sendingStatus.setText("Sending Failed. Tap To Retry.");
                ((NewMessageToOcpd)holder).sendingStatus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(new ConnectionDetector(mActivity).isConnectingToInternet()){
                            parseObject.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    ((NewMessageToOcpd)holder).sendingStatus.setText("Sent");
                                }
                            });
                        }
                    }
                });
            }

        }

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    @Override
    public int getItemViewType(int position) {

        if(isMessage(position)){
            return IS_MESSAGE;
        }else if (isNewMessage(position)){
            return IS_NEW_MESSAGE;
        }
        return IS_RESPONSE;
    }

    private boolean isNewMessage(int position) {
        return arrayList.get(position).isNewMessage();
    }

    private boolean isMessage(int position) {
        return arrayList.get(position).isMessage();
    }

    class MessageFromOcpd extends RecyclerView.ViewHolder{

        TextView time;
        TextView comment;
        LinearLayout linearLayout;

        public MessageFromOcpd (View itemView) {
            super(itemView);

            linearLayout = (LinearLayout)itemView.findViewById(R.id.private_message_linear);
            time = (TextView) itemView.findViewById(R.id.comments_time);
            comment = (TextView) itemView.findViewById(R.id.ocpd_message);
        }
    }

    class Response extends RecyclerView.ViewHolder{

        TextView time;
        TextView comment;
        LinearLayout linearLayout;

        public Response (View itemView) {
            super(itemView);

            linearLayout = (LinearLayout)itemView.findViewById(R.id.private_message_linear);
            time = (TextView) itemView.findViewById(R.id.comments_time);
            comment = (TextView) itemView.findViewById(R.id.ocpd_message);
        }
    }

    class NewMessageToOcpd extends RecyclerView.ViewHolder{

        TextView time;
        TextView comment;
        LinearLayout linearLayout;
        TextView sendingStatus;

        public NewMessageToOcpd(View itemView) {
            super(itemView);

            linearLayout = (LinearLayout)itemView.findViewById(R.id.private_message_linear);
            time = (TextView) itemView.findViewById(R.id.comments_time);
            comment = (TextView) itemView.findViewById(R.id.ocpd_message);
            sendingStatus = (TextView)itemView.findViewById(R.id.comments_sending);

        }

    }

}
