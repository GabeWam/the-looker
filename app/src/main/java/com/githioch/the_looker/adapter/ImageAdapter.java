package com.githioch.the_looker.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.githioch.the_looker.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import java.util.ArrayList;

/**
 * Created by Githioch on 7/3/2015.
 */
public class ImageAdapter extends BaseAdapter {

    private final int count;
    private final ArrayList<Bitmap> thumbnails;
    private final String[] arrPath;
    private final GridImageClickListener gridImageClickListener;
    private final ImageLoader imageLoader;
    private DisplayImageOptions optionsLoader;
    private LayoutInflater mInflater;
    private Context context;


    public ImageAdapter(Context context, int count,
                        ArrayList<Bitmap> thumbnails, String[] arrPath,
                        GridImageClickListener gridImageClickListener) {




        Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                R.drawable.ic_take_photo);
        this.thumbnails = thumbnails;
        this.thumbnails.add(0, icon);
        this.count = count;
        this.context = context;
        this.arrPath = arrPath;
        this.gridImageClickListener = gridImageClickListener;
        imageLoader = ImageLoader.getInstance();
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }


    public int getCount() {
        return count;
    }



    public Object getItem(int position) {
        return position;
    }



    public long getItemId(int position) {
        return position;
    }



    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.galleryitem,parent, false);
            holder.imageview = (ImageView) convertView.findViewById(R.id.thumbImage);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.imageview.setId(position);
//        holder.imageview.setImageBitmap(thumbnails.get(position));
        holder.imageview.setMaxHeight(80);
        holder.imageview.setMaxWidth(80);

        // Get Image
        // First decode with inJustDecodeBounds=true to check dimensions
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(arrPath[position], options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, 50, 50);
        options.inJustDecodeBounds = false;

        optionsLoader = new DisplayImageOptions.Builder()
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .decodingOptions(options)
                .showImageOnLoading(R.drawable.ic_launcher)
                .showImageOnFail(R.drawable.ic_launcher)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .showImageForEmptyUri(R.drawable.ic_launcher)
                .considerExifParams(true)
                .build();
//        ImageSize imageSize = new ImageSize(100, 100);


//        imageLoader.loadImage("file://" + arrPath[position], holder.imageview,options);
        imageLoader.displayImage("file://"+arrPath[position], holder.imageview, optionsLoader);
        holder.imageview.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                gridImageClickListener.gridImageClicked(arrPath[position]);

            }

        });


        return convertView;

    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // BEGIN_INCLUDE (calculate_sample_size)
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            long totalPixels = width * height / inSampleSize;

            // Anything more than 2x the requested pixels we'll sample down further
            final long totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels > totalReqPixelsCap) {
                inSampleSize *= 2;
                totalPixels /= 2;
            }
        }
        return inSampleSize;
        // END_INCLUDE (calculate_sample_size)
    }

    public interface GridImageClickListener {

        void gridImageClicked(String arrPath);
    }

    class ViewHolder {

        ImageView imageview;

    }

}





