package com.githioch.the_looker.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import com.githioch.the_looker.R;

/**
 * Created by Githiora Wamunyu on 10/5/2015.
 */
public class FeedbackAdapter extends BaseAdapter{

    private Context context;
    private LayoutInflater layoutInflater;

    public FeedbackAdapter(Context context){
        this.context = context;
       layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        ViewHolderFeedback viewHolder;

        if(view == null){
            view = layoutInflater.inflate(R.layout.adapter_feedback, parent, false);

            viewHolder = new ViewHolderFeedback();
            /*viewHolder.no = (Button)view.findViewById(R.id.feedback_no);
            viewHolder.maybeLater = (Button)view.findViewById(R.id.feedback_maybe_later);
            viewHolder.yes = (Button)view.findViewById(R.id.feedback_yes);*/

            view.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolderFeedback) view.getTag();
        }



        return view;

    }

    private static class ViewHolderFeedback {
        Button yes;
        Button no;
        Button maybeLater;
    }
}
